# -*- mode: dockerfile -*-
FROM python:alpine as builder
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

RUN sphinx-build "./docs" "en/html"
RUN sphinx-build "./docs" "ru/html" -D language='ru'

# FROM openresty/openresty:latest
# FROM openresty/openresty:stretch-fat
FROM openresty/openresty:alpine
# COPY --from=builder en/html /usr/share/nginx/html/en
# COPY --from=builder ru/html /usr/share/nginx/html/ru
COPY --from=builder en/html /usr/local/openresty/nginx/html/en
COPY --from=builder ru/html /usr/local/openresty/nginx/html/ru
