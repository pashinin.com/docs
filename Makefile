# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    = -a
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = pashinin
SOURCEDIR     = ./docs
# BUILDDIR      = en
BUILDDIR      = /tmp/en

# locale_dirs = ['locale/']   # path is example but recommended.
# gettext_compact = False     # optional.

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: locale
locale:
	make gettext  # creates $(BUILDDIR)/gettext
#	sphinx-intl update -p "translated" -l ru
	sphinx-intl update -p "$(BUILDDIR)/gettext" -l ru
#	make -e SPHINXOPTS="-D language='ru'" html

# Install
install:
	which sphinx-build || yay -S python-sphinx  # on Arch

.PHONY: en
en: install
	make html

.PHONY: ru
ru: install
	make -e BUILDDIR="ru" -e SPHINXOPTS="-D language='ru'" html

.PHONY: deploy
deploy:
	cd deploy; ./deploy.sh master

.PHONY: deps
deps:
	sudo pip install -r requirements.txt
