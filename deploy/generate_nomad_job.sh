#!/bin/sh

REGION=$1
TEMPLATE=region-$REGION.job.tpl

if [ "$CI_COMMIT_SHORT_SHA" == "" ]; then
    export CI_COMMIT_SHORT_SHA="sha1"  # master / stable
fi

TAG="$CI_COMMIT_TAG"
if [ "$TAG" == "" ]; then
    TAG="$CI_COMMIT_REF_NAME"  # master / stable
fi
if [ "$TAG" == "" ]; then
    TAG="dev"
fi

export REGION=$REGION
export TAG=$TAG
export DATE=`date`

envsubst < "deploy/$TEMPLATE" | sed -e '/<<EOH.*/rdeploy/nginx.conf'
