# -*- mode: hcl -*-
job "docs" {
  region = "${REGION}"
  datacenters = ["ohio"]
  type = "service"

  constraint {
    attribute = "${meta.instance_type}"
    value = "worker"
  }

  # constraint {
  #   distinct_hosts = true
  # }

  update {
    stagger      = "30s"
    max_parallel = 1
    # canary = 1
  }

  group "master" {
    count = 1

    restart {
      mode = "delay"  # fail, delay (default)
      interval = "20s"
      attempts = 1
      delay = "10s"  # minimum wait
    }

    task "docs" {
      driver = "docker"
      config {
        image = "https://registry.gitlab.com/pashinin.com/docs:latest"
        port_map {
          http = 80  # used inside a container
        }

        volumes = [
          # "local/default.conf:/etc/nginx/conf.d/default.conf",
          "local/nginx.conf:/usr/local/openresty/nginx/conf/nginx.conf",
        ]
      }

      env {
        V = "7"
        VERSION = "master-${VERSION}-${DATE}"
      }

      # Do NOT remove this empty template
      # The content of nginx.conf will be inserted here.
      template {
        data = <<EOH

EOH
        destination = "local/nginx.conf"
      }


      resources {
        cpu    = 100 # 100 = 100 MHz
        memory = 128 # 128 = 128 MB
        network {
          mbits = 10
          port "http" { }
        }
      }

      service {
        name = "docs"
        tags = [
          "docs",
          "urlprefix-docs.pashinin.com:443/",
          "urlprefix-docs.pashinin.com:80/ redirect=301,https://docs.pashinin.com$path",

          # Traefik tags:
          # "traefik.enable=true",
          # "traefik.http.routers.docs.rule=Host(`docs.pashinin.com`)",
          # "traefik.http.routers.docs.entrypoints=http",
          # "traefik.tags=service",
        ]
        port = "http"
        check {
          # Using "tcp" since nginx.conf will redirect everyone to
          # https:// and changed path.
          type     = "tcp"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }
  }
}
