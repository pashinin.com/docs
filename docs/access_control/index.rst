Access control
==============

Access to a Tree can be granted to:

* nobody
* everyone
* specific roles
* specific users


.. toctree::
   :maxdepth: 2

   roles
   permissions
