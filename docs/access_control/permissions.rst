Permissions
===========






SQL. Queries
------------

Insert a new permission:

.. code-block:: sql

   INSERT INTO permissions (name, key) VALUES ('View tree', 'tree.view');


Assign all existing permissions to an existing role:

.. code-block:: sql

   INSERT INTO roles_permissions (role_id, permission_id) SELECT 'ROLE_ID', id FROM permissions;



Get all user permissions for all user's roles from root tree until
current specific tree:

.. code-block:: sql

   -- First, get all roles, then - get all permissions for these roles
   SELECT perms.* from permissions perms
   INNER JOIN roles_permissions rp ON rp.permission_id=perms.id
   WHERE rp.role_id IN (
     -- get users' roles
     SELECT r.id FROM roles r
     INNER JOIN users_roles ur ON ur.role_id=r.id AND ur.user_id='d0b9a2d9-00f0-49ca-af4f-026517846b4c'
     WHERE tree_id IS NULL OR tree_id IN (
       -- get parents and self
       SELECT parent FROM tree_connections WHERE child='86859e1a-5e29-44bc-9462-9c4aa5ba11c6'
     )
   );


Indexes
-------

.. code-block:: sql

   CREATE UNIQUE INDEX ON permissions (key);



Tree permissions
----------------

.. note::

   Permissions can be granted only for the whole Tree (including
   subtrees) but not for specific children. So if a user can edit
   :code:`Tree-1` he can edit all it's children as well :code:`Tree-1 >
   Tree-2 > ...`.

.. note::

   Another example. User has only :code:`view` permission for
   :code:`Tree-1` but he has :code:`all` permissions for a :code:`draft`
   child of :code:`Tree-1`. How to get items of :code:`Tree-1` to
   display?

   User does not have access to all drafts of :code:`Tree-1` but only
   for one specific child.

:code:`Tree.Permissions` an array of strings
   Set when a Tree is about to be sent to a user in JSON format.


"all" permission
----------------

If a user has a "Superuser" role with :code:`tree_id` not null than in a
list of Tree permission would be only one string:

.. code-block:: text

   {
     Name: "Tree-1"
     ...
     Permissions: ['all'],
   }

Possible permissions:

#. :code:`all` - user can do anything with this Tree.  When user has
   :code:`all` permissions only this string is inside :code:`Permissions`
   array.
#. :code:`view` - you can view the content of a current Tree.
#. :code:`edit` - you can edit different text fields and other options.
#. :code:`publish` - you can set :code:`Draft=false`
#. :code:`s_del` - soft delete
#. :code:`h_del` - hard delete

To check if user can do something use:

.. code-block:: js

   // if (tree.can('<permission>')) {
   //   ...
   // }

   if (tree.can('edit')) {
     // ...
   }

.. note::

   The list of all permissions is tied to a Tree object when getting
   this Tree from API.
