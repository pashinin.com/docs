Roles
=====

This project uses Role-based access control (RBAC). The roles can be
either "global" or "tree-specific".


Superuser (owner) role
----------------------

Superuser role is defined by :code:`superuser` boolean flag in a
database. Superusers can do anything. When "tree-specific" - anything
within this tree.

If :code:`tree_id` is NOT specified then this is a "global"
superuser. Currently there is only one global superuser.

If :code:`tree_id IS NOT NULL` - this is a "tree-specific"
superuser.


Custom roles
------------

Any superuser can create roles. Tree-specific superusers can create only
tree-specific roles (inside his tree).



SQL. Queries
------------

Assign a role to a user:

.. code-block:: sql

   INSERT INTO users_roles (user_id, role_id) VALUES ('', '');

..
   CONSTRAINT user_roles_pk PRIMARY KEY (id ASC),                                       |
   |              |        CONSTRAINT user_roles_fk FOREIGN KEY (user_id_old) REFERENCES users(id_old),         |
   |              |        CONSTRAINT user_roles_role_id_fk FOREIGN KEY (role_id_old) REFERENCES roles(id_old),

Get **global roles only** for a user:

.. code-block:: sql

   SELECT r.* FROM roles r INNER JOIN users_roles ur ON ur.role_id=r.id AND ur.user_id='USERID' WHERE tree_id IS NULL;

Get all user's roles in a specific tree (including global superuser role):

.. code-block:: sql

   SELECT r.* FROM roles r
   INNER JOIN users_roles ur ON ur.role_id=r.id AND ur.user_id='d0b9a2d9-00f0-49ca-af4f-026517846b4c'
   WHERE tree_id IS NULL OR tree_id IN (
     -- get parents and self
     SELECT parent FROM tree_connections WHERE child='86859e1a-5e29-44bc-9462-9c4aa5ba11c6'
   );

Get all roles in a tree:

.. code-block:: sql

   SELECT * FROM roles r WHERE tree_id IS NULL OR tree_id='86859e1a-5e29-44bc-9462-9c4aa5ba11c6';

Delete role:

.. code-block:: sql

   DELETE FROM users_roles WHERE role_id='';
   DELETE FROM roles WHERE id='';


users_roles indexes
-------------------

PK :code:`(user_id, role_id)`

Index on :code:`user_id` (to get all roles for a user)

Index on :code:`tree_id` (to filter global/tree-specific roles)


Global permissions
------------------

Editing global permissions is allowed only from separate admin page.
