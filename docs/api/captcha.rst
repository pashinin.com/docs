/captcha
========

`Repo <https://gitlab.com/pashinin.com/api-group/captcha-rs>`_

.. note::

   #. Each captcha has an UUID. So many captchas can exist on the same
      page.
   #. Default storage - Redis.

      ..
         I'm using Tarantool as a storage for captchas. Since captcha's IDs
         are always unique it is safe to insert records into any master in
         multi-master setup. Tarantool's space ID for "captchas" space is
         :code:`1`. To create this space manually:
         :code:`box.schema.space.create('captchas',{id=1})`. Use
         :code:`box.space` to check it was created.  Also create an index
         (field #1 - ID, field #2 - code itself):
         :code:`box.space.captchas:create_index('primary', {type = 'hash',
         unique=true, parts = {1, 'string'}})`. To view all existing records:
         :code:`box.space.captchas:select()`. No additional index on time
         field (there were no such indexes in examples of expirationd, it
         scans by primary index).

   #. Captcha is expired in 15 minutes. It is a `hardcoded constant
      <https://gitlab.com/pashinin.com/api-group/captcha-rs/-/blob/master/src/main.rs#L22>`_.

.. http:get:: /captcha/
   :deprecated:

   Returns a JSON object with captcha UUID and base64-encoded image
   (150x56) of a captcha.

   :resheader Content-Type: application/json
   :>json string id: Captcha UUID
   :>json string b64data: Base64 encoded PNG with :code:`data:` prefix
                          to insert into a HTML :code:`img` tag
                          (:code:`src` attribute).

   .. code-block:: http

      HTTP/1.1 200 OK
      cache-control: no-cache
      content-type: application/json

      {
          id: "82e7b36b-9ec4-47dc-a812-a069945f49eb",
          b64data: "data:image/png;base64, iVBORw0K...=="
      }


.. http:get:: /captcha/base64

   Returns captcha (PNG 150x56) encoded in base64.
   Captcha UUID is in headers (:code:`captcha-uuid` header).

   :resheader Content-Type: text/plain
   :resheader captcha-uuid: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
   :code 200: OK

   .. code-block:: http

      HTTP/1.1 200 OK
      cache-control: no-cache
      content-type: text/plain

      {
          id: "82e7b36b-9ec4-47dc-a812-a069945f49eb",
          b64data: "data:image/png;base64, iVBORw0K...=="
      }


.. http:get:: /captcha/healthcheck

   Returns :code:`200 OK` if service is ok.

   :code 200: OK
   :code 404: there's no user

   Checks:

   * Redis connection

   Example response (``200 OK``):

   .. code-block:: text

      {
          service: "files-rs",
          version: "0.1.0",
          description: "Files management",
          commit_short_sha: ""
      }

   Example response (``500``):

   .. code-block:: text

      {
          service: "files-rs",
          version: "0.1.0",
          description: "Files management",
          commit_short_sha: ""
      }


.. http:get:: /captcha/png

   Returns a PNG image (150x56) of a captcha with :code:`captcha-uuid`
   header.

   :resheader captcha-uuid: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
   :resheader Content-Type: image/png

   .. code-block:: http

      HTTP/1.1 200 OK

      cache-control: no-cache
      captcha-uuid: e445b9fe-15bf-46ab-a3fc-b7956a74be83
      content-type: image/png


   .. image:: ../../images/captcha.png
