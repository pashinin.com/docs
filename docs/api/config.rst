Configuration
=============

API can be configured through flags and environment variables. All
environment variables are uppercased variants of flags with hyphens
replaces with underscored. For example if a flag is :code:`--sentry-dsn`
then env var will be :code:`SENTRY_DSN`.


Following 4 variables are used to connect to SQL database:

--database
  (required) SQL database connection URI. Default:
  :code:`cockroach://root@127.0.0.1:26258/db1`

JDI
---

--jdi-message-broker


--jdi-task-storage

--jdi-task-broker
  STAN (NATS-streaming) address for tasks.
  Must use "at-least-once" delivery broker for tasks.
  Default: :code:`nats://localhost:4323`

--jdi-task-channel

Sentry
------

--sentry-dsn
  (optional) If set - internal errors will also be sent to `Sentry
  <https://sentry.io/>`_

Object storage (S3)
-------------------

S3_ACCESS
  (required) Access key to S3 object storage.

S3_SECRET
  (required) Secret key to S3 object storage. Default: not set. Set it
  in :code:`.env.local` when developing locally.
