Content-type
============

Request
-------

API should support 2 types of requests when possible:

1. :code:`Content-Type: application/x-www-form-urlencoded`
2. :code:`Content-Type: application/json`


Response
--------
