/deploy
=======

`Repo <https://gitlab.com/pashinin.com/api-group/deploy-rs>`_


.. http:post:: /deploy/

   Deploys a given project.

   :reqheader Content-Type: application/json
   :reqheader DEPLOY_TOKEN: <token>

   :<json string project_url: (*required*) Gitlab url
   :<json string tag: (*optional*) Docker tag to deploy. If empty or not
                      set then :code:`latest` is used.

   :>json string status: :code:`ok` or :code:`failed`
   :>json object errors: :code:`{}`

   .. important::

      Deploying :code:`latest` never goes to production.

   .. tip::

      In Debug mode you don't have to specify DEPLOY_TOKEN.

   Example:

   .. code-block:: text

      # Deploy docs
      curl -L -X POST -H 'Content-Type: application/json' -H 'DEPLOY_TOKEN: 123' \
      --data '{"project_url": "https://gitlab.com/pashinin.com/docs", "tag": ""}' \
      http://api.localhost/deploy/

   .. code-block:: json

      {
          "status": "ok"
      }



.. http:get:: /deploy/healthcheck

   Returns :code:`200 OK` if everything is ok.asd

   :code 200: no error
   :code 404: there's no user

   Checks:

   * Database connection
   * S3 storage connection

   Example response (``200 OK``):

   .. code-block:: text

      {
          service: "files-rs",
          version: "0.1.0",
          description: "Files management",
          commit_short_sha: ""
      }

   Example response (``503``):

   .. code-block:: text

      {
          service: "files-rs",
          version: "0.1.0",
          description: "Files management",
          commit_short_sha: ""
      }
