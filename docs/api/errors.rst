Error handling and health checks
================================

Health checks
-------------

Each microservice has a health check at ``GET /healthcheck``. If it
returns :http:statuscode:`200` then everything is fine.

Any other code means a service has problems.

Errors in regular JSON responses
--------------------------------

Regular JSON responses always return :http:statuscode:`200`.  If there
were errors then they are returned in ``errors`` field.

Example:

.. code-block:: http

   HTTP/1.1 200 OK

   {
     "errors": {
       "password": "Password can not be empty"
     }
   }

.. note::

   About i18n. `Project Fluent <https://www.projectfluent.org/>`_ is used.
   Errors are always in English. Translation is done in frontend.


Errors in other responses
-------------------------

If a missing file was requested - :http:statuscode:`404`

If a server failure happens - :http:statuscode:`503`
