/files
======

`Repo <https://gitlab.com/pashinin.com/api-group/files-rs>`_


.. http:get:: /files/healthcheck

   Returns :code:`200 OK` if everything is ok.asd

   :code 200: no errors
   :code 404: there's no user

   .. attribute:: name

      The name of the encoding.

   * Database connection is ok
   * S3 storage connection is ok

   Example response (``200 OK``):

   .. code-block:: text

      {
          service: "files-rs",
          version: "0.1.0",
          description: "Files management",
          commit_short_sha: ""
      }

   Example response (``503``):

   .. code-block:: text

      {
          service: "files-rs",
          version: "0.1.0",
          description: "Files management",
          commit_short_sha: ""
      }



.. http:post:: /files/upload

   Uploads files into S3 storage.
