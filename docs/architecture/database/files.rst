files
=====

.. code-block:: text

   +------------+--------------------------+-------------------------+
   | Column     | Type                     | Modifiers               |
   |------------+--------------------------+-------------------------|
   | sha1       | bytea                    |  not null               |
   | size_bytes | bigint                   |                         |
   | added_at   | timestamp with time zone |  not null default now() |
   +------------+--------------------------+-------------------------+
   Indexes:
       "files_pkey" PRIMARY KEY, btree (sha1)


A file can be uploaded many times by different users. Information about
uploads is in files_uploads table.


sha1
----

HEX representation of a 160 bit (20 bytes) SHA-1 hash of a file. Takes
40 chars as a HEX-string.

To find a file by hash:

.. code-block:: sql

   INSERT INTO files (sha1, size_bytes) VALUES ('\xdeadbeef', 123);
   SELECT encode(sha1, 'hex') FROM files;
   SELECT * FROM files WHERE sha1 =
