tree
====

Each Tree in DB.

.. code-block:: text

   postgres@postgres:postgres> \d tree
   +--------+------+--------------------------------------+
   | Column | Type | Modifiers                            |
   |--------+------+--------------------------------------|
   | id     | uuid |  not null default uuid_generate_v4() |
   | title  | text |  not null default ''::text           |
   | slug   | text |  not null default ''::text           |
   | owner  | uuid |                                      |
   +--------+------+--------------------------------------+
   Indexes:
       "tree_pkey" PRIMARY KEY, btree (id)



owner
-----

Who owns that Tree.

Owner can do anything with a Tree and and it's children.

Any file uploads become Trees and an uploader becomes an
owner of these Trees.
