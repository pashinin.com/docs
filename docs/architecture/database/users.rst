users
=====

.. code-block:: text

   +--------------+--------------------------+--------------------------------------+
   | Column       | Type                     | Modifiers                            |
   |--------------+--------------------------+--------------------------------------|
   | id           | uuid                     |  not null default uuid_generate_v4() |
   | created_at   | timestamp with time zone |  not null default now()              |
   | updated_at   | timestamp with time zone |  not null default now()              |
   | first_name   | text                     |  not null                            |
   | last_name    | text                     |  not null                            |
   | email        | text                     |  not null                            |
   | password     | text                     |  not null                            |
   | lng          | text                     |  not null default 'en'::text         |
   | data         | jsonb                    |                                      |
   | use_gravatar | boolean                  |  not null default true               |
   | active       | boolean                  |  not null default true               |
   +--------------+--------------------------+--------------------------------------+
