Files removal from object storage
=================================

Same file can be used in multiple places: in articles, tasks, Trees,
anywhere... So it is complicated to tell if it is not used anyhow.

So no removal is made from object storage.
