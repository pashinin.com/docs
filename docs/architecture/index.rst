Architecture
============

.. toctree::
   :maxdepth: 2
   :caption: Contents

   database/index
   mail
   files/index
   ..
      pages
   user/index
