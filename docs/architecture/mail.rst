Mail
====


Mails meta data storage
-----------------------

Information about sent emails is stored for 6 months (to detect if an
email was already sent).  This information includes:

* user ID
* mail category ("0" for sign ups, "1" for password restore, ...)
* time when email was sent

.. warning::

   The content of an email is NEVER stored!

Currently this informations is stored in Tarantool (in-memory
persistent).
