Authentication
==============

Host: :code:`auth.pashinin.com`

.. note::

   `JSON Web Tokens (JWT) <https://jwt.io>`_ allow to store auth
   information (tokens) on client side (browser) and only verify them on
   server. So we don't need a server storage to save sessions. But we
   will still use it to store logins (IPs, user agents, but not
   sessions - that's different things).

.. warning::

   JSON Web Tokens (JWT) are NOT used for authentication.

   http://cryto.net/~joepie91/blog/2016/06/19/stop-using-jwt-for-sessions-part-2-why-your-solution-doesnt-work/

   If JWT is "bad" see https://paseto.io/.



Access token
------------

When a valid pair of username/password provided - user gets an access
token. But the only information you get in response in
:code:`/signin` is:

.. code-block:: text

   {code: 200, expire: "2019-07-29T15:21:17+03:00"}

Access token itself has following information in it's claims:

.. code-block:: text

   {id: "user.ID", exp: "expiration time", orig_iat: "timestamp when token generated"}

User ID is UUID string.

**The access token itself is saved in a HTTP-only secure cookie.** In
development mode - it's just HTTP-only of course (not secure). So there
is no access to :code:`access token` from Javascript, we know only
expiration time of a token.

.. warning::

   Access token must be stored in HTTP-only secured cookies. Not
   LocalStorage!


The expiration time needs to be preserved between application (page)
reloads. For that Web Storage is used which is unique for origin
(:code:`protocol://host:port` combination). So expiration time at
different host will be missing but it's ok.

Each 30s a fucntion is called which checks if access token will expire
in less then 20 mins. If so - it will attempt to refresh it at
:code:`/refresh`. A new access token will be set in a HTTP-only
cookie.


https://gist.github.com/zmts/802dc9c3510d79fd40f9dc38a12bccfc

Password hashing algorithm
--------------------------

Argon2id is used with following parameters:

+-------------------------+---------------------+
| Memory                  | 64 Mb               |
+-------------------------+---------------------+
| Iterations              | 3                   |
+-------------------------+---------------------+
| Parallelism             | 2                   |
+-------------------------+---------------------+

It takes up to 130 Mb with 2 parallel threads hence minimum RAM
requirement for API binary - 256 Mb.


Social auth
-----------

VK
