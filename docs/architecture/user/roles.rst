Roles
*****

User's permissions are stored in :code:`Roles`. Each user can have **global** roles
and **roles tied up to a specific tree**.

To get user's global roles use: :code:`user.Storage.GetUserGlobalRoles(u)`

To get all user's roles in a Tree: :code:`user.Storage.GetUserGlobalRoles(u)`

Golang struct :code:`Role` is defined in `user.go
<https://gitlab.com/pashinin.com/api/blob/master/models/user.go#L67>`_
and has following fields:

Name
  :code:`string`. However you wish to call that group of
  users. E.x.: :code:`Admins`. Default: not set.

Superuser
  :code:`bool`. Such user can do anything. Default: :code:`false`.

In a database all these fields are simply lower-cased.  Users-Roles is
many2many relation saved in :code:`user_roles` table.

..
   User's role without assigned tree (:code:`tree_id IS NULL`) is a superuser role.

2.  Each tree can have an owner who can do anything within that tree.
3.  Each tree can have it's own set of roles
