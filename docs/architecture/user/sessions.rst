User sessions
=============

Session storage
---------------

**Redis.**

Session data can be stored either in browser **cookies** (has limit 4096
bytes) or in a **server-side storage (Redis)**. You must use server-side
storage only at least for 1 purpose - to be able to log out from all
sessions at once.

Session id is the only part that is stored in cookies.



Session expiration time
-----------------------

**5 days**

There is an expiration time of:

#. **cookie in browser**
#. **Redis key-value pair**

Both parameters are set in auth repo :code:`auth/src/session.rs`

.. code-block:: text

   .session_length(SessionLength::Predetermined {
       max_session_length: Some(time::Duration::days(5)),
   })




Problem: cookie is only set for a subdomain
-------------------------------------------

While at "auth.localhost" and making request to
http://api.localhost/auth/signin a cookie is set for "auth.localhost"
only, not any other subdomain.

.. code-block:: text

   ...
   set-cookie: actix-session=...; HttpOnly; Path=/; Max-Age=604800
   ...
