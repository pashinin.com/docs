Documentation for pashinin.com
******************************

.. toctree::
   :maxdepth: 1
   :caption: pashinin.com

   api/index
   architecture/index
   infrastructure/index
   tree/index
   ui/index


.. toctree::
   :maxdepth: 1
   :caption: Other articles


   misc/index


.. glossary::

   Service worker
      asd
