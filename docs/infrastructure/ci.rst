Continuous integration (CI)
===========================


CI for Rust
-----------

Rust has a **big** compilation time of all dependencies + project itself
(10 mins and more). To reduce it I set :code:`GIT_CLEAN_FLAGS: -ffdx -e
target/` in :code:`.gitlab-ci.yml`. But then I need each project to
always build on the same gitlab runner.

The problem is each project will have 2-3 Gb of compiled dependencies
(debug + release) in :code:`target/` folder.

Solution to it may be Docker multistage build where the first stage is a
prepared image with all deps precompiled. But then all Gitlab
environment variables are not available on build stage since the build
process is inside Docker's stage.

Docker image with precompiled deps still can be created. Let's say
project's target path in this docker image will be under
/usr/src/app/target.  But I found a better solution for myself - run my
own Gitlab runner with some configuration for many similar projects.



1. Cache downloaded crates


   In order **to cache downloaded crates** (and not redownload them again)
   CARGO_HOME must be set to a directory that will persist between builds.
   Currently I set whole "/builds" dir as a volume in gitlab-runner config:


2. Big "./target" dir is a problem.

   Since I want to develop microservices that will use almost the same
   dependencies I do not want to have 2GB "./target" folder in each
   project. So I set 1 common "target" folder to :code:`/builds/target`.


Resulting gitlab-runner config:

   .. code-block:: text

      [[runners]]
        ...
        environment = ["CARGO_HOME=/builds/.cargo", "CARGO_TARGET_DIR=/builds/target"]
        ...
        [runners.docker]
          volumes = ["/cache", "/builds:/builds"]

.. warning::

   Runner must work with concurrency = 1. Several projects can't be
   built using the same directory.

.. note::

   When using :code:`/builds` as a volume - it's name looks like this:
   :code:`runner-bm5-wqbs-project-22525866-concurrent-0-cache-c33bcaa1fd2c77edfc3893b41966cea8/`
   where :code:`22525866` is a project ID. I wanted all API projects to
   build in the same :code:`/builds` dir. To exclude project ID you need
   to host-bind /builds dir :code:`/builds:/builds`.


See `configs
<https://gitlab.com/pashinin/vms/-/tree/master/gitlab-runner>`_ for more
info.
