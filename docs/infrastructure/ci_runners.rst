Gitlab runners
==============


"rust" runner
-------------

All projects are built in host-bound :code:`/builds` directory.
Example: :code:`/builds/xxx-xxxx/0/pashinin.com/ui/tree-rs`.

:code:`xxx-xxxx` - runner id

:code:`0` - concurrency id (always 0 since this runner has :code:`concurrency=1`)

All files will be preserved between builds since :code:`/builds`
directory is host mounted.
