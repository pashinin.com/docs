DB migrations
=============

Single repo vs multiple migrations in many projects
---------------------------------------------------

There are 2 ways of storing migrations:

#. All migrations in a single repo
#. Split migrations to several projects.

Splitting migrations into several projects leads to problems:

#. Ordering (no guarantee that changes in different projects will be
   pushed in the same order)
#. Will need to have multiple migration tables or something else.
#. Each project will include code for migrations.

**So all migrations are numbered and stored in a single repo.**


Flyway
------

I tried to use `flyway <https://flywaydb.org/>`_ for migrations. It
works fine but it's written in Java. So looking for an alternative.

.. code-block:: bash

   docker run --rm --network=dev_default -v "${PWD}/${TYPE}:/flyway/sql" flyway/flyway:7.2 \
   -url="${URL}" -user=${USER} -password="${PASSWORD}" migrate


It creates **flyway_schema_history** tables with migrations.


SQLx (Rust)
-----------

.. code-block:: rust

   sqlx::migrate!().run(&pool).await.unwrap();


It creates **_sqlx_migrations** tables with migrations.


migrations/01__initial.sql


refinery (Rust)
---------------

I used SQLx and it has it was easier to configure.
