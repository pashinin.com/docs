Postgres
========

Do I need Postgres?
-------------------

#. Sentry works with Postgres only, so yes.
#. It is stable and fast for a single node database.


Backups
-------

Nomad is configured to run backup task daily. Backups are stored at
:code:`node3` in a docker volume named :code:`postgres-backup-of-node5`.

Example:

.. code-block:: text

   /var/lib/docker/volumes/postgres-backup-of-node5/_data:
   total 5.6M
   drwxr-xr-x 3 root root 4.0K Jul 20 13:42 ..
   -rw-r--r-- 1 root root 5.5M Jul 21 12:20 sentry21-2021-07-21.dump

To test an existing dump run a clean copy of Postgres:

.. code-block:: text

   docker run --rm -p 5432:5432 -e POSTGRES_PASSWORD=123 postgres:13.3

and use :code:`pg_restore`:

.. code-block:: text

   cd /var/lib/docker/volumes/postgres-backup-of-node5/_data
   createdb -h localhost -U postgres sentry21
   pg_restore -h localhost -U postgres --no-privileges --no-owner -d sentry21 sentry21-2021-07-21.dump

:code:`-d` - connect to database name
