Restore DB
==========

1. Run Postgres server on a new host

2. Change "postgres" user password

   Enter task console using Nomad UI.

   .. code-block:: text

      su postgres
      psql

3. Create databases and users

   .. code-block:: sql

      -- postgres - already exists (password in Vault)
      ALTER USER postgres WITH PASSWORD '...';

      -- sentry (in Vault)
      CREATE DATABASE sentry21;
      CREATE USER sentry WITH ENCRYPTED PASSWORD '.......';
      -- ALTER USER sentry WITH PASSWORD 'new_password';
      GRANT ALL PRIVILEGES ON DATABASE sentry21 TO sentry;

4. Restore file

   .. code-block:: text

      # postgres
      PGUSER=postgres PGHOST=10.254.239.3 pg_restore -d postgres \
      /var/lib/docker/volumes/postgres-backup-of-node5/_data/postgres-2021-12-15.dump

      # sentry21
      PGUSER=sentry PGHOST=10.254.239.3 pg_restore -d sentry21 \
      /var/lib/docker/volumes/postgres-backup-of-node5/_data/postgres-2021-12-15.dump
