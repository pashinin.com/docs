Deployment
==========

`Hashicorp Nomad <https://www.nomadproject.io>`_ is used for
deployment. It starts all services (API instances, workers, frontend
instances).

.. note::

   Each task has CPU and RAM resources in Nomad.

   :code:`cpu` is a minimum reserved CPU. A task can use more of it.

   :code:`memory` is a maximum reserved memory. Process will crash if it
         uses more.

Environments
------------

There are always 2 environments running:

:code:`latest`
   uses :code:`:latest` Docker image and opens at links
   like :code:`baumanka-latest.pashinin.com`.

:code:`production`
   uses Docker images built on tags and opens at hosts
   without :code:`-latest` part like :code:`baumanka.pashinin.com`.

.. warning::

   I wanted to use hosts like :code:`latest.baumanka.pashinin.com` but
   except DNS records I would also need to renew LetsEncrypt certs (to
   include subdomains of a new host
   :code:`*.baumanka.pashinin.com`). Those certs have maximum of 100
   domains. Can lead to multiple certs in future. Also many domains in 1
   cert can lead to performance issues. Just do not want all this
   complexity. :code:`...-latest.pashinin.com` is enough.

.. note::

   In production one must use only static images like "v1.2.3" - no
   "latest", no "stable". Otherwise - no ability to roll back. Also if a
   stable service is restarted a new :code:`latest` docker image will be
   downloaded.

.. note::

   Nomad job names are unique per region. So a job needs to be deployed
   to every region separately.


Routing to different environments
---------------------------------

.. code-block:: text

                                 --> latest
                                /
   User -> baumanka.pashinin.com
                                \
                                 --> production


**Solution 1 (easiest)**. Just have 2 separate domains for each
environment. Like :code:`baumanka.pashinin.com` and
:code:`latest.baumanka.pashinin.com`.

Cons:

#. need to add each subdomain to DNS ().

   Using nsupdate:

   .. code-block:: text

      baumanka.pashinin.com 3600 A 89.179.240.127
      latest.baumanka.pashinin.com 3600 CNAME baumanka.pashinin.com.

#. need to add each subdomain to LetsEncrypt cert which maximum is 100
   domains per cert, so potentially need to have several certs.

   .. code-block:: text

      ...

**Solution 2.** Use Consul Connect routing. But it has it's own cons
like big memory usage with lots of running services, and compatibility
of other programs with it.
