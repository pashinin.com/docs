DNSSEC
======

DS records
----------

.. note::

   You will need to create DS records in your registrar's control panel.
   Like writing NS records for your own DNS servers.

   Example: :code:`63678 5 1 .......`

   * 63678 - Key tag
   * 5 - Algorithm (5 - sha1, 8 - sha256)
   * 1 - Digest Type (1 - sha1)
   * :code:`....` - Digest

To get all this info using KnotDNS:

.. code-block:: text

   keymgr pashinin.com ds
   docker exec -it `docker ps | grep knotd | awk '{print $1}'` keymgr pashinin.com ds


To get all this info using Bind:

.. code-block:: text

   dnssec-dsfromkey /var/lib/bind/pashinin.com/Kpashinin.com.+005+63678.key
   pashinin.com. IN DS 63678 5 1 .......
   pashinin.com. IN DS 63678 5 2 .......


.. note::

   Both keys need to be added to registrar's DNSSEC config.


Ubuntu has such Apparmor configuration that it allows Bind to read zone
files only from /var/lib/bind so keep them there.


1. Move zone to /var/lib/bind/pashinin.com/db file

.. code-block:: text

   # Use this one:
   dnssec-keygen -r /dev/urandom -a RSASHA256 -b 2048 -K /var/lib/bind/pashinin.com pashinin.com


I added to named.local:

.. code-block:: text

   key "update" {
       algorithm hmac-sha512;
       secret "...==";
   };



Test your DNSSEC
----------------

#. http://dnsviz.net


Test DNS
--------

https://mxtoolbox.com/SuperTool.aspx?action=dns%3apashinin.com&run=toolpage
