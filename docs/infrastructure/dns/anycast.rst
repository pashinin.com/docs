DNS for multi-region web site
=============================

.. note::

   There are 2 ways to give DNS answers based on client's location:
   **Anycast** and **GeoDNS**.

.. note::

   **Anycast**. To understand anycast do not think that an IP address
   identifies a unique machine. An IP address only identifies a unique
   machine within one region of a planet. So there can be IP 1.1.1.1 in
   Europe and 1.1.1.1 in US. Then a client :code:`2.2.2.2` wants to
   connect to server :code:`1.1.1.1`. Things responsible for creating a
   route from IP1 to IP2 are *Autonomous Systems (AS)* and *BGP
   protocol*.

   Your Nginx servers in all regions can listen on this IP
   :code:`1.1.1.1` and all users when querying DNS get only this
   IP. *You then configure routes of the Internet so that different
   clients get differnet routes to 1.1.1.1 IP*. Terms to look for:
   *Autonomous Systems (AS)* and *BGP protocol*.

   This is only possible when you own an IP address block :code:`/24`
   and bring it to a provider. This can be costly. Absolutely not
   "my own home server" stuff.

   And... anycast approach has it's mistakes very often.  `Article
   <https://mattgadient.com/i-tested-geodns-vs-anycast-for-websites-so-which-one-is-better/>`_.


GeoDNS answers are based on user’s IP address.


Anycast features:

* Traffic is routed to the server that is the closest (best path)
* When the service host is down, the route is withdrawn and another
  route (another anycast server) is selected.

.. note::

   Each domain has a list of name servers (edit it in your registrator's
   config panel).

.. note::

   https://archive.nanog.org/sites/default/files/nick%20holt.pdf

How to configure DNS for multiple regions? So that users from
America/Europe got an IP address close to America/Europe.
