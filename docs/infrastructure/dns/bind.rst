Bind9
=====











Bind views - different DNS answers for different networks
---------------------------------------------------------

Bind has such feature as views, which allow you to give different
answers to different people based on their IP addresses.

* Alice is in your home network :code:`10.0.0.0/24`, asks for an IP for
  :code:`yourdomain.com`. Give :code:`10.0.0.1`.
* Bob works on Evil Corp. and has IP - :code:`65.55.58.201`. Give hime
  :code:`127.0.0.1`.

What to do:

# Create views in /etc/bind/named.conf.local as shown below (instead of default configs)
# [[Create a zone file for your own domain |Create a zone file "yourdomain.com_internal.db"]] for a local network

.. code-block:: text

   view "10.0.0.0/24" {
       match-clients {
           10.0.0.0/24;
       };

       zone "yourdomain.com" {
           type master;
           file "yourdomain.com_internal.db";
           notify no;
       };

       // ...

       // COPY ALL DEFAULT ZONES HERE
   };

   view "external" {
       zone "yourdomain.com" {
           type master;
           file "yourdomain.com.db";
           notify no;
       };

       // ...

       // COPY ALL DEFAULT ZONES HERE
   };


So you configured 2 views in Bind. Be sure you have created both ".db" files for local and external networks.

Now test DNS server answer from both networks.
