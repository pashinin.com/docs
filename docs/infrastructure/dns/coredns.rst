CoreDNS
=======

.. warning::

   Does not support GeoDNS.

.. note::

   When trying out CoreDNS as your new server start it at 54 port.
   You can test it with this command:

   dig @10.254.239.4 -p 54 pashinin.com


.. toctree::
   :maxdepth: 2
   :caption: Configuration

   first_config
