DNS servers
===========

.. note::

   There are **caching** and **authoritative** DNS servers. You need a
   *caching* DNS server to speed up DNS requests in your LAN. You need
   *authoritative* DNS server to serve your own zone files (for your
   domains).


.. toctree::
   :maxdepth: 2
   :caption: List of DNS servers

   bind
   coredns
   knot_dns



Check your DNS server configuration
-----------------------------------

#. https://zonemaster.labs.nic.cz/
