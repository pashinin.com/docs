DNS tools: dig, kdig
====================



DNS request with fake source IP
-------------------------------

.. note::

   This is useful when testing GeoDNS. Different clients should get
   different answers.

.. code-block:: bash

   kdig @10.254.239.4 -p 54 A pashinin.com +subnet=1.1.1.1
