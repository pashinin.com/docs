DNS dynamic updates
===================

.. note::

   You will need to create a key, configure DNS server to allow updates
   with that key. Then use :code:`nsupdate` or :code:`knsupdate` to
   enter console and do updates.

Create a key
------------

Create a key for DNS updates:

.. code-block:: text

   tsig-keygen -a HMAC-SHA512 update > update.key

.. code-block:: text

   key "update" {
     algorithm hmac-sha512;
     secret "...";
   };

Save full key and secret to Vault
---------------------------------

TODO


Configure Bind9
---------------

You can use :code:`include` option or just paste your key in
:code:`named.conf.local`:

.. code-block:: text

   # include "/etc/bind/keys/update.key";
   key "update" {
       algorithm hmac-sha512;
       secret "...";
   };


nsupdate / knsupdate
--------------------

Save update.key to temp file and enter :code:`nsupdate`:

.. code-block:: bash

   # nsupdate
   vault read -field="value" acme/dns/update.key > /tmp/update.key; \
   nsupdate -k /tmp/update.key; \
   rm /tmp/update.key

   # knsupdate
   echo -ne "hmac-sha512:update:" > /tmp/update.key; vault kv get -field="value" acme/dns/update.secret >> /tmp/update.key; \
   knsupdate -k /tmp/update.key; \
   rm /tmp/update.key

After that you will enter console

.. code-block:: text

   >


Then see :doc:`nsupdate tutorial <./nsupdate>`.


http://kiko.ghost.io/things-i-wish-id-known-about-nsupdate-and-dynamic-dns-updates/
