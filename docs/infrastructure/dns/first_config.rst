CoreDNS first configuration
===========================

Configuration is done in :code:`Corefile` (:code:`/Corefile` - full path
when using Docker).

.. code:: text

   .:54 {
      forward . 8.8.8.8:53
      log
   }

This config will listen on 54 port for all DNS requests and forward them
to Google DNS. All requests will be logged to stdout (:code:`log` plugin).

CoreDNS has more plugins.
