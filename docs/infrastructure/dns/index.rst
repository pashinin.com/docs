DNS
===

.. note::

   **Open 53 port (TCP and UDP).** Most DNS transactions take place
   over UDP.  TCP is always used for zone transfers and is often used
   for messages whose sizes exceed the DNS protocol's original 512-byte
   limit.


.. toctree::
   :maxdepth: 2

   resolver
   dns_servers
   dns_tools
   DNSSEC
   dynamic_updates
   nsupdate
   anycast


.. warning::

   **Disable AXFR zone transfer** after setting up your server!  Without
   it any person can download a full zone. Well any zone should contain
   only public info. But you don't everyone to know that you have
   configured your :code:`top-secret.domain.com`.



.. note::

   **Use TTL=1 week **. The longer your DNS records stay cached (i.e.,
   the longer your TTLs), the more likely your site will stay up even
   when your DNS provider has an outage.

.. warning::

   DNS server alone does not support geo-based DNS answers. If you want
   use geodns https://github.com/abh/geodns




.. note::

   "there are lots of DNS daemons (bind, powerdns, djbdns, etc,etc)."
   For authoritative service, nsd. For a recursive - unbound.


Links
-----

#. https://habr.com/ru/company/oleg-bunin/blog/350550/#bind
