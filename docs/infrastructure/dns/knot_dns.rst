Knot DNS
========

.. note::

   Supports GeoDNS


https://hub.docker.com/r/cznic/knot

Knot console
------------

.. code-block:: bash

   docker run -it --rm -v /tmp/rundir:/rundir cznic/knot knotc


Part of Nomad job
-----------------

.. code-block:: text

   config {
     image = "cznic/knot"
     command = "knotd"
     args = [
       "-c",
       "/config/knot.conf",
     ]
     network_mode = "host"
     port_map {
       dns = 55  # Default: 53
     }
     mounts = [
       {
         type = "volume"
         target = "/storage"
         source = "knotdns-storage"
         readonly = false
       },
     ]
     volumes = [
       "local/config:/config/knot.conf"
     ]
   }

   template {
     data = <<EOH
   server:
       listen: 0.0.0.0@55
       listen: ::@55
   EOH
     destination = "local/config"
   }


GeoIP
-----

Create /etc/GeoIP.conf file:

.. code-block:: text

   # Please see http://dev.maxmind.com/geoip/geoipupdate/ for instructions
   # on setting up geoipupdate, including information on how to download a
   # pre-filled GeoIP.conf, if you are a MaxMind subscriber.
   #
   # You can also have a look at GeoIP.conf(5) ("man GeoIP.conf") for a
   # comprehensive list of configuration options.

   # Enter your account ID and license key below. These are available from
   # https://www.maxmind.com/en/my_license_key. If you are only using free
   # GeoLite databases, you may leave the 0 values.
   #AccountID 0
   UserId XXXXXXXXX
   LicenseKey XXXXXXXXXXXXXXX

   # Enter the edition IDs of the databases you would like to update.
   # Multiple edition IDs are separated by spaces.
   #
   # Include one or more of the following edition IDs:
   # * GeoLite2-City - GeoLite 2 City
   # * GeoLite2-Country - GeoLite2 Country
   # * GeoLite-Legacy-IPv6-City - GeoLite Legacy IPv6 City
   # * GeoLite-Legacy-IPv6-Country - GeoLite Legacy IPv6 Country
   # * 506 - GeoLite Legacy Country
   # * 517 - GeoLite Legacy ASN
   # * 533 - GeoLite Legacy City
   # EditionIDs GeoLite2-Country GeoLite2-City
   ProductIds GeoLite2-ASN GeoLite2-City GeoLite2-Country

Now install geoipupdate and fully automate this process:

.. code-block:: bash

   # Download geoipupdate
   wget https://github.com/maxmind/geoipupdate/releases/download/v4.9.0/geoipupdate_4.9.0_linux_amd64.deb

   # Install it
   dpkg -i geoipupdate_4.9.0_linux_amd64.deb

   # Run
   geoipupdate -v

Create a cron job "/etc/cron.d/geoipupdate":

.. code-block:: text

   # Regular cron job for the geoipupdate package, used to update GeoIP databases
   #
   # According to MaxMind: "The GeoIP2 and GeoIP Legacy Country and City databases
   # are updated every Tuesday. The GeoIP2 ISP, GeoIP Legacy ISP and Organization
   # databases are updated every one to two weeks. All other databases are updated
   # on the first Tuesday of each month."

   # m h dom mon dow user  command
   47 6    * * 3   root    test -x /usr/bin/geoipupdate && /usr/bin/geoipupdate





.. note::

   There are 2 formats of GeoIP databases: .dat and .mmdb. First one is
   "old", second is "new". **KnotDNS uses "new" (.mmdb) format.** When
   given old format file: :code:`failed to open geo DB`.

   /etc/GeoIP.conf

   DBs are saved in :code:`/var/lib/GeoIP/`

   DBs are saved in :code:`/usr/share/GeoIP/GeoLite2-City.mmdb`

   To see actual location run geoipupdate with "-v" flag.

.. code-block:: bash

   cd /usr/share/GeoIP
   mv GeoIP.dat GeoIP.dat.bak
   wget https://dl.miyuru.lk/geoip/maxmind/country/maxmind.dat.gz
   gunzip maxmind.dat.gz
   mv maxmind.dat GeoIP.dat
   mv GeoIPCity.dat GeoIPCity.dat.bak
   wget https://dl.miyuru.lk/geoip/maxmind/city/maxmind.dat.gz
   gunzip maxmind.dat.gz
   mv maxmind.dat GeoIPCity.dat

apt install mmdb-bin

.. code-block:: bash

   # test IP
   mmdblookup --file /var/lib/GeoIP/GeoLite2-Country.mmdb --ip 89.179.240.127 | less


.. note::

   To simulate request from another IP use:
   :code:`+subnet=IP.ADD.RE.SS`.  EDNS must be enabled with
   :code:`edns-client-subnet: true` for this to work.

.. code-block:: text

   # Simulate a DNS request from North America IP (18.188.240.62)
   kdig @10.254.239.4 -p 55 A docs.pashinin.com +subnet=18.188.240.62
   kdig @89.179.240.127 A docs.pashinin.com +subnet=18.188.240.62
