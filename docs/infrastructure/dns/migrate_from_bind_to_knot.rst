Migrate from Bind to KnotDNS
============================

My files in :code:`/var/lib/bind/pashinin.com`:

.. code-block:: text

   -rw-r--r-- 1 bind bind  23K Apr 25 07:35 db
   -rw-r--r-- 1 root root 1.3K Apr 22  2018 db_backup
   -rw-r--r-- 1 bind bind 1.2M Apr 25 07:22 db.jnl
   -rw-r--r-- 1 bind bind  12K May 24  2018 db.signed
   -rw-r--r-- 1 bind bind  14K May 24  2018 db.signed.jnl
   -rw-r--r-- 1 bind root  433 May 24  2018 Kpashinin.com.....key
   -rw------- 1 bind root 1010 May 24  2018 Kpashinin.com.....private
