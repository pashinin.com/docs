nsupdate / knsupdate examples
=============================

https://serverfault.com/questions/733490/bind-nsupdate-l-failed-with-status-update-failed-refused

.. code-block:: bash

   apt install knot-dnsutils


Enter console
-------------

.. code-block:: bash

   # knsupdate (Vault KV v1)
   echo -ne "hmac-sha512:update:" > /tmp/update.key; vault read -address=http://10.254.239.4:8200 -field="value" acme/dns/update.secret >> /tmp/update.key; \
   knsupdate -k /tmp/update.key; \
   rm /tmp/update.key

   # knsupdate (Vault KV v2)
   echo -ne "hmac-sha512:update:" > /tmp/update.key; vault kv get -address=http://10.254.239.4:8200 -field="value" acme/dns/update.secret >> /tmp/update.key; \
   knsupdate -k /tmp/update.key; \
   rm /tmp/update.key


   # nsupdate
   vault read -field="value" acme/dns/update.key > /tmp/update.key; \
   nsupdate -k /tmp/update.key; \
   rm /tmp/update.key


Enter zone and domain to edit
-----------------------------

.. code-block:: text

   server IP.OF.DNS.SERVER 53
   zone example.com

   server pashinin.com 53
   zone pashinin.com


THEN DO CHANGES and commit:

.. code-block:: text

   show
   send


Do not forget to clear cache


Add A record
------------

.. code-block:: text

   update add docs.pashinin.com 3600 A 89.179.240.127
   update add auth.pashinin.com 3600 A 89.179.240.127
   update add admin.pashinin.com 3600 A 89.179.240.127
   update add repetitor-informatika.pashinin.com 3600 A 89.179.240.127
   update add xbs.pashinin.com 3600 A 89.179.240.127
   update add cdn.pashinin.com 3600 A 89.179.240.127
   update add files.pashinin.com 3600 A 89.179.240.127
   update add git.pashinin.com 3600 A 89.179.240.127
   update add nomad.pashinin.com 3600 A 89.179.240.127
   update add vault.pashinin.com 3600 A 89.179.240.127
   update add registry.pashinin.com 3600 A 89.179.240.127
   update add status.pashinin.com 3600 A 89.179.240.127


Add CNAME record
----------------

.. code-block:: text

   update add canary.api.pashinin.com 3600 CNAME api.pashinin.com.
   update delete canary.api.pashinin.com 3600 CNAME api.pashinin.com.

   update add api-latest.pashinin.com 3600 CNAME api.pashinin.com.
   update add auth-latest.pashinin.com 3600 CNAME auth.pashinin.com.
   update add repetitor-informatika-latest.pashinin.com 3600 CNAME repetitor-informatika.pashinin.com.


Add TXT record
--------------

.. code-block:: text

   update add default._domainkey.pashinin.com 3600 TXT "..."
   update add _dmarc.pashinin.com 3600 TXT "v=DMARC1; p=none"

Change record
-------------

.. code-block:: text

   update delete example.com 86400 TXT "v=spf1 a mx ip4:10.0.0.131"
   update add    example.com 86400 TXT "v=spf1 a mx ip4:192.168.0.17"

Delete
------

.. code-block:: text

   update delete default._domainkey.pashinin.com TXT
   update delete qwe.example.com A


Additional notes
----------------

.. note::

   To clear Knot Resolver cache for a domain:

   .. code-block:: text

      socat - UNIX-CONNECT:/run/knot-resolver/control/1
      cache.clear('*.pashinin.com')
      cache.clear('pashinin.com')
      cache.clear()




http://kiko.ghost.io/things-i-wish-id-known-about-nsupdate-and-dynamic-dns-updates/
