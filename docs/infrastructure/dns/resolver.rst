DNS resolver
============

.. note::

   Knot resolver and Unbound are the best recursive DNS resolvers
   according to `this
   <https://www.redpill-linpro.com/techblog/2019/08/27/evaluating-local-dnssec-validators.html#summary>`_.

.. note::

   **Forwarding DNS requests to your provider is NOT a DNS resolver.** A
   long time ago I configured Bind to forward DNS requests to my
   provider's DNS resolver.  I thought I configured my local DNS
   resolver. No, that is called a *caching DNS server*. It just
   remembered (for some time) a DNS answer from my provider's resolver.

.. warning::

   Do not use Docker for DNS resolvers for performance and security
   reasons. Just install a resolver on bare metal. Anyway you need only
   one per datacenter.

   *"The images are meant as an easy way to try knot-resolver, and
   they're not designed for production use."*


.. note::

   Resolver itself can listen on any port (1053 for example). Just
   configure your caching DNS server (Bind or CoreDNS) to forward
   requests to your resolver. Or just use 53 port without any other
   servers.


.. note::

   Performance of one DNS query :code:`dig google.com` through CoreDNS
   :code:`forward` running in Docker using different DNS resolvers:

   #. Local (Knot resolver) - 1.5ms
   #. Almost local (ISP Beeline) - 2ms
   #. Yandex - 6ms
   #. Google - 20ms

   Direct DNS query :code:`google.com` to Knot resolver - 0.5ms.

.. toctree::
   :maxdepth: 1

   resolver_knot
