Knot resolver
=============

Install
-------

Install `instructions
<https://software.opensuse.org//download.html?project=home%3ACZ-NIC%3Aknot-resolver-latest&package=knot-resolver>`_
(taken from `readme <https://github.com/CZ-NIC/knot-resolver>`_):

For Ubuntu 20.04:

.. code-block:: bash

   echo 'deb http://download.opensuse.org/repositories/home:/CZ-NIC:/knot-resolver-latest/xUbuntu_20.04/ /' | sudo tee /etc/apt/sources.list.d/home:CZ-NIC:knot-resolver-latest.list
   curl -fsSL https://download.opensuse.org/repositories/home:CZ-NIC:knot-resolver-latest/xUbuntu_20.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home_CZ-NIC_knot-resolver-latest.gpg > /dev/null
   sudo apt update
   sudo apt install knot-resolver

Configure
---------

Edit :code:`/etc/knot-resolver/kresd.conf` and set whatever ports and
addresses you need. Default ports: 53 and 853.

.. code-block:: text

   net.listen('127.0.0.1', 53, { kind = 'dns' })
   net.listen('127.0.0.1', 853, { kind = 'tls' })
   net.listen('::1', 53, { kind = 'dns', freebind = true })
   net.listen('::1', 853, { kind = 'tls', freebind = true })

.. note::

   To run Knot resolver alongside with Bind - set Bind to listen only on
   external IP :code:`89.179.240.127:53` and Knot resolver - only on internal
   IP :code:`10.254.239.4:53` and localhost.



`Configure
<https://knot-resolver.readthedocs.io/en/stable/modules-policy.html#replacing-part-of-the-dns-tree>`_
forwarding of :code:`.consul` domain:

Add :code:`'policy'` to modules and add following to config:

.. code-block:: text

   -- extraTrees = policy.todnames({'faketldtest', 'sld.example', 'internal.example.com'})
   extraTrees = policy.todnames({'consul'})
   -- Beware: the rule order is important, as STUB is not a chain action.
   policy.add(policy.suffix(policy.FLAGS({'NO_CACHE'}),   extraTrees))
   policy.add(policy.suffix(policy.STUB({'127.0.0.1@8600'}), extraTrees))

   -- Local development
   -- policy to change IPv4 address and TTL for example.com
   policy.add(
       policy.domains(
           policy.ANSWER(
               { [kres.type.A] = { rdata=kres.str2ip('127.0.0.1'), ttl=300 } }
           ), { todname('pashinin.test') }))



Start
-----

To start 1 (or 4) kresd instance and enable it at boot:

.. code-block:: bash

   systemctl enable --now kresd@1.service
   systemctl enable --now kresd@{1..2}.service
   systemctl status kresd@{1..2}.service
   systemctl restart kresd@{1..2}.service


Test
----

.. code-block:: bash

   kdig @10.254.239.4 -p 53 A ya.ru


How to enter Knot Resolver console
----------------------------------

.. code-block:: bash

   socat - UNIX-CONNECT:/run/knot-resolver/control/1

Where :code:`1` is a running instance depending on how many of them you
started.

Logs
----

Knot resolver does not log almost anything by default (except errors).


Metrics
-------

Add following lines:

.. code-block:: text

   net.listen('10.254.239.4', 8453, { kind = 'webmgmt' })

   modules = {
     ...
     'http',             -- for Prometheus endpoint /metrics
     ...
   }

   -- Set Prometheus namespace
   http.prometheus.namespace = 'resolver_'

Then in Grafana just import existing dashboard. It is already configured
for `resolver_` prefix.



Problems after installation
---------------------------

**Problem 1**

Knot resolver forgets dns answer quickly. After a few minutes - a new
request is made.



**Problem 2**

Need some domains to be always up-to-date (like google.com and
yandex.ru). Not a single slow request. They must be always in cache.


**Main problem - Incorrect answers**

Incorret resolve. At some point Knot Resolver started to give answers
that differ with other DNS servers. For example answer for
`hub.docker.com` had incorret IP addresses and I had no access to it.

I manually configured **very** long cache time. DNS answers changed
and I still got them from cache.

Also I updated system packages. After a few kresd restarts it was
fixed. (I think cache was cleared)

PROBLEM APPEARED AGAIN. A record for `registry.terraform.io` at kresd
and 8.8.8.8 is different.


**Slow requests (IPv4 only)**

.. warning::

   IPv4 and IPv6 protocols are used by default. For performance reasons
   it is recommended to explicitly disable protocols which are not
   available on your system. (`Docs
   <https://knot-resolver.readthedocs.io/en/stable/daemon-bindings-net_client.html>`_)

Solution: add following lines to /etc/knot-resolver/kresd.conf

.. code-block:: text

   -- Fix slow requests
   net.ipv6 = false


**DNSSEC validation failure habr.com**

When I restarted KRESD - after a few hours it was fixed.

Sep 25 08:25:42 balancer1 kresd[3345674]: [ta_update] refreshing TA for .
Sep 25 08:25:42 balancer1 kresd[3345674]: [ta_update] next refresh for . in 12 hours

https://www.icann.org/dns-resolvers-updating-latest-trust-anchor

See trust_anchors


Full config
-----------

.. code-block:: text

   -- -*- mode: lua -*-

   -- vim:syntax=lua:set ts=4 sw=4:
   -- Refer to manual: https://knot-resolver.readthedocs.org/en/stable/

   -- Network interface configuration
   -- net.listen('127.0.0.1', 53, { kind = 'dns' })
   -- net.listen('127.0.0.1', 1853, { kind = 'tls' })
   net.listen('10.254.239.4', 53, { kind = 'dns' })
   net.listen('10.254.239.4', 853, { kind = 'tls' })
   -- net.listen('::1', 53, { kind = 'dns', freebind = true })
   -- net.listen('::1', 1853, { kind = 'tls', freebind = true })
   net.listen('10.254.239.4', 8453, { kind = 'webmgmt' })


   -- Load useful modules
   modules = {
   	'hints > iterate',  -- Load /etc/hosts and allow custom root hints
   	'stats',            -- Track internal statistics
       'http',             -- for Prometheus endpoint /metrics
   	-- 'predict',          -- Prefetch expiring/frequent records
       predict = {
           -- window = 30, -- 30 minutes sampling window
           -- period = 24*(60/30) -- track last 24 hours
           window = 30, -- 30 minutes sampling window
           period = 7*24*(60/30) -- track last 7 days
       },
   }

   -- My config

   -- Cache size
   -- cache.size = 100 * MB
   cache.size = cache.fssize() - 10*MB
   -- cache.min_ttl(24*60*60)


   -- extraTrees = policy.todnames({'faketldtest', 'sld.example', 'internal.example.com'})
   extraTrees = policy.todnames({'consul'})
   -- Beware: the rule order is important, as STUB is not a chain action.
   policy.add(policy.suffix(policy.FLAGS({'NO_CACHE'}),   extraTrees))
   policy.add(policy.suffix(policy.STUB({'127.0.0.1@8600'}), extraTrees))

   -- Local development
   -- All *.pashinin.test subdomains -> 127.0.0.1
   policy.add(
       policy.suffix(
           policy.ANSWER(
               { [kres.type.A] = { rdata=kres.str2ip('127.0.0.1'), ttl=300 } }
           ), policy.todnames({'pashinin.test'})))



   -- Set Prometheus namespace
   http.prometheus.namespace = 'resolver_'

   -- Fix slow requests
   net.ipv6 = false
