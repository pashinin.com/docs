DNS time-to-live (TTL)
======================

If you plan to change DNS records:

#. change TTL to a low value (5 mins)
#. make changes
#. change TTL back to high values (1 day)

.. note::

   I used a long TTL (hours, days). But sites like google.com and
   yandex.ru use 5 min TTL.
