firewalld
=========

.. note::

   All changes in firewalld are temporary. Thats good if something goes
   wrong. To make them permanent run
   :code:`firewall-cmd --runtime-to-permanent`.


firewall-cmd --list-all
firewall-cmd --list-all --zone=external

To remove just replace "add-source" to "remove-source" on so on.

Priority:

1. direct rules
2. ip / network addresses
3. interface



Moscow - node1 (wifi router)
----------------------------

zone "external":

.. code-block:: bash

   firewall-cmd --zone=trusted --add-interface=enp3s0
   firewall-cmd --zone=trusted --add-interface=wlan0




Moscow - balancer
-----------------

zone "public":

.. code-block:: bash

   #firewall-cmd --zone=external --add-interface=enp4s0


zone "trusted":

.. code-block:: bash

   # LAN interface
   firewall-cmd --zone=trusted --add-interface=enp4s0

   # ???
   firewall-cmd --zone=trusted --add-source=10.254.240.0/24
   firewall-cmd --zone=trusted --add-source=195.181.245.180/32


   # for my Wi-fi LAN to have access to remote servers.
   # My Wi-fi network is in trusted zone. So I added MASQ to trusted zone.
   firewall-cmd --zone=trusted --add-masquerade


   # ACCEPT all
   # firewall-cmd --permanent --zone=internal --set-target=ACCEPT

   # forward ssh (for gitea in LAN)
   firewall-cmd --zone=trusted --add-forward-port=port=22:proto=tcp:toport=2222:toaddr=10.254.239.5


zone "external":

.. code-block:: bash

   # for public NIC
   #
   # MASQ must be enabled on external networking device (which has public IP)
   #
   # Firewalld "external" zone has MASQ enabled by default.
   # So adding my external interface to this zone.
   firewall-cmd --zone=external --add-interface=enp3s0

   firewall-cmd --add-service=dns --zone=external
   firewall-cmd --add-service=http --zone=external
   firewall-cmd --add-service=https --zone=external

   # 993 (IMAPS), 25 (SMTP), 465 (SMTPS), 587 (smtp-submission)
   firewall-cmd --add-service=imaps --zone=external
   firewall-cmd --add-service=smtp --zone=external
   firewall-cmd --add-service=smtps --zone=external
   firewall-cmd --add-service=smtp-submission --zone=external

   firewall-cmd --add-service=mosh --zone=external

   # set it as default
   firewall-cmd --set-default-zone=external

   # OpenVPN
   firewall-cmd --add-service=openvpn --zone=external
   # Wireguard
   firewall-cmd --add-port=51800/udp
   # Custom SSH port
   #firewall-cmd --add-port=2222/tcp

   # forward 51413 port (torrents) to 1 node
   firewall-cmd --add-forward-port=port=51413:proto=tcp:toport=51413:toaddr=10.254.239.6
   firewall-cmd --add-forward-port=port=51414:proto=tcp:toport=51414:toaddr=10.254.239.2

   # forward ssh (for gitea)
   firewall-cmd --add-forward-port=port=22:proto=tcp:toport=2222:toaddr=10.254.239.5

   # forward 22000 (syncthing) to node3
   firewall-cmd --add-forward-port=port=22000:proto=tcp:toport=22000:toaddr=10.254.239.3





Services
--------

firewalld services are not the same as /etc/services.

They are located at /usr/lib/firewalld/services/



Vilnius
-------

firewall-cmd --zone=trusted --add-source=89.179.240.127/32

firewall-cmd --add-service=openvpn
firewall-cmd --add-masquerade
firewall-cmd --direct --add-rule ipv4 filter FORWARD 0 -i tun0 -o ens0 -j ACCEPT
firewall-cmd --direct --add-rule ipv4 nat POSTROUTING 0 -o ens3 -j MASQUERADE

# Whitelist ONLY Moscow DC (do not do this, for mobile VPN clients)
# firewall-cmd --add-source=89.179.240.127

# Allow access to Consul from Moscow DC
# Why do I still need to use this rule!? Even when I whitelisted whole IP above.
firewall-cmd --add-port=8302/tcp
firewall-cmd --add-rich-rule='rule family="ipv4" source address="89.179.240.127/32" port protocol="tcp" port="8302" accept'

firewall-cmd --runtime-to-permanent



Rest
----

firewall-cmd --zone=trusted --add-source=195.181.245.180/32
