Masquerade
==========

Masquerading replaces source ip address.


`curl registry.terraform.io` works from balancer but hangs from any LAN PC:

.. code-block:: text

   curl -vvvv registry.terraform.io
   *   Trying 151.101.86.49:80...
   * TCP_NODELAY set
   * Connected to registry.terraform.io (151.101.86.49) port 80 (#0)
   > GET / HTTP/1.1
   > Host: registry.terraform.io
   > User-Agent: curl/7.68.0
   > Accept: */*
   >

So it even connects but does not get any data. The problem is
`masquerade` is enabled on router and all other sites are working
perfectly.


firewall-cmd --zone=trusted --remove-masquerade


Detect problems with masquerade
-------------------------------

.. code-block:: bash

   # -n  Don't convert addresses (i.e., host addresses, port numbers, etc.) to names.
   #
   # 151.101.86.49 - registry.terraform.io
   # 13.33.243.0/24 - releases.hashicorp.com
   #
   tcpdump -i enp3s0 net 13.33.243.0/24 -n -s 0 -vvv
   tcpdump net 151.101.86.0/24 -n -vvv -w dump
   
   tcpdump -i enp3s0 host 13.33.243.0/24 -n -s 0 -vvv -w dump
   tcpdump -i enp3s0 dst 151.101.86.49 -n -s 0 -vvv -w dump
   
