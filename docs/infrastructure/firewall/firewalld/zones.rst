Zones
=====

By default all interfaces and source addresses belong to public zone.

.. code-block:: text

   firewall-cmd --get-zones
   block dmz drop external home internal libvirt public trusted work


#. Trusted

   This zone allows all incoming traffic. On trust scale, it stands on
   first position. Use this zone to handle the traffic on which you can
   trust blindly because it filters nothing.

#. Home

   This zone is customized for home network. It allows return traffic
   with following services ssh, mdns, ipp-client, samba-client and
   dhcpv6-client.

#. Internal

   This zone is similar to home zone but it is customized for internal
   network. It also allows return traffic with following services ssh,
   mdns, ipp-client, samba-client and dhcpv6-client.

#. Work

   This zone is customized for work network. It allows return traffic
   with following services ssh, ipp-client and dhcpv6-client.

#. Public

   This zone is customized for public network. It allows return traffic
   with following services ssh and dhcpv6-client. This is the default
   zone unless you change the setting.

#. External

   This zone is customized for masquerading. It allows return traffic
   and ssh service only.

#. Dmz

   This zone is customized to limit the access to internal network. It
   allows return traffic and ssh service only.

#. Block

   This zone rejects all incoming traffic with “icmp-host-prohibited”
   message. It allows only return traffic. On trust scale it stands on
   second last position.

#. Drop

   This zone rejects all incoming traffic without sending any error
   message. It allows only return traffic. On trust scale it stands on
   last position.


.. code-block:: text

   firewall-cmd --list-all
   firewall-cmd --list-all --zone=external
