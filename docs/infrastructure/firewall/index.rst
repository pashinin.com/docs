Firewall
========

.. warning::

   All firewall solutions here are software. Which is slow.

.. note::

   Debian recommends using frewalld instead of writing rules in
   nftables.

.. toctree::
   :maxdepth: 1

   firewalld/index
   nftables
   qos/index
