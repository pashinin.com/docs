nftables
========

List rules:

.. code-block:: bash

   nft list tables [<family>]
   # family - one of the following table types: ip, arp, ip6, bridge, inet, netdev.

   nft list tables ip
