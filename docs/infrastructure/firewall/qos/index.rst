QoS (quality of service) on Linux
=================================

QoS is about giving a **guaranteed** bandwidth to all nodes. All
bandwidth can still be used when other nodes are inactive. It is called
"traffic shaping".

In general QoS is about classifying the network traffic into categories,
and differentiating the handling of traffic according to which category
it belongs to.

The program we need is `tc` (traffic control).

.. note::

   You can shape only an outgoing traffic on an interface. Not incoming.


.. note::

   To limit incoming traffic (download speed) - IFB is used.


.. note::

   **Problem: 1 node blocks others**

   I have a network with 3 nodes (#1, #2, #3). When I download a big ISO
   file (several gigabytes) on node #2 - all other nodes (#1 and #3) can
   not get access to internet at all. All bandwidth was used by node #2.


.. toctree::
   :maxdepth: 2

   tc
