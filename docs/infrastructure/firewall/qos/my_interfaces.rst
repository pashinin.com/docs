QoS on my interfaces
====================

Upload on enp3s0 89.179.240.127/24
----------------------------------

High priority:

#. src interface: "dron"


Medium priority:

#. http server (src ports: 80, 443)


.. code-block:: bash

   IF=enp3s0
   tc qdisc add dev "$IF" root handle 1: htb default 20;







Download on enp3s0 89.179.240.127/24
------------------------------------

#. System updates (from specific IPs)
#. Big file downloads (from any IP)


.. code-block:: bash

   # Redirect incoming traffic to ifb0
   tc qdisc add dev "$IFACE" handle ffff: ingress
   tc filter add dev "$IFACE" parent ffff: protocol ip u32 match u32 0 0 \
       action mirred egress redirect dev "$IFB";


dron interface
--------------
