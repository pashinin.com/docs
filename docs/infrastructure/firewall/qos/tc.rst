tc (traffic control)
====================

https://habr.com/ru/post/420525/

:code:`tc` program is used for traffic shaping (see `QoS <../>`_).

:code:`enp3s0` - external IP ISP interface (100 Mbit / 100 Mbit)


How to test (generate traffic)
------------------------------

Use iperf3

.. code-block:: bash

   sudo apt install iperf3


.. code-block:: bash

   # server
   iperf3 -s

   # send data from client to server
   iperf3 -c 192.168.122.175

   # reverse: from server to client
   iperf3 -c 192.168.122.175 -R


How tc works
------------

All network interfaces have a "root" queue by default.


Statistics
----------

.. code-block:: bash

   tc qdisc show

.. code-block:: text

   qdisc noqueue 0: dev lo root refcnt 2
   qdisc fq_codel 0: dev enp3s0 root refcnt 2 limit 10240p flows 1024 quantum 1514 target 5ms interval 100ms memory_limit 32Mb ecn drop_batch 64
   qdisc fq_codel 0: dev enp4s0 root refcnt 2 limit 10240p flows 1024 quantum 1514 target 5ms interval 100ms memory_limit 32Mb ecn drop_batch 64
   qdisc fq_codel 0: dev tun0   root refcnt 2 limit 10240p flows 1024 quantum 1500 target 5ms interval 100ms memory_limit 32Mb ecn drop_batch 64
   qdisc noqueue 0: dev docker0 root refcnt 2
   qdisc noqueue 0: dev nomad root refcnt 2
   qdisc noqueue 0: dev veth7450d3bf root refcnt 2
   qdisc noqueue 0: dev vethc17a4a15 root refcnt 2
   qdisc noqueue 0: dev dron root refcnt 2
   qdisc noqueue 0: dev test root refcnt 2
   ...

:code:`qdisc [name]` - "queueing discipline [name]"

:code:`fq_codel` — один из самых эффективных и современных алгоритмов,
использующий AQM (Active Queue Management). Fair Queuing Controlled
Delay.


:code:`refcnt 2`

:code:`limit 10240p` - hard limit on the real queue size in packets.

:code:`interval 100.0ms` -


Other commands
--------------

.. code-block:: bash

   tc qdisc show

   # Clear out any existing tc policies
   tc qdisc del dev enp1s0 root

   # Add a root queue on interface
   tc qdisc add dev enp1s0 root handle 1: fq_codel
   tc qdisc add dev enp1s0 root handle 1: htb default 20
   # default 20 - used when creating "1:20" class


   # Set the upload to 8Mbps and burstable to 10mbps
   tc qdisc add dev enp1s0 root tbf rate 8mbit burst 10mbit latency 50ms

   tc class add dev enp1s0 parent 1: classid 1:1 htb rate 11mbit burst 15k

   # Set the download to 70Mbps and burstable to 80Mbps
   tc qdisc add dev enp5s0 root tbf rate 70mbit burst 80mbit latency 50ms


tc qdisc
--------

.. code-block:: bash

   tc qdisc show
   tc qdisc del dev enp3s0 root


tc class
--------

.. code-block:: bash

   tc class show dev enp3s0
   class htb 1:1 root rate 100Mbit ceil 100Mbit burst 1600b cburst 1600b
   class htb 1:10 parent 1:1 leaf 10: prio 1 rate 20Mbit ceil 95Mbit burst 1600b cburst 1579b
   class htb 1:20 parent 1:1 leaf 20: prio 2 rate 40Mbit ceil 95Mbit burst 1600b cburst 1579b
   class htb 1:30 parent 1:1 leaf 30: prio 3 rate 20Mbit ceil 90Mbit burst 1600b cburst 1586b


tc filter
---------

Use it to pass specific traffic to a class.

.. code-block:: text

   tc filter show dev enp3s0
   filter parent 1: protocol ip pref 10 u32 chain 0
   filter parent 1: protocol ip pref 10 u32 chain 0 fh 800: ht divisor 1
   filter parent 1: protocol ip pref 10 u32 chain 0 fh 800::800 order 2048 key ht 800 bkt 0 flowid 1:10 not_in_hw
     match 00100000/00ff0000 at 0
   filter parent 1: protocol ip pref 11 u32 chain 0
   filter parent 1: protocol ip pref 11 u32 chain 0 fh 801: ht divisor 1
   filter parent 1: protocol ip pref 11 u32 chain 0 fh 801::800 order 2048 key ht 801 bkt 0 flowid 1:10 not_in_hw
     match 00010000/00ff0000 at 8
   filter parent 1: protocol ip pref 12 u32 chain 0
   filter parent 1: protocol ip pref 12 u32 chain 0 fh 802: ht divisor 1
   filter parent 1: protocol ip pref 12 u32 chain 0 fh 802::800 order 2048 key ht 802 bkt 0 flowid 1:10 not_in_hw
     match 00060000/00ff0000 at 8
     match 05000000/0f00ffc0 at 0
   filter parent 1: protocol ip pref 18 u32 chain 0
   filter parent 1: protocol ip pref 18 u32 chain 0 fh 803: ht divisor 1
   filter parent 1: protocol ip pref 18 u32 chain 0 fh 803::800 order 2048 key ht 803 bkt 0 flowid 1:20 not_in_hw
     match 00000000/00000000 at 16


"u32" selector can check any part of a packet.

Examples:

.. code-block:: bash

   # TOS (type-of-service) Minimum Delay (ssh, NOT scp) in 1:10:
   tc filter add dev "$IFACE" parent 1: protocol ip prio 10    u32 match ip tos 0x10 0xff  flowid 1:10;


.. code-block:: bash

   # TOS Minimum Delay (ssh, NOT scp) in 1:10:
   tc filter add dev "$IFACE" parent 1: protocol ip prio 10    u32 match ip tos 0x10 0xff  flowid 1:10;


Save after reboot
-----------------

No way to save settings. Use your own script.
