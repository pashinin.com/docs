Consul
======

.. warning::

   Nodes in the same datacenter (DC) should be on a single LAN (1ms ping).

.. warning::

   Data is not replicated between different datacenters in Consul.


Init new cluster
----------------

Remove /var/consul folder and add `bootstrap=3` in each server
config.

Run all servers.

Remove `bootstrap=3`


Drain/Remove node
-----------------

Run it on node that will go away (gracefully):

.. code-block:: bash

   consul leave

Run it to remove failed node from cluster:

.. code-block:: bash

   consul force-leave <NODE NAME>

   consul force-leave [node-name].[datacenter]


Add a new datacenter to cluster
-------------------------------

1. Just make that new Consul cluster running separately

.. note::

   Do not forget to uncomment for the first time:

   # bootstrap = true
   # bootstrap_expect = 1

.. code-block:: bash

   # On a node in a new datacenter
   sudo apt install python-pip unzip
   sudo -H pip install ansible

   # Install Consul as a single node cluster
   # In my provision repo - to install on AWS:
   # (change IP as you need)
   ansible-playbook consul.yml -i hosts --user 'ubuntu' --become-user 'root'  --limit '3.17.27.156' --extra-vars "mode=server" --extra-vars "datacenter=us-east-2" --check

After that there may be an error in logs: failed to sync remote state:
No cluster leader.  That's not important. Go on.

.. warning::

   Do not forget to open 8302 port in both datacenters for each other
   and allow them to connect. Check firewall.

   # on existing balancer (allow access from remote.dc.ip.addr)
   -A INPUT -s remote.dc.ip.addr -p tcp -m tcp --dport 8302 -j ACCEPT
   -A INPUT -s 18.188.240.62 -p tcp -m tcp --dport 8302 -j ACCEPT



2. On a new remote node join to existing cluster:

.. code-block:: bash

   # connects to port 8302 when using "-wan" (unblock it in firewall)
   consul join -wan IP.OF.EXISTING.CLUSTER
   consul join -wan 89.179.240.127

   # check
   consul members -wan


.. note::

   If some Consul Servers in existing cluster do not have a real IP (WAN
   address) you may want to disable WAN traffic for them. By default all
   "server" nodes may try to connect to other datacenters. In my case
   only 1 Consul server from DC had an external IP, so I disabled WAN
   negotiation for other servers except this one. This is done by adding
   following in config:

   .. code-block:: text

      ports {
        serf_wan = -1
      }

3. Configure DNS resolver for .consul domain on AWS

   :code:`systemd-resolved` (default, 127.0.0.53) has problems. It is
   impossible to set port for forwarder (always 53, consul uses
   8600). So stop and disable it via systemctl.

   apt install dnsmasq

   vim /etc/resolv.dnsmasq
   nameserver 169.254.169.253

   vim /etc/dnsmasq.conf
   listen-address=127.0.0.1
   resolv-file=/etc/resolv.dnsmasq


Register/deregister service in Consul (manually)
------------------------------------------------

**Example 1. Ceph RGW service (HTTP)**

To register RGW service working on node "10.254.239.3" in Consul:

.. code-block:: bash

   curl -X PUT -d '{ "Name": "s3", "Port": 7480, "Tags": [ "urlprefix-s3.pashinin.com/" ], "Check": { "name": "rgw health check", "id": "s3", "ServiceID": "s3", "HTTP": "http://localhost:7480", "interval": "10s", "timeout": "2s" } }' http://10.254.239.3:8500/v1/agent/service/register

Service will also appear in Fabio load balancer.

To deregister:

.. code-block:: bash

   curl -X PUT http://10.254.239.3:8500/v1/agent/service/deregister/s3



**Example 2. Nginx (old site)**

.. code-block:: bash

   # Register as "old-site-http" (HTTP)
   curl -X PUT -d '{ "Name": "old-site-http", "Port": 80, "Tags": [ "urlprefix-baumanka.pashinin.com:443/", "urlprefix-baumanka.pashinin.com:80/ redirect=301,https://baumanka.pashinin.com$path" ], "Check": { "name": "old site health check", "ServiceID": "old-site-http", "TCP": "10.254.239.1:80", "interval": "10s", "timeout": "2s" } }' http://10.254.239.1:8500/v1/agent/service/register

   # Deregister
   curl -X PUT http://10.254.239.1:8500/v1/agent/service/deregister/old-site-http

   # Test using your domain:
   curl -H "Host: pashinin.com" http://10.254.239.4:85/
   curl -H "Host: pashinin.com" http://10.254.239.4:86/



**Example 3. Nomad**

.. code-block:: bash

   # Register as "nomad-ui" (HTTP)
   curl -X PUT -d '{ "Name": "nomad-ui", "Port": 4646, "Tags": [ "urlprefix-nomad.pashinin.com:443/ allow=ip:10.254.239.0/24", "urlprefix-nomad.pashinin.com:80/ redirect=301,https://nomad.pashinin.com$path" ], "Check": { "name": "nomad-ui check", "ServiceID": "nomad-ui", "TCP": "10.254.239.4:4646", "interval": "10s", "timeout": "2s" } }' http://10.254.239.4:8500/v1/agent/service/register

   # Deregister
   curl -X PUT http://10.254.239.4:8500/v1/agent/service/deregister/nomad-ui

   # Test using your domain:
   curl -H "Host: nomad.pashinin.com" http://10.254.239.4:4646/


**Example 4. Vault**

.. code-block:: bash

   # Register as "vault-ui" (HTTP)
   curl -X PUT -d '{ "Name": "vault-ui", "Port": 8200, "Tags": [ "urlprefix-vault.pashinin.com:443/ allow=ip:10.254.239.0/24", "urlprefix-vault.pashinin.com:80/ redirect=301,https://vault.pashinin.com$path" ], "Check": { "name": "vault-ui check", "ServiceID": "vault-ui", "TCP": "10.254.239.4:8200", "interval": "10s", "timeout": "2s" } }' http://10.254.239.4:8500/v1/agent/service/register

   # Deregister
   curl -X PUT http://10.254.239.4:8500/v1/agent/service/deregister/vault-ui

   # Test using your domain:
   curl -H "Host: vault.pashinin.com" http://10.254.239.4:8200/
