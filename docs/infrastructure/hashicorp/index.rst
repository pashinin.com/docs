Hashicorp stack
===============

I have Consul, Nomad and Vault running on bare metal (deployed with
Ansible). All other services are started by Nomad.


Dependencies
------------

Consul has no dependencies.

Vault needs Consul (if using Consul as a storage).

Nomad needs Vault.


Modes (server/client)
---------------------

Consul - 3 servers per DC, clients on all other nodes.

Vault - 3 servers per DC.

Nomad - 1 "server+client" per DC and clients on all other nodes.



.. toctree::
   :maxdepth: 2
   :caption: Contents

   consul/index
   vault/index
   nomad/index
   service_mesh
