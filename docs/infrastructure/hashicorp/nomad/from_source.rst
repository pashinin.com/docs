Build Hashicorp Nomad from source
=================================

.. warning::

   Clone *your* "nomad" repo only into following path!

   git clone ... ~/go/src/github.com/hashicorp/nomad

.. code-block:: bash

   cd ~/go/src/github.com/pashinin/nomad
   make bootstrap
   make deps
   # PATH=$PATH:~/go/bin
   make generate-structs
   make pkg/linux...
