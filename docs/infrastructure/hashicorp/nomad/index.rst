Hashicorp Nomad
===============

.. note::

   Nomad servers in 1 region are expected to have sub 10 millisecond
   network latencies.

   Nomad clients can have 100+ millisecond latency to their servers.

.. warning::

   Hashicorp Vault multi datacenter feature is a part of Enterprise
   version.

.. note::

   Nomad server is responsible for scheduling.
   Nomad client is responsible for running workloads.


.. code-block:: bash

   nomad server members
   # nomad server force-leave desktop.europe
   nomad node status
   nomad node status -self
   nomad node status -self -stats | less

   nomad plan jobs/certs.nomad  # preview what will happen
   nomad run jobs/certs.hcl     # run actually
   nomad job status certs       # see details
   nomad job stop certs         # stop <job>
   nomad run -force jobs/certs.nomad     # run periodic job immediately

   # view logs
   nomad status certs  # see allocs there
   nomad alloc logs 82a0cb48   # get STDOUT
   nomad alloc logs -stderr 82a0cb48   # get STDERR




Remove node from Nomad
----------------------

.. code-block:: bash

   # list nodes
   nomad node status

   # drain node (delete temporarily)
   nomad node drain -enable cedfab9b

   # garbage collect (stop listing this node)
   # actually do not need it - Nomad will "forget" it later
   curl --request PUT http://localhost:4646/v1/system/gc

.. warning::

   **Last resort!**

   :code:`dig nomad.services.consul` still lists left node (that was
   days down). I want to remove it. From Consul UI I noticed strange
   thing: one live node had "Nomad" services of left node. So I went to
   this node in :code:`/var/consul/services` and removed all files that
   contained IP address of left node. That helped.
