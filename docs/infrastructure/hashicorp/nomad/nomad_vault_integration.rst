Nomad-Vault integration
-----------------------

Taken from `here
<https://www.nomadproject.io/docs/vault-integration/index.html>`_. Token
(for a current task) creation is delegated to a trusted service such as
Nomad. So Nomad servers must be provided a Vault token (that can create
tokens).

1. Create a "nomad-server" policy to create and manage tokens:


.. code-block:: bash

   vault operator unseal -address=http://0.0.0.0:8200

   # Download the policy
   curl https://nomadproject.io/data/vault/nomad-server-policy.hcl -O -s -L

   # Write the policy to Vault
   vault policy write -address=http://0.0.0.0:8200 nomad-server nomad-server-policy.hcl
   Success! Uploaded policy: nomad-server

2. Create a Vault token role

.. code-block:: bash

   # Download the token role
   curl https://nomadproject.io/data/vault/nomad-cluster-role.json -O -s -L

   # Create the token role with Vault
   vault write -address=http://0.0.0.0:8200 /auth/token/roles/nomad-cluster @nomad-cluster-role.json
   Success! Data written to: auth/token/roles/nomad-cluster

3. Configure Nomad to use the created token role

.. code-block:: bash

   vault token create -address=http://127.0.0.1:8200 -policy nomad-server -period 72h -orphan
   vault token create -address=http://10.254.239.4:8200 -policy nomad-server -period 72h -orphan

   vault token create -address=http://127.0.0.1:8200 -policy nomad-server -period 72h -orphan -wrap-ttl=60s

   ...
   token          xxxxxx-xxxxxxx-xxxxxx-xxxx-xxxxxx
   ...

*Users should set the VAULT_TOKEN environment variable when starting the
agent instead.*

So store this token in /etc/systemd/system/nomad.service.d/env.conf
which is read-only by root. Manually edit this file and write your token
there. For 3 Nomad servers.


.. note::

   From systemd logs:

   .. code-block:: text

      Configuration file /etc/systemd/system/nomad.service.d/env.conf is
      marked world-inaccessible. This has no effect as configuration
      data is accessible via APIs without restrictions. Proceeding
      anyway.



.. code-block:: text

   Vault: failed to derive vault token: Error making API request. URL:
   PUT http://127.0.0.1:8200/v1/sys/wrapping/unwrap Code: 400. Errors: *
   wrapping token is not valid or does not exist

   This error happened when a Nomad client used a local Vault server but
   tokens were supplied from different Nomad+Vault server from
   another DC.
