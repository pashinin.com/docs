Nomad: remove node from cluster
===============================

First drain node. You can do this from the UI.


nomad node drain [options] <node>

nomad node drain -enable -self



After nothing is running on that node - remove it from raft peer:

.. code-block:: text

   nomad operator raft list-peers
   nomad operator raft remove-peer -peer-id=cc501056-9754-82ee-39d0-a1a9c258a52f
