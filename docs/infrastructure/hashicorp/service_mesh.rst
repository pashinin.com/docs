Service mesh using Consul (Hashicorp)
=====================================

Moving your infrastructure to service mesh
------------------------------------------

https://gitlab.com/pashinin.com/terraform/-/issues/12

.. note::

   You need this section when having services inside and outside of a
   service mesh (always).

   Example: load balancer (runs inside a service mesh) needs to access
   Consul which runs not inside a Service mesh.

   To do this you need a "terminating gateway". Nomad supports only
   Ingress Gateway for now.

   Waiting...
