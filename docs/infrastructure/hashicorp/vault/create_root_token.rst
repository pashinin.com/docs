Create root token
=================


vault operator generate-root -init -pgp-key=gpg:my_gpg_public.asc


.. code-block:: text

   Nonce              e24dec5e-f1ea-2dfe-ecce-604022006976
   Started            true
   Progress           0/5
   Complete           false
   PGP Fingerprint    e2f8e2974623ba2a0e933a59c921994f9c27e0ff

The :code:`nonce` value should be distributed to all unseal key holders.



vault operator generate-root -address=http://localhost:8200

Then enter unseal key.

:code:`Encoded Token` is what you need.
