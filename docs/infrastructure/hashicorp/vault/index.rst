Hashicorp Vault
===============

.. warning::

   Hashicorp Vault multi datacenter feature is a part of Enterprise
   version.


.. note::

   Consul's storage is unique per datacenter (LAN). So when using as a
   Vault's storage - your secrets will exist within 1 DC.


.. toctree::
   :maxdepth: 2
   :caption: Contents

   init
   unseal
