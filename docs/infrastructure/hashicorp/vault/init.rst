Vault init
==========

.. warning::

   *"Hashicorp Vault multi datacenter feature is a part of Enterprise
   version."*

.. note::

   So I have to go through Vault "init" process in each new region and
   datacenter.  But anyway you will have a separate Vault cluster in
   each region.

Check if vault is not initialized
---------------------------------

.. code-block:: text

   vault status -address=http://0.0.0.0:8200
   ...
   Initialized        false
   ...

.. note::

   :code:`vault status` uses https protocol by default. So you may have
   an error if omitting :code:`-address=http://0.0.0.0:8200`

   .. code-block:: text

      Error checking seal status: Get https://127.0.0.1:8200/v1/sys/seal-status: http: server gave HTTP response to HTTPS client

Run vault init
--------------

It's better to use encrypted keys. At first see what keys do you have:

.. code-block:: text

   gpg --list-keys
   /home/user/.gnupg/pubring.gpg
   -----------------------------
   pub   2048R/PUB... 2013-01-11
   uid                  Sergey Pashinin (YOUR_EMAIL)
   sub   2048R/SUB... 2013-01-11


Save you public gpg key and upload it to server:

.. code-block:: text

   gpg --armor --export YOUR_EMAIL > my_gpg_public.asc
   scp ~/my_gpg_public.asc ubuntu@NEW.SERVER.IP.ADDR:~/my_gpg_public.asc


Run :code:`vault init`:

.. code-block:: text

   # With gpg public keys
   # example:
   # vault operator init -key-shares=3 -key-threshold=2 -pgp-keys="jeff.asc,vishal.asc,seth.asc"
   vault operator init -key-shares=1 -key-threshold=1 -pgp-keys="my_gpg_public.asc" -address=http://0.0.0.0:8200

.. note::

   What happens if I `vault init` once again?

   You will get an error: :code:`Vault is already initialized`.


Rekeying: add/remove unseal keys, change threshold
--------------------------------------------------

By default Vault generates 5 keys. For example I initialized Vault with
3 keys threshold. But only I use it - so I want to `unseal` it with only
one key:

First, initialize a rekeying operation. The flags represent the newly
desired number of keys and threshold:

.. code-block:: text

   vault rekey -init -key-shares=1 -key-threshold=1 -pgp-keys="my_gpg_public.asc"


If something goes wrong and you start it again you will get an error:

.. code-block:: text

   Error initializing rekey: Error making API request.

   URL: PUT http://127.0.0.1:8200/v1/sys/rekey/init
   Code: 400. Errors:

   * rekey already in progress


Then just remove `-init` flag. Simply:

.. code-block:: text

   vault rekey
   Rekey operation nonce: eb5fdde2-2e20-c075-01b5-9fc79b3a80af
   Key (will be hidden):


This will generate a nonce value and start the rekeying process. All
other unseal keys must also provide this nonce value. This nonce value
is not a secret, so it is safe to distribute over insecure channels like
chat, email, or carrier pigeon.

.. code-block:: text

   Nonce: 22657753-9cca-189a-65b8-cb743d104ffc
   Started: true
   Key Shares: 3
   Key Threshold: 2
   Rekey Progress: 0
   Required Keys: 1


Each unseal key holder runs the following command and enters their unseal key:

.. code-block:: text

   vault rekey
   Rekey operation nonce: 22657753-9cca-189a-65b8-cb743d104ffc
   Key (will be hidden):


When the final unseal key holder enters their key, Vault will output the new unseal keys:

.. code-block:: text

    Key 1: wcBMA37rwGt6FS1VAQgAk1q8XQh6yc...
    Key 2: wcBMA0wwnMXgRzYYAQgAavqbTCxZGD...
    Key 3: wcFMA2DjqDb4YhTAARAAeTFyYxPmUd...

    Operation nonce: 22657753-9cca-189a-65b8-cb743d104ffc

    ...


Like the initialization process, Vault supports PGP encrypting the
resulting unseal keys and creating backup encryption keys for disaster
recovery.

Get key from encrypted key
--------------------------

You have a long one (encrypted) key and want to get decrypted one to enter:

.. code-block:: text

   # if you use keybase:
   echo "wcBMA37..." | base64 -d | keybase pgp decrypt
   keybase: command not found
   # To fix this - install keybase?

   # if you use gpg:
   echo "wcBMA37..." | base64 -d | gpg -dq




Keys vs token
-------------

3/5 keys are needed for unsealing (by default).

Token is needed for authentication (i.e. for doing all the operations
except unsealing).


Where to store a token?
-----------------------

A token is used to request all the secrets I'm allowed to request. But I
can't just put my app's token in my project git repo in some file
(settings.py).

By default when you do `vault auth` manually your token is written to
`~/.vault-token` file. But this command itself is optional. You may pass
token as env variable VAULT_TOKEN as well.




## App Role

App Roles can get tokens dynamically. We need to create a role, for example:

#. `domain1.com` - a role to get access to secrets used by this domain
#. `postgres` - a role to get access to secrets used by PostgreSQL
#. ... or anything - you decide

Enable AppRole authentication:

```
vault auth-enable approle
Successfully enabled 'approle' at 'approle'!
```

Create `testrole1`:

```
vault write auth/approle/role/testrole secret_id_ttl=10m token_num_uses=10 token_ttl=20m token_max_ttl=30m secret_id_num_uses=40
Success! Data written to: auth/approle/role/testrole
```
