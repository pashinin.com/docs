Revoke compromised token and create a new one
=============================================

Revoke a token and all the token's children:

.. code-block:: text

   $ vault token revoke -mode=orphan 96ddf4bc-d217-f3ba-f9bd-017055595017
   Success! Revoked token (if it existed)
