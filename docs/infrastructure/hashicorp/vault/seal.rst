vault seal
==========

First - auth:

.. code-block:: text

   vault auth
   Token (will be hidden):
   Successfully authenticated! You are now logged in.
   token: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
   token_duration: 0
   token_policies: [root]


and then repeat `vault seal`

.. code-block:: text

   vault seal
   Vault is now sealed.
