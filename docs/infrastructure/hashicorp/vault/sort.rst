sort
====

Read secrets
------------

.. code-block:: bash

   vault list secret
   vault list secret/site
   vault write secret/s3-www ACCESS_KEY=foo SECRET_KEY=bar
   vault read secret/s3-www


Add new secrets
---------------

.. code-block:: bash

   vault write secret/site/sentry KEY="..."

   vault write secret/s3-www ACCESS_KEY=foo SECRET_KEY=bar
   vault read secret/s3-www

Multiline:

vault write secret/foo zip=-


Add new policy (and a new prefix)
---------------------------------

.. code-block:: bash

   vault policy write dkim - <<EOF
   path "dkim/*" {
     capabilities = ["create", "update", "read"]
   }
   EOF

   # For traefik:
   vault policy write traefik - <<EOF
   path "traefik/*" {
     capabilities = ["create", "update", "read", "list"]
   }
   EOF

   vault policy list

   vault write dkim/pashinin.com key=value
   # error: no handler for route 'dkim/pashinin.com'
   # solution:
   vault secrets enable -path=dkim kv  # create path "/dkim"

working with AppRole
--------------------

## Create a policy for a future role

```
vim letsencrypt.hcl
```

.. code-block:: text

   path "secret/letsencrypt/pashinin.com" {
     capabilities = ["read","create","update"]
   }


Write it:

.. code-block:: bash

   vault policy-write letsencrypt letsencrypt.hcl
   # Policy 'letsencrypt' written.


## Create a role that is restricted to my LAN with no secret-id

.. code-block:: text

   vault write auth/approle/role/letsencrypt secret_id_ttl=10m token_num_uses=10 token_ttl=20m token_max_ttl=30m secret_id_num_uses=40 bind_secret_id=false policies=default,letsencrypt bound_cidr_list=10.254.239.0/24,127.0.0.1/32


`bind_secret_id=false` - do not use secret-id

`bound_cidr_list=10.254.239.0/24,127.0.0.1/32` - restrict to my LAN

## Get Role ID

Use roled id as env variable, then read it from this env variable in your code.

.. code-block:: text

   # vault read auth/approle/role/MY_ROLE_NAME/role-id
   vault read auth/approle/role/letsencrypt/role-id
