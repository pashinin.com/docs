Unseal Vault servers
====================

Manually
--------

.. code-block:: bash

   vault operator unseal -address=http://10.254.239.2:8200
   vault operator unseal -address=http://0.0.0.0:8200


Auto unseal
-----------

.. warning::

   Some problems might happen with auto unsealing.

   :code:`Error parsing Seal configuration: ... connect: connection refused`

   https://github.com/hashicorp/vault/issues/9316


.. note::

   Need "auto unseal" when rebooting a node.

.. warning::

   This will work only for nodes inside one DC.  Because sealed Vault
   connects to unsealed ones and they must have the same storage to use
   created tokens.

1. Create a "transit" secret engine: vault secrets enable transit

2. Create a key named 'autounseal': vault write -f
   transit/keys/autounseal

3. Create policy

.. code-block:: text

   vault policy write autounseal - <<EOF
   path "transit/encrypt/autounseal" {
      capabilities = [ "update" ]
   }

   path "transit/decrypt/autounseal" {
      capabilities = [ "update" ]
   }
   EOF

4. Generate a token for another Vault server

.. code-block:: bash

   vault token create -policy="autounseal"
   # vault token create -policy="autounseal" -wrap-ttl=120



**Then migrate all Vault servers one by one to a new seal type**

.. code-block:: bash

   vault operator unseal -migrate -address=http://10.254.239.5:8200
