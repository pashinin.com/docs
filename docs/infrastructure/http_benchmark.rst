HTTP benchmarks
===============

.. code-block:: text

   ab -n 1000 -c 100 https://baumanka-latest.pashinin.com/


https://baumanka-latest.pashinin.com
------------------------------------

#. Fabio (TLS)
#. ui-tree-rs (actix-web)
#. CockroachDB backend (3 nodes)

1000 reqs, 100 parallel

.. code-block:: text

   Time per request:       234.309 [ms] (mean)


https://baumanka.pashinin.com
-----------------------------

#. Fabio (TLS)
#. Nginx
#. Python (uwsgi)
#. File system reads (list dir, stat files)

1000 reqs, 100 parallel

.. code-block:: text

   Time per request:       1059.286 [ms] (mean)
