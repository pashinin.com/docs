etcd
====

.. note::

   Etcd is used by many projects. Useful to have it running. Use 3-5
   etcd servers per a datacenter.

.. note::

   Etcd binary is "server mode" only. There is no Etcd "client mode"
   like in Hashicorp Consul which can be installed on all nodes for
   admin purposes.

Multiple datacenters (regions)
------------------------------
