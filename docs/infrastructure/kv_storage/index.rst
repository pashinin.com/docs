Key-value storage
=================

Consistent KV storage is used to store state, configs and encrypted secrets
of multiple computers in a datacenter and/or region(s).

.. note::

   For example Hashicorp Vault (app for storing secrets) supports
   several storages. Each has it's own features.

.. note::

   I wanted to have cross-region KV storage.
   Problems: big latency and bandwidth cost as well.


Consul vs Etcd vs ZooKeeper
---------------------------

Etcd - for kubernetes

ZooKeeper - for ClickHouse

Consul - for everything else


.. toctree::
   :maxdepth: 1
   :caption: List of KV storages

   ../hashicorp/consul/index
   etcd
