Envoy
=====

https://www.envoyproxy.io/docs/

Start Envoy (using "envoy-alpine" Docker image) on "balancer1" node:

.. code-block:: bash

   terraform apply  # everything
   # or
   nomad run envoy.nomad  # only Envoy


What happens when services get up and down? How Envoy reacts?
