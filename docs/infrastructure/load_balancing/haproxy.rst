HAProxy
=======

.. note::

   Do not run HAProxy inside Docker. Can't change configuration file
   dynamically using Docker.

haproxy.cfg.ctpl

.. code-block:: ini

   # -*- mode: conf -*-
   # Test HAProxy hot-reload:
   #
   # 1. ab -S -q -n 10000 -c 300 https://pashinin.com/favicon.ico
   #
   # 2. While test is running - make "service haproxy reload"
   #
   # 3. See "Failed requests: 0"


   # Documentation:
   #
   # http://cbonte.github.io/haproxy-dconv/1.8/configuration.html
   #
   # maxconn - maximum per-process number of concurrent connections (Ex.:
   #           maxconn 32)
   #
   # option tcplog - https://cbonte.github.io/haproxy-dconv/1.8/configuration.html#option%20tcplog
   #
   # timeout client - Set the maximum inactivity time on the client side.

   global
   	log /dev/log	local0
   	log /dev/log	local1 notice
   	chroot /var/lib/haproxy

       # new (hot reload):
       stats socket /run/haproxy/admin.sock mode 600 expose-fd listeners level admin
       # old:
   	# stats socket /run/haproxy/admin.sock mode 660 level admin
   	stats timeout 30s
   	user haproxy
   	group haproxy
   	daemon

   	# Default SSL material locations
   	ca-base /etc/ssl/certs
   	crt-base /etc/ssl/private

   	# Default ciphers to use on SSL-enabled listening sockets.
   	# For more information, see ciphers(1SSL). This list is from:
   	#  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
   	# An alternative list with additional directives can be obtained from
   	#  https://mozilla.github.io/server-side-tls/ssl-config-generator/?server=haproxy
   	ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS
   	ssl-default-bind-options no-sslv3

   defaults
   	log	global
   	mode	http
   	option	httplog
   	option	dontlognull
           timeout connect 5000
           timeout client  50000  # docs: https://cbonte.github.io/haproxy-dconv/1.8/configuration.html#4-timeout%20client
           timeout server  50000
   	errorfile 400 /etc/haproxy/errors/400.http
   	errorfile 403 /etc/haproxy/errors/403.http
   	errorfile 408 /etc/haproxy/errors/408.http
   	errorfile 500 /etc/haproxy/errors/500.http
   	errorfile 502 /etc/haproxy/errors/502.http
   	errorfile 503 /etc/haproxy/errors/503.http
   	errorfile 504 /etc/haproxy/errors/504.http

   # --------------------------------------------------------------------
   # Tarantool
   # frontend frontend-tarantool
   #     bind *:3301
   #     mode tcp

   #     # Wait for console (or any service) to send packets to Tarantool at
   #     # least once in 2 days or close connection
   #     timeout client 2d

   #     default_backend backend-tarantool
   # backend backend-tarantool
   #     mode tcp
   #     balance leastconn

   #     # timeout docs: https://serverfault.com/a/778222
   #     #
   #     # Tarantool may not send data for a long time. We should not termintate
   #     # this connection or connected console will say: "error: Peer closed"
   #     timeout server 2d

   #     stick store-request src
   #     stick-table type ip size 200k expire 30m
   # {{ range service "tarantool" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} send-proxy-v2 check inter 15s{{ end }}


   # HTTP
   # frontend http-in
   #          bind *:80 name HTTP
   #          default_backend servers
   # backend servers
   #     # on-marked-down shutdown-sessions - ?
   #     server server     10.254.239.1:80 check port 80 send-proxy
   #     # server desktop    10.254.239.2:80 maxconn 32 check port 80 on-marked-down shutdown-sessions
   #     # server student-pc 10.254.239.3:80 maxconn 32 check port 80
   # --------------------------------------------------------------------
   # nginx-http
   frontend frontend-nginx-http
       bind *:80 name HTTP
       acl desktop src 10.254.239.2
       # use_backend backend-nginx-http-canary if desktop  # || wifi
       default_backend backend-nginx-http

   backend backend-nginx-http
       balance leastconn
       server node1     10.254.239.1:80 check port 80 send-proxy
   # {{ range service "nginx-http" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 10s{{ end }}

   # backend backend-nginx-http-canary
   #     balance roundrobin

   #     # Use Canaries when they are running
   #     {{ range service "canary.vue-http" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 10s send-proxy
   #     # {{ .Tags | join ", " }}
   #     {{ else }}
   #      # Otherwise - use regular services
   #      {{ range service "vue-http" }}
   #      server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 10s send-proxy
   #      # {{ .Tags | join ", " }}
   #      {{ end }}
   #     {{ end }}

   # --------------------------------------------------------------------

   # HTTPS
   frontend frontend-nginx-https
       bind *:443
       mode tcp

       acl desktop src 10.254.239.2
       # acl wifi src 10.254.240.0/24
       acl wifi src 10.254.239.1
       acl tele2 src 176.59.39.70

       # Telegram IPs:
       # 149.154.167.197-233
       acl telegram src 149.154.167.197/26
       # log             global
       # option          dontlognull
       # option          dontlog-normal
       # option          log-separate-errors
       # timeout         client 30000
       # tcp-request     inspect-delay 5s
       # tcp-request     content accept if { req.ssl_hello_type 1 }
       # acl proto_tls   req.ssl_hello_type 1
       # use_backend nodes-https if proto_tls
       # use_backend B if desktop || wifi
       # use_backend B if tele2
       # use_backend B if telegram
       # use_backend backend-nginx-https-canary if desktop  # || wifi
       default_backend backend-nginx-https

   backend backend-nginx-https
       mode            tcp
       # log             global
       stick-table type ip size 1m expire 1h
       stick on src
       balance         leastconn
       timeout         connect 30s
       timeout         server 300s
       retries         3
       # option          ssl-hello-chk
       server          node1 10.254.239.1:443 check send-proxy
       # Fuck "ssl verify none" options! Didn't work with them.

   # backend backend-nginx-https-canary
   #     mode            tcp
   #     balance roundrobin
   #     # Use Canaries when they are running
   #     #
   #     {{ range service "canary.vue-https" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 10s send-proxy
   #     # {{ .Tags | join ", " }}
   #     {{ else }}
   #      # Otherwise - use regular services
   #      {{ range service "vue-https" }}
   #      server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 10s send-proxy
   #      # {{ .Tags | join ", " }}
   #      {{ end }}
   #     {{ end }}


   # --------------------------------------------------------------------
   # alertmanager
   #
   # frontend frontend-alertmanager
   #     bind *:9093
   #     mode tcp
   #     timeout client 1h
   #     default_backend backend-alertmanager
   # backend backend-alertmanager
   #     mode tcp
   #     balance leastconn
   #     timeout server 1h
   # {{ range service "prometheus-alertmanager" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 10s{{ end }}



   # --------------------------------------------------------------------
   # CockroachDB SQL port
   # frontend frontend-cockroach-sql
   #     bind *:26257
   #     mode tcp
   #     #timeout client 30s  # "psql" console may NOT send data each minute
   #     timeout client 12h  # was 30m
   #     default_backend backend-cockroach-sql
   # backend backend-cockroach-sql
   #     mode tcp
   #     # balance leastconn
   #     balance roundrobin
   #     #timeout server  30s
   #     timeout server 12h  # was 30m
   #     option pgsql-check user root
   #     # option httpchk GET /health?ready=1
   #     # stick store-request src
   #     # stick-table type ip size 200k expire 30m
   # {{ range service "cockroach" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 10s{{ end }}



   # --------------------------------------------------------------------
   # CockroachDB Dashboard port
   # frontend frontend-cockroach-dashboard
   #     bind *:8081
   #     default_backend backend-cockroach-dashboard
   # backend backend-cockroach-dashboard
   #     # mode tcp
   #     balance leastconn
   #     # option httpchk GET /health?ready=1
   #     # stick store-request src
   #     # stick-table type ip size 200k expire 30m
   # {{ range service "cockroach-dashboard" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 10s{{ end }}


   # --------------------------------------------------------------------
   # Dovecot (993)
   # frontend frontend-dovecot-993  # SSL/TLS (port 993)
   #     bind *:993
   #     mode tcp
   #     timeout client 10m  # Thunderbird should NOT reconnect each minute
   #     default_backend backend-dovecot-993
   # backend backend-dovecot-993  # SSL/TLS (port 993)
   #     mode tcp
   #     balance leastconn
   #     timeout server 10m
   #     stick store-request src
   #     stick-table type ip size 200k expire 30m
   # {{ range service "dovecot-ssl-tls" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} send-proxy check inter 10s{{ end }}


   # --------------------------------------------------------------------
   #Dovecot (2525. LMTP, for connecting with Exim)
   # # Exim sends mails to Dovecot over this port
   # frontend frontend-dovecot-lmtp
   #     bind *:2525
   #     mode tcp
   #     timeout client 2m
   #     default_backend backend-dovecot-lmtp
   # backend backend-dovecot-lmtp
   #     mode tcp
   #     balance roundrobin
   #     # option tcplog
   #     stick store-request src
   #     stick-table type ip size 200k expire 30m
   # {{ range service "dovecot-lmtp" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 10s{{ end }}


   # --------------------------------------------------------------------
   # Exim, 465 port (submission, better use 465, not 587)
   # Can't switch to Fabio because of this (mail server must know real IP):
   # https://github.com/fabiolb/fabio/issues/191
   # frontend frontend-exim-submission
   #     bind *:465
   #     mode tcp
   #     timeout client 1m  # Thunderbird should NOT reconnect each minute
   #     default_backend backend-exim-submission
   # backend backend-exim-submission
   #     mode tcp
   #     balance roundrobin
   #     # option smtpchk HELO pashinin.com
   #     stick store-request src
   #     stick-table type ip size 200k expire 30m
   # {{ range service "exim-submission" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} send-proxy-v2 check inter 10s{{ end }}


   # --------------------------------------------------------------------
   # Exim, 587 port (submission, better use 465, not 587)
   # Can't switch to Fabio because of this (mail server must know real IP):
   # https://github.com/fabiolb/fabio/issues/191
   # frontend frontend-exim-submissionold
   #     bind *:587
   #     mode tcp
   #     timeout client 1m  # Thunderbird should NOT reconnect each minute
   #     default_backend backend-exim-submissionold
   # backend backend-exim-submissionold
   #     mode tcp
   #     balance roundrobin
   #     # option smtpchk HELO pashinin.com
   #     stick store-request src
   #     stick-table type ip size 200k expire 30m
   # {{ range service "exim-submissionold" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} send-proxy-v2 check inter 10s{{ end }}


   # --------------------------------------------------------------------
   # Exim, 25 port (smtp)
   # Can't switch to Fabio because of this (mail server must know real IP):
   # https://github.com/fabiolb/fabio/issues/191
   # frontend frontend-exim-smtp
   #     bind *:25
   #     mode tcp
   #     timeout client 2m  #
   #     default_backend backend-exim-smtp
   # backend backend-exim-smtp
   #     mode tcp
   #     balance roundrobin
   #     # option smtpchk HELO pashinin.com
   #     stick store-request src
   #     stick-table type ip size 200k expire 30m
   # # send-proxy
   # {{ range service "exim-smtp" }}
   #     server {{ .Node }} {{ .Address }}:{{ .Port }} send-proxy check inter 10s{{ end }}


   # --------------------------------------------------------------------
   # rspamd (https://rspamd.com/)
   #
   # DKIM signing,
   #
   frontend frontend-rspamd
       bind *:11333
       mode tcp
       timeout client 1m
       default_backend backend-rspamd
   backend backend-rspamd
       mode tcp
       balance leastconn
       stick store-request src
       stick-table type ip size 200k expire 30m
   {{ range service "rspamd" }}
       server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 20s{{ end }}


   # --------------------------------------------------------------------
   # frontend frontend-es-cluster
   #          bind *:9200 name HTTP
   #          default_backend backend-es-cluster
   # backend backend-es-cluster{{ range service "es-cluster" }}
   #   server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 10s{{ end }}


   # docs frontend
   # frontend frontend-docs
   #          bind *:8080 name HTTP
   #          default_backend backend-docs
   # backend backend-docs{{ range service "docs" }}
   #   server {{ .Node }} {{ .Address }}:{{ .Port }} check inter 15s{{ end }}


   # # S3
   # frontend S3
   #          bind *:7480 name HTTP
   #          default_backend RGW-servers
   # backend RGW-servers
   #     # on-marked-down shutdown-sessions - ?
   #     server server     10.254.239.1:7480 maxconn 32 check inter 10s port 7480
   #     server server     10.254.239.3:7480 maxconn 32 check inter 10s port 7480



   # backend B
   #     mode            tcp
   #     log             global
   #     stick-table type ip size 1m expire 1h
   #     stick on src
   #     balance         leastconn
   #     timeout         connect 30000
   #     timeout         server 300000
   #     retries         3
   #     server          server 10.254.239.3:444 check send-proxy


   # Postfix
   # frontend ft_smtp
   #   bind 0.0.0.0:25
   #   mode tcp
   #   no option http-server-close
   #   timeout client 1m
   #   # log global
   #   # option tcplog
   #   default_backend bk_postfix

   # backend bk_postfix
   #   mode tcp
   #   no option http-server-close
   #   log global
   #   option smtpchk
   #   timeout server 1m
   #   timeout connect 5s
   #   server server 10.254.239.1:25 send-proxy check inter 10s

   # frontend ft_smtp_587
   #   bind 0.0.0.0:587
   #   mode tcp
   #   no option http-server-close
   #   timeout client 1m
   #   timeout connect 5s
   #   log global
   #   # option tcplog
   #   default_backend bk_postfix_587

   # backend bk_postfix_587
   #   mode tcp
   #   no option http-server-close
   #   option smtpchk
   #   log global
   #   timeout server 1m
   #   timeout connect 5s
   #   server server 10.254.239.1:587 send-proxy check inter 10s


   # --------------------------------------------------------------------
   # IMAP (143)
   # frontend ft_imap_143
   #     bind :143
   #     mode tcp
   #     timeout client 20m  # Thunderbird may not send any data for maximum
   #                         # 20 mins and connection will not be termintated
   #     default_backend bk_imap_143

   # backend bk_imap_143
   #     mode tcp
   #     balance leastconn
   #     stick store-request src
   #     stick-table type ip size 200k expire 30m
   #     server s1 10.254.239.1:10143 send-proxy-v2 check inter 10s
   #     # server s1 10.254.239.1:143 send-proxy-v2
   #     # server s1 backend.example.com:10143 send-proxy-v2


   # --------------------------------------------------------------------
   # IMAP (993)
   # frontend ft_imap_993
   #     bind :993
   #     timeout client 20m  # Thunderbird may not send any data for maximum
   #                         # 20 mins and connection will not be termintated
   #     no option http-server-close
   #     mode tcp
   #     default_backend bk_imap_993

   # backend bk_imap_993
   #     mode tcp
   #     balance leastconn
   #     stick store-request src
   #     stick-table type ip size 200k expire 30m
   #     server s1 10.254.239.1:10993 send-proxy-v2 check inter 10s


   # Stats
   # listen stats :1936
   # port 9000 is used by Sentry
   listen stats
       bind :9001
       mode http
       stats enable
       # stats hide-version
       stats realm Haproxy\ Statistics
       stats uri /
       # stats auth Username:Password








When 1 server is down
---------------------

Вот что может случиться если HAProxy направит запрос к серверу, который
отвалился только что (и HAProxy еще не знает, что он отвалился)

```
503 error
No server is available to handle this request
```

В раздел `defaults` нужно добавить:

```
    option redispatch  # if 1 backend failed - try another one
```

тогда если один сервер не ответил - HAProxy спросит у другого. Это будет
работать только если произошел таймаут, если сервер ответил 500
ошибкой - HAProxy вернет этот ответ.
