Load balancing
==============

**Fabio** - using as an edge LB. Supports Proxy Protocol and loading
certificates from Consul and Vault.

**Envoy** - used by Consul Connect for Service Mesh.

**Traefik** - does not support saving and loading certificates from
Consul and Vault.

**HAProxy** - to test (did not support service discovery earlier)

To try out a new balancer - :doc:`move to it without interrupting the
main one <moving_between_balancers>`.


.. toctree::
   :maxdepth: 2
   :caption: List of load balancers

   traefik
   haproxy
