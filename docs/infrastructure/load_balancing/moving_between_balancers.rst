Move from one load balancer to another one
==========================================

.. image:: ../../../images/lb_move.png

1. Configure your *new* load balancer to listen on 81 (HTTP) and 82
   (HTTPS) ports while your current LB uses default 80 and 443.

2. Configure :code:`iptables` to route requests from testing IP to your
   new LB.

.. code-block:: text

   -t nat -A PREROUTING -s 10.254.239.2 -d 89.179.240.127/32 -p tcp -m tcp --dport 80 -j DNAT --to-destination 10.254.239.4:81
   -t nat -A PREROUTING -s 10.254.239.2 -d 89.179.240.127/32 -p tcp -m tcp --dport 443 -j DNAT --to-destination 10.254.239.4:82
   # iptables -A FORWARD -p tcp -d 10.254.239.2 --dport 81 -j ACCEPT
