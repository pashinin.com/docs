Traefik
=======

Let's Encrypt
-------------

When configured Traefik automagically creates certificates for
domains.

**Where are certificates stored?**

*In Traefik v1* - in Consul key-value storage.  Can be viewed using
Consul web interface under "traefik/acme/account". They are compressed (so
looks like garbage).

*In Traefik v2* - in a file.
