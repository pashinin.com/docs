Configure_Amavis_to_sign_my_mail_with_DKIM_keys
===============================================

sudo apt-get install amavisd-new


service amavis status       # start|stop|restart|force-reload|status|debug
 * amavisd is running


netstat -anp | grep amavisd-new
#tcp        0      0 127.0.0.1:10024         0.0.0.0:*               LISTEN      2215/amavisd-new (m


Add settings in '''/etc/amavis/conf.d/50-user'''

.. code-block:: php

   ...

   $enable_dkim_signing = 1;
   #dkim_key('domain.tld', 'selector', '/var/dkim/domain.tld.key.pem');
   dkim_key('example1.com', 'mail', '/var/dkim/example1.com.pem');
   dkim_key('example2.com', 'mail', '/var/dkim/example1.com.pem');
   @dkim_signature_options_bysender_maps = (
     { '.' => { ttl => 21*24*3600, c => 'relaxed/simple' } } );
   @mynetworks = qw(0.0.0.0/8 127.0.0.0/8 10.254.0.0/16 172.16.0.0/12
                    192.168.0.0/16);  # list your internal networks

   ...

And restart:


.. code-block:: bash

   service amavis restart
   #Stopping amavisd: amavisd-new.
   #Starting amavisd: amavisd-new.
