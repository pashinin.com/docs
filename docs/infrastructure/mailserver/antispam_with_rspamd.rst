Antispam with rspamd, Exim and Dovecot
**************************************

.. warning::

   This article is only about antispam configuration. Nothing else.

Exim
====

First of all Exim receives an email and sends it to rspamd. This is
configured in Exim's config:

.. code-block:: ini

   # this was somewhere at the top of Exim's config:
   spamd_address = 10.254.239.4 11333 variant=rspamd
   acl_smtp_data = acl_check_spam

So we need to add this "acl" (in Exim's acl section):

.. code-block:: ini

   begin acl
   ...
   acl_check_spam:
     warn
       spam = nobody:true

These lines are actually responsible for redirecting an email to rspamd.

After scanning an email rspamd will set some variables (for Exim) that
we will use to do actions with this mail. All actions are done by
Exim. So this is also a part of Exim's config. rspamd will set following
variables:

+---------------------------------------+-----------------------------------------+
| $spam_action                          | "add header", "soft reject", "reject"   |
+---------------------------------------+-----------------------------------------+
| $spam_score                           | the message score (we unlikely need it) |
+---------------------------------------+-----------------------------------------+
| $spam_score_int                       | Fast web server                         |
+---------------------------------------+-----------------------------------------+
| $spam_report                          | Distributed (S3-compatible) file system |
+---------------------------------------+-----------------------------------------+
| $spam_bar                             | :doc:`Mail server<./index>`             |
+---------------------------------------+-----------------------------------------+

.. code-block:: text

   $spam_action is the action recommended by rspamd
   $spam_score is the message score (we unlikely need it)
   $spam_score_int is spam score multiplied by 10
   $spam_report lists symbols matched & protocol messages
   $spam_bar is a visual indicator of spam/ham level

"add header" and "rewrite subject" are equivalent to Rspamd. They are
just two options with the same purpose: to mark a message as probable
spam. The "soft reject" action is mainly used to indicate temporary issues
in mail delivery, for instance, exceeding a rate limit.


Exim's "defer" indicates a temporary problem (4xx) to the sender, and
"deny" means a permanent problem (5xx).

Test spam message:

.. code-block:: text

   XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X

rspamd
======

How to teach rspamd
-------------------

.. note::

   The best way to teach rspamd is to manually move mails between folder
   on your IMAP server. For example when I move a mail from
   Thunderbird's "Junk" folder to "Inbox" means that rspamd should
   "learn" that it was actually a "good" mail. And otherwise - moving a
   mail to "Junk" folder should tell rspamd that was a spam. So we need
   to configure Dovecot to do something on these events.

.. note::

   Where rspamd stores all learned info? Actually it wants
   Redis. Problem here - I do not want to use Redis.
   TODO...

sa-learn-spam.sh

   1 #!/bin/sh
   2 exec /usr/bin/rspamc -h /run/rspamd/worker-controller.socket -P <secret> learn_spam

sa-learn-ham.sh

   1 #!/bin/sh
   2 exec /usr/bin/rspamc -h /run/rspamd/worker-controller.socket -P <secret> learn_ham

TODO


Dovecot
=======

My old /etc/dovecot/sieve/default.sieve:

.. code-block:: ini

   require "fileinto";
   if header :contains "X-Spam-Flag" "YES" {
       fileinto "Junk";
   }

For rspamd I changed it to:

.. code-block:: ini

   require "fileinto";
   if header :contains "X-Spam-Status" "Yes" {
       fileinto "Junk";
   }


Jan 10 09:44:18 lmtp(18): Error: Failed to lookup user sergey@pashinin.com: Plugin 'sieve' not found from directory /usr/lib/dovecot
