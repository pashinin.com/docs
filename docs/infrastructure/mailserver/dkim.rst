DKIM
====

.. note::

   DKIM is not about encrypting messages. This is ensuring that message
   (which tells it is from example.org) indeed IS from example.org.

   So we need a key associated with a domain and a signaute of each
   email using this key.


Create a DKIM key for your domain
=================================

You can create this key by yourself. There is no need for these keys to
be verified by LetsEncrypt or anything else.

.. code-block:: bash

   openssl genrsa -out dkim_private.pem 1024

.. code-block:: text

   -----BEGIN RSA PRIVATE KEY-----
   MIICXgIBAAKBgQDlmyB29Spcpmtg+BjkHy5QuaktIuIs4sscNHvXnHuo40tNshQL
   AY20A5bSoMWgYRZw9KsE+4gxgG8rFn/pc/KPcIe/GOp4hTYYN37QAk2iqpm4lud6
   JQhI26sHldRNRiW1+y5ZjWLSwV78fLhtj2OVRnXJ/KanTr/Nsx8t39Y7lwIDAQAB
   AoGBAIqPyVHAtmXfUKHeVzcj0hGJWZ0Rlii8XRKSYcJGfyH2uTUUKKBTua5PFC/c
   MieNL7vPC34gRZqKBYydMWgzPsdI2zL8uf0dWqdinlisjSgZqexe+xr+vgOvBXzD
   1jnnANyRYDEt6foOZcWGvR8ANpMDvyAcB8M4Y1OlF71Dwt5hAkEA9kJxdWcuJb8X
   DPESVgoka358r+eBBy5aAGs8gtAltg0GWsST5k8ub0aMQWbchhSOcezEo0qv823p
   xzrr++z+2QJBAO6wDdSEyxHbdTqEc1VPE0V5uVb4WSlJ6PjwXDZTVO3r5Zu/cVZG
   iAgz73iZXV/nooZ8LgRLVgBVmwJHU/qDZ+8CQQDfEGh9lCz0/BcBHg0h6qX6yORg
   4i66Nn5ICtsRE0JVmY68AXyHDgpduWWqiGYQ/eXZxEKKN0kSE+nDEdhYpuBhAkBf
   TB9ZeGNzj8FwSa2ao+W32W+sT5+Zoo7HxX+rmP3RuFuGIZtorORnEgVMiz8CqYL0
   WuQshr+hhoxLDpty68A1AkEAz6+mpqVii9Sr1b9uMLUBNVoj8ewUfbYZz+5PoW/E
   RhXE3BMKja1FRNolo43Y+uoiP6NrnkmMaEqSfumXLYqJNA==
   -----END RSA PRIVATE KEY-----

.. warning::

   This key must stay private!


I'm using Vault, so I created Vault path for DKIM keys:


.. code-block:: bash

   vault policy write -address=http://10.254.239.4:8200 dkim - <<EOF
   path "dkim/*" {
     capabilities = ["create", "update", "read"]
   }
   path "dkim/data/*" {
     capabilities = ["create", "update", "read"]
   }
   EOF

   vault policy list -address=http://10.254.239.4:8200

   # vault secrets enable vault policy list -path=dkim kv  # create path "/dkim"
   # vault write dkim/pashinin.com/key value=-
   vault kv put -address="http://10.254.239.4:8200" dkim/pashinin.com/key value=-
   # insert text
   # -----BEGIN RSA PRIVATE KEY----- .......
   # Ctrl-D
   Success! Data written to: dkim/pashinin.com


Add DNS TXT record
==================

Make a public key from the private key, and format
it in BASE64 encoding suitable for use in a DKIM DNS record:

.. code-block:: bash

   openssl rsa -in dkim_private.pem -pubout -outform der 2>/dev/null | openssl base64 -A
   MIICXgIBAAKB.....

Then form a DKIM key:

.. code-block:: text

   v=DKIM1; p=MIICXgIBAAKB.....

Enter nsupdate:


.. code-block:: text

   server IP.OF.DNS.SERVER 53
   zone pashinin.com
   update delete default._domainkey.pashinin.com TXT
   update add default._domainkey.pashinin.com 86400 TXT "v=DKIM1; p=MIICXgIBAAKB....."
   send


dig default._domainkey.pashinin.com TXT


Exim + rspamd (optional)
========================

I created dkim_signing.conf for rspamd.


Postfix + Amavis (optional)
===========================

All outgoing emails in Postfix first go to Amavis where they are signed
with DKIM.

Amavis by default listens on port 10024. The only parameter in
/etc/postfix/main.cf that you should modify is "content-filter".

.. code-block:: text

   content_filter = smtp-amavis:[127.0.0.1]:10024

Now Postfix doesn't know what "smtp-amavis" is. So we need to describe
it in /etc/postfix/master.cf:

.. code-block:: text

   smtp-amavis     unix    -       -       -       -       2       smtp
        -o smtp_data_done_timeout=1200
        -o smtp_send_xforward_command=yes
        -o disable_dns_lookups=yes
        -o max_use=20

And restart Postfix.

To enable DKIM signing in Amavis I added following code to
/etc/amavis/conf.d/50-user file:

.. code-block:: text

   $enable_dkim_signing = 1;  # loads DKIM signing code

   dkim_key('pashinin.com', 'default', '/etc/amavis/pashinin.com-default.key.pem');
   # dkim_key('some-other-virtual.com', 'mail', '/etc/amavis/dkim/atlantis-example-com.key.pem');
   # dkim_key('another-virtual', 'mail', '/etc/amavis/dkim/atlantis-example-com.key.pem');

.. code-block:: bash

   service amavis restart  # restart Amavis


Test DKIM (last step)
=====================

Send an email via your server to yourself. See it's source code. You
need to have something like this:

.. code-block:: bash

   DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/simple; d=YOURDOMAIN.TLD; h=
   content-transfer-encoding:content-type:content-type:subject
   :subject:mime-version:user-agent:from:from:date:date:message-id
   :received:received; s=main; t=1375446256; x=1377260657; bh=g3zLY
   H4xKxcPrHOD18z9YfpAcnk/GaJedfustWU5uGs=; b=ktbEZozfdSSoaNNnemRl3
   9D2ozHGh6igwp5GwZJ1kAbsYK341gjTgJOqxX6aBsIfTsaEAp+q0MveRI7Q7WYzf
   wUkxBjr/bP/bOYACEECsqdLyQFNZ5TqFBZs2XFt+fdG5GXJRc+8hHACCpiTWShzM
   WL17pF6p5Nagqq5FYwM06Y=


https://www.mail-tester.com/
