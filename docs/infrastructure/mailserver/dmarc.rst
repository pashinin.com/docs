DMARC
=====

Политика DMARC позволяет отправителю указать, что их электронные письма
защищены SPF и / или DKIM, и дать инструкции, если ни один из этих
методов проверки подлинности проходит. Пожалуйста, убедитесь, что у вас
есть набор DKIM и SPF перед использованием DMARC.

У вас нет записи DMARC, пожалуйста, добавьте TXT запись на ваш домен
_dmarc.pashinin.com со следующим значением:

.. code-block:: text

   v=DMARC1; p=none

Детали проверки:

.. code-block:: text

   mail-tester.com; dkim=pass (1024-bit key; unprotected) header.d=pashinin.com header.i=@pashinin.com header.b=ycf5yT5t; dkim-atps=neutral
   mail-tester.com; dmarc=none header.from=pashinin.com
   mail-tester.com; dkim=pass (1024-bit key; unprotected) header.d=pashinin.com header.i=@pashinin.com header.b=ycf5yT5t; dkim-atps=neutral
   From Domain: pashinin.com
   DKIM Domain: pashinin.com


DMARC позволяет отправителю указать, как поступать с письмом по
результатам проверки. Это делается через указание политики DMARC,
которая бывает трёх видов:

none — политика для мониторинга. Используется, когда вы только начинаете
работу с DMARC и нужно понять, от имени каких доменов отправляется
почта.

quarantine — политика карантина, с ней письма в зависимости от почтового
провайдера будут отправлены в спам, отмечены к более строгой проверке
или помечены как подозрительные для пользователя.

reject — самая строгая политика и высочайший уровень защиты. При
политике reject отправитель указывает почтовому провайдеру блокировать
все письма, которые не прошли проверки SPF и/или DKIM.  Если совсем
кратко — то при использовании DMARC получатель может быть уверен, что
письмо действительно отправлено именно с того емейла, который указан в
поле From-email.

Solution:

.. code-block:: text

   v=DMARC1; p=none; rua=mailto:dmarc@pashinin.com; ruf=mailto:dmarc@pashinin.com; rf=afrf; pct=100


   ..
      echo -ne "hmac-sha512:update:" > /tmp/update.key; vault read -field="value" acme/dns/update.secret >> /tmp/update.key; \
      knsupdate -k /tmp/update.key; \
      rm /tmp/update.key
   echo -ne "hmac-sha512:update:" > /tmp/update.key; vault kv get -address=http://10.254.239.4:8200 -field="value" acme/dns/update.secret >> /tmp/update.key; \
   knsupdate -k /tmp/update.key; \
   rm /tmp/update.key
   >
   server pashinin.com 53
   zone pashinin.com

   update delete _dmarc.pashinin.com 86400 TXT "v=DMARC1; p=none; rua=mailto:dmarc@pashinin.com; ruf=mailto:dmarc@pashinin.com; rf=afrf; pct=100"
   update add _dmarc.pashinin.com 86400 TXT "v=DMARC1; p=quarantine; rua=mailto:dmarc@pashinin.com; ruf=mailto:dmarc@pashinin.com; rf=afrf; pct=100"







“v=DMARC1”

Version – This is the identifier that the receiving server looks for
when it is scanning the DNS record for the domain it received the
message from. If the domain does not have a txt record that begins with
v=DMARC1, the receiving server will not run a DMARC check.

“p=none”

Policy – The policy you select in your DMARC record will tell the
participating recipient email server what to do with mail that doesn’t
pass SPF and DKIM, but claims to be from your domain. In this case, the
policy is set to “none.” There are 3 types of policies you can set:

p=none – Tell the receiver to perform no actions against unqualified mail, but still send email reports to the mailto: in the DMARC record for any infractions.
p=quarantine – Tell the receiver to quarantine unqualified mail, which generally means “send this directly to the spam folder.”
p=reject – Tell the receiver to completely deny any unqualified mail for the domain. With this enabled, only mail that is verified as 100% being signed by your domain will even have a chance at the inbox. Any mail that does not pass is blackholed— not bounced—so there’s no way to catch false positives.
“rua=mailto:dmarc@sendgrid.com“

This part tells the receiving server where to send aggregate reports of DMARC failures. Aggregate reports are sent daily to the administrator of the domain that the DMARC record belongs to. They include high-level information about DMARC failures but do not provide granular detail about each incident. This can be any email address you choose.

“ruf=mailto:dmarc@sendgrid.com”

This part tells the receiving server where to send forensic reports of DMARC failures. These forensic reports are sent in real-time to the administrator of the domain that the DMARC record belongs to and contain details about each individual failure. This email address must be from the domain that the DMARC record is published for.

“rf=afrf”

Reporting format. This part tells the receiving server what kind of reporting the policyholder wants. In this case rf=afrf means aggregate failure reporting format.

“pct=100”

Percent – This part tells the receiving server how much of their mail
should be subjected to the DMARC policy’s specifications. You can choose
any number from 1-100. In this case, if the p= was set to reject, 100%
of the mail that fails DMARC would be rejected.
