Dovecot
*******

`Docker image <https://hub.docker.com/r/instrumentisto/dovecot/>`_:
instrumentisto/dovecot

I'm using Dovecot as IMAP server. Couldn't find any other decent
application for this. And I have no problems with it actually.

Using Dovecot's :code:`mdbox` format for storing mails now. It must be
faster than :code:`Maildir`.


Cluster
=======

node1 - node2 - node3

The problem is to sync all mails data. If an email comes to Node1 then
it should appear on all other nodes. Solutions:

#. Shared storage (CephFS mount)

   .. warning::

      From docs: WARNING: Shared folder replication doesn't work
      correctly right now – mainly it can generate a lot of duplicate
      emails.

#. Dovecot built-in master-master replication (`dsync
   <https://wiki.dovecot.org/Replication>`_)


DEBUG
=====


Get users from SQL
==================

When I go to a database through HAProxy i get:

.. code-block:: text

   server closed the connection unexpectedly

I tried to increase "client" and "server" timeouts in HAProxy to 30
mins.


Replica
=======

.. code-block:: bash

   docker exec -it `docker ps | grep dovecot | cut -d' ' -f1` doveadm replicator status *
   docker exec -it `docker ps | grep dovecot | cut -d' ' -f1` doveadm replicator replicate sergey2

Sometimes replication is not working (see error below). When I manually
run following command (sync) I immediately get new message in
Thunderbird:

.. code-block:: bash

   docker exec -it `docker ps | grep dovecot | cut -d' ' -f1` doveadm sync -u sergey2@pashinin.com tcp:10.254.239.2:8999


error
-----

Oct 30 14:25:46 doveadm: Error: Failed to lock mailbox Drafts for dsyncing: file_create_locked(/var/vmail/pashinin.com/sergey2/mailboxes/Drafts/dbox-Mails/.dovecot-box-sync.lock) failed: fcntl(/var/vmail/pashinin.com/sergey2/mailboxes/Drafts/dbox-Mails/.dovecot-box-sync.lock, write-lock, F_SETLKW) locking failed: Timed out after 30 seconds (WRITE lock held by pid 31)

It can be different mailbox in "Failed to lock mailbox Drafts for
dsyncing".


This happens when I passed all ip addresses to mail_replica options. I
needed to exclude self IP.


Dovecot service behind HAProxy
==============================

When using Dovecot behind HAProxy I used this Consul-template config:

.. code-block:: ini

   # Dovecot (993)
   frontend frontend-dovecot-993  # SSL/TLS (port 993)
       bind *:993
       mode tcp
       timeout client 10m  # Thunderbird should NOT reconnect each minute
       default_backend backend-dovecot-993
   backend backend-dovecot-993  # SSL/TLS (port 993)
       mode tcp
       balance leastconn
       timeout server 10m
       stick store-request src
       stick-table type ip size 200k expire 30m
   {{ range service "dovecot-ssl-tls" }}
       server {{ .Node }} {{ .Address }}:{{ .Port }} send-proxy check inter 10s{{ end }}

Where "dovecot-ssl-tls" was a service name defined in Nomad job file.

Dovecot part in master.conf:

.. code-block:: ini

   service imap-login {
     # ...
     inet_listener imap_haproxy {
       port = 10143
       haproxy = yes
     }
     inet_listener imaps_haproxy {
       port = 10993  # that is the port of service behind HAProxy
       ssl = yes
       haproxy = yes
     }
     # ...

I had problems with Fabio when using "proxy protocol" + Dovecot.  But
actually I don't need "proxy protocol" because authentication uses
(email+password). Of course I will see IP address of my balancer in
Dovecot logs instead of real IP but that's not a big problem.


Check SSL connection to Dovecot
===============================

openssl s_client -connect pashinin.com:993


Errors
======

Dovecot_lda:_Error:_chdir(/root)_failed:_Permission_denied
----------------------------------------------------------

The fix to this is simply not to deliver mail to root. You should
have aliased root to a mortal user.

See and edit '''/etc/aliases''':

<pre>
root:	justauser
</pre>

then run

<pre>
newaliases
</pre>
