Mail server
===========

.. warning::

   If using Docker - you **MUST** run MTA (Exim or Postfix) using
   **:code:`host` mode** (not bridged). Otherwise your MTA will get only
   local IPs and may become open relay and unavailable to filter spam.

Ready mail servers (using Docker):

#. https://github.com/mailcow/mailcow-dockerized

.. note::

   **Exim vs Postfix** popularity: Exim ~ 50%, Postfix ~ 30%.  I used
   Postfix and could do pretty everything. Exim can do more, is a bit
   more popular.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   exim
   dovecot
   dkim
   antispam_with_rspamd


https://github.com/tomav/docker-mailserver - A fullstack but simple
mailserver (smtp, imap, antispam, antivirus, ssl...) using Docker.





Using Postfix, Dovecot.

Probably add a subdomain (like mail.example.org). Bind zones are here:

.. code-block:: text

   /etc/bind/example.org.zone

   ...
   mail  	        IN	A	10.254.239.1
   ...


SSL
---



SQL configuration
-----------------

dovecot-sql.conf - in this file we say Dovecot

.. code-block:: ini

   driver = pgsql
   connect = host=127.0.0.1 dbname=postfix user=<USER> password=<PASSWORD>
   default_pass_scheme = MD5-CRYPT
   password_query = SELECT username, domain, password FROM mailbox WHERE username = '%u' AND active = TRUE
   user_query = SELECT '/home/vmail/%d/%n' AS home, 'vmail' AS uid, 'vmail' AS gid FROM mailbox where username = '%u' AND active = TRUE


Password schemes: http://wiki2.dovecot.org/Authentication/PasswordSchemes

MD5-CRYPT: A weak but common scheme often used in /etc/shadow. The
encrypted password will start with $1$

.. code-block:: ini

    %u = entire :code:user@domain
    %n = user part of user@domain (user)
    %d = domain part of user@domain  (domain)

Commonly used available substitutions (see
http://wiki2.dovecot.org/Variables for full list):


Add a mailbox
-------------

1. Generate a password with MD5-CRYPT (why MD5-CRYPT? where did I see
   this shit? PostfixAdmin?). A command below.
2. Run SQL code to insert a new mailbox to a database.



To generate password:

.. code-block:: text

   openssl passwd --help
   -6                  SHA512-based password algorithm
   -5                  SHA256-based password algorithm
   -apr1               MD5-based password algorithm, Apache variant
   -1                  MD5-based password algorithm

.. code-block:: text

   openssl passwd -6  # 6 means SHA512
   Password: 123
   Verifying - Password: 123"
   $6$.fUnMADfvMJnym/H$5aGs4LqSiW7o0bBJEN7pJvSnFtNbF.cZxqLQZJxpFsputPMUaedm3Qzx9p1wCqZEebWOPTz8hdLzPCYzEbsEC.


.. code-block:: sql

   INSERT INTO mailbox (username, password, domain, active) VALUES ('dmarc', '$6$.fUnMADfvMJnym/H$5aGs4LqSiW7o0bBJEN7pJvSnFtNbF.cZxqLQZJxpFsputPMUaedm3Qzx9p1wCqZEebWOPTz8hdLzPCYzEbsEC.', 'pashinin.com', TRUE);


Remove mailbox
--------------

Just DELETE from a database table:

.. code-block:: sql

   DELETE from mailbox WHERE username='';


Sieve
-----

Sieve is a language for filtering e-mail messages (antispam
actions).

Create sieve file:

.. code-block:: bash

   mkdir -p /etc/dovecot/sieve
   touch /etc/dovecot/sieve/default.sieve

   # important!
   chown -R vmail /etc/dovecot/sieve

.. code-block:: bash

   require "fileinto";
   if header :contains "X-Spam-Flag" "YES" {
       fileinto "Junk";
   }

Dovecot mailboxes.conf:


.. code-block:: ini

   mailbox Junk {
     special_use = \Junk
     # auto = create
     auto = subscribe
   }

Dovecot lda.conf:


.. code-block:: ini

   protocol lda {
        postmaster_address = postmaster

        # default - commented:
        # mail_plugins = sieve
        mail_plugins = $mail_plugins sieve

        quota_full_tempfail = yes
        deliver_log_format = msgid=%m: %$
        rejection_reason = Your message to <%t> was automatically rejected:%n%r
   }


Edit /etc/dovecot/conf.d/90-sieve.conf:

.. code-block:: ini

   # point to your sieve config
   plugin {

      sieve = /etc/dovecot/sieve/default.sieve


http://sieve.info/
