Connect_Postfix_with_PostgreSQL
===============================

Postfix should know in what files to write your new mail. So it needs '''email<-->folder''' mapping.

I assume you have:

# database [[PostfixAdmin on Ubuntu |created by PostfixAdmin]]
# separate user "vmail" (which in my case has id=1004) for storing your mailboxes


Now create a folder:

<syntaxhighlight lang="bash">
/etc/postfix/sql
</syntaxhighlight>

Future content of which is following - [[File:postfix_sql.7z|download it]]:

<syntaxhighlight lang="bash">
# all the rules for "email-directory" mapping
virtual_alias_domain_catchall_maps.cf
virtual_alias_domain_mailbox_maps.cf
virtual_alias_domain_maps.cf
virtual_alias_maps.cf
virtual_domains_maps.cf
virtual_mailbox_maps.cf
</syntaxhighlight>

* Extract it to /ets/postix/sql dir.

* In each file change "user" and "password" to connect to your DB.

* Now add these lines in your '''/etc/postfix/main.cnf'''

<syntaxhighlight lang="bash" enclose="div">
...

virtual_mailbox_domains = proxy:pgsql:/etc/postfix/sql/virtual_domains_maps.cf

virtual_alias_maps =
   proxy:pgsql:/etc/postfix/sql/virtual_alias_maps.cf,
   proxy:pgsql:/etc/postfix/sql/virtual_alias_domain_maps.cf,
   proxy:pgsql:/etc/postfix/sql/virtual_alias_domain_catchall_maps.cf


virtual_mailbox_maps =
   proxy:pgsql:/etc/postfix/sql/virtual_mailbox_maps.cf,
   proxy:pgsql:/etc/postfix/sql/virtual_alias_domain_mailbox_maps.cf

virtual_gid_maps = static:1004
virtual_mailbox_base = /home/vmail   # all dirs like "user@yourdomain.com/" with mail files are here!
virtual_minimum_uid = 1004
virtual_transport = dovecot
virtual_uid_maps = static:1004
</syntaxhighlight>

== Info ==

Lets take an email address ''user@mail.example.com'':

<syntaxhighlight lang="bash">
# user@mail.example.com
%s   - user@mail.example.com
%d   - mail.example.com
%u   - user
%1   - com
%2   - example
%3   - mail
</syntaxhighlight>

[http://www.postfix.org/pgsql_table.5.html Details...]
