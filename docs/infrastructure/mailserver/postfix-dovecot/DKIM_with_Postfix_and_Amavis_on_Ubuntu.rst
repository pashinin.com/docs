DKIM_with_Postfix_and_Amavis_on_Ubuntu
======================================

DomainKeys Identified Mail (DKIM) is a step to reduce spam. What it does - it makes other servers know that a mail was sent exactly from your server and it's not a fake.

I assume you have installed:

# [[Install Bind DNS-server on Ubuntu |DNS-server]]
# [[Install Postfix on Ubuntu |Postfix]]
# [[Install Amavisd on Ubuntu |Amavis]]
# can send a mail via Postfix

Now let's make DKIM signing.

* Create a dir for your DKIM keys, make it readable only for amavis:

<syntaxhighlight lang="bash">
mkdir /var/dkim
chown amavis:amavis /var/dkim
cd /var/dkim
</syntaxhighlight>

* Create keys for signing our mail (''these are not keys for SSL, no verification needed, just do as shown''):

<syntaxhighlight lang="bash">
amavisd-new genrsa example1.com.pem
#Private RSA key successfully written to file "example1.com.pem" (1024 bits, PEM format)
amavisd-new genrsa example2.com.pem
#Private RSA key successfully written to file "example2.com.pem" (1024 bits, PEM format)
</syntaxhighlight>


After this do following steps:

# [[Configure Amavis to sign my mail with DKIM keys |Configure Amavis to sign mail with DKIM keys]]
# [[Add DKIM keys to my domain zone file]]
# [[Connect Postfix and Amavis]]


== Test ==

Send a mail via your server. See it's source code - you need to see something like this:

.. code-block:: text

   ...
   DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/simple; d=YOURDOMAIN.TLD; h=
   	content-transfer-encoding:content-type:content-type:subject
   	:subject:mime-version:user-agent:from:from:date:date:message-id
   	:received:received; s=main; t=1375446256; x=1377260657; bh=g3zLY
   	H4xKxcPrHOD18z9YfpAcnk/GaJedfustWU5uGs=; b=ktbEZozfdSSoaNNnemRl3
   	9D2ozHGh6igwp5GwZJ1kAbsYK341gjTgJOqxX6aBsIfTsaEAp+q0MveRI7Q7WYzf
   	wUkxBjr/bP/bOYACEECsqdLyQFNZ5TqFBZs2XFt+fdG5GXJRc+8hHACCpiTWShzM
   	WL17pF6p5Nagqq5FYwM06Y=
   ...


[[Category:DKIM]]
[[Category:Postfix]]
