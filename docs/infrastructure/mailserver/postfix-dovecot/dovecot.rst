dovecot
=======

Dovecot is a program that gives received mail to a user (and the mail user sends to your mail server) . So it stands between a user and a mail server.

<syntaxhighlight lang="bash">
sudo apt-get install dovecot-common dovecot-imapd dovecot-pop3d
</syntaxhighlight>

<syntaxhighlight lang="bash">
# How is it going?
service dovecot status             # start | stop | reload
dovecot start/running, process 1544

# What ports are used?
netstat -anp | grep dovecot
#tcp        0      0 0.0.0.0:110             0.0.0.0:*               LISTEN      1544/dovecot
#tcp        0      0 0.0.0.0:143             0.0.0.0:*               LISTEN      1544/dovecot
#tcp        0      0 0.0.0.0:4190            0.0.0.0:*               LISTEN      1544/dovecot
#tcp        0      0 0.0.0.0:993             0.0.0.0:*               LISTEN      1544/dovecot
#tcp        0      0 0.0.0.0:995             0.0.0.0:*               LISTEN      1544/dovecot
</syntaxhighlight>

About ports:

<pre>
110   - POP
143   - IMAP4
4190  - ManageSieve Protocol
993   - SSL IMAP
995   - SSL-POP3
</pre>

== What next ==

# [[Configure Dovecot]]
# [[Installing Dovecot - errors]]

[[Category: Dovecot]]
