Свой почтовый сервер на Postfix, Dovecot, PostgreSQL
====================================================

## Теория

Все пользователи (почтовые адреса) будут храниться в таблице PostgreSQL.
При этом сама почта будет находиться в файлах на диске - одно письмо - один файл.

Таблица может быть в принципе любой, создана, например, при установке PostfixAdmin. Главное чтобы там было поля с адресом и паролем.

## Postfix + Dovecot

```
sudo apt-get install postfix
sudo apt-get install dovecot-pgsql
```

### Связь Postfix и Dovecot

Когда приходит письмо, Postfix должен все проверить (есть ли такой адрес у нас на сервере)

.. code-block:: text

   virtual_alias_maps =
     proxy:pgsql:/etc/postfix/sql/alias_maps.cf,
     proxy:pgsql:/etc/postfix/sql/alias_domain_maps.cf,
     proxy:pgsql:/etc/postfix/sql/alias_domain_catchall_maps.cf,

   virtual_mailbox_domains =
     proxy:pgsql:/etc/postfix/sql/domains_maps.cf,

   virtual_mailbox_maps =
     proxy:pgsql:/etc/postfix/sql/mailbox_maps.cf,
     proxy:pgsql:/etc/postfix/sql/alias_domain_mailbox_maps.cf,

   virtual_mailbox_base = /home/vmail

   virtual_uid_maps = static:1001
   virtual_gid_maps = static:1001


virtual_alias_maps нужен для того, чтобы изменять адрес получателя. Например, если приходит письмо на ящик "example@pashinin.com", а мы хотим, чтобы оно попало в другой, чтобы почта на имя какого-то пользователя "username" попадала в "username@example.com".


## SPF

SPF - это TXT запись зоны DNS ( [https://github.com/pashinin-com/pashinin.com/blob/master/configs/bind.zone.mustache пример] ), которая скажет другим серверам, что если им пришло письмо с нашего домена, то это и правда так. То есть кому-то пришло письмо от `user@example.org`, тот сервер думает - кто-то мне отправил письмо и выдает себя за `example.org`, надо бы проверить настоящий example.org, смотрит запись DNS настоящего example.org - а там говорят - мы разрешаем отправку от имени `example.org` и с такого-то IP адреса. Если все совпадает - проверка пройдена.

.. code-block:: text

   ...
   @   IN  TXT    "v=spf1 +mx +ip4:{{ip}} +a:{{domain}} +a:server451.static.corbina.ru -all"
   ...


Устанавливать нужно всегда.

## DKIM


## SpamAsassin

### Обновление

Чтобы скачать новые конфигурационные файлы (rules, scores, etc,):

.. code-block:: text

   sa-update


Откуда скачивается? Каналы, которые использует `sa-update` по умолчанию - updates.spamassassin.org, но кто-угодно может сделать свой канал и публиковать обновления.

Файлы обновлений лежат в `/var/lib/spamassassin/<spamassassin version>`:

.. code-block:: text

   ls -la /var/lib/spamassassin/3.004001/
   total 16K
   drwxr-xr-x 3 root         root         4.0K Oct 24 01:17 ./
   drwxr-xr-x 2 root         root         4.0K Oct 24 01:17 updates_spamassassin_org/
   -rw-r--r-- 1 root         root         2.8K Oct 24 01:17 updates_spamassassin_org.cf


Скрипт обновления в Ubuntu `/etc/cron.daily/spamassassin` запускается раз в день. Он делает `sa-update`, компилирует скачанные правила, `reload` SpamAsassin'а.

## Sieve (Dovecot)

https://stackoverflow.com/questions/24256008/how-to-move-spam-to-spam-folder

## Autoconfig, autodiscover

https://www.rzegocki.pl/blog/adding-email-server-autoconfig-and-autodiscover/
