Configure Postfix
=================

Postfix configuration is done in 2 files:

<syntaxhighlight lang="bash" enclose="div">
/etc/postfix/main.cf            # server parameters
/etc/postfix/master.cf          # black magic =)
</syntaxhighlight>

== main.cf ==

Set some basic server parameters like [[How to get my own domain?|hostname]], relay adresses...

<syntaxhighlight lang="bash" enclose="div">
...
myhostname = mail.yourdomain.com
...
# relay from (IPs that can send mail through our server without any verification!!!)
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 10.0.0.2
...

</syntaxhighlight>

== master.cf ==

Tell Postfix we want to use Dovecot to work with IMAP or POP3. Add in the end of '''/etc/postfix/master.cf''':

<syntaxhighlight lang="bash">
# /etc/postfix/master.cf
# ...
dovecot unix - n n - - pipe flags=DRhu user=vmail:vmail argv=/usr/lib/dovecot/deliver -d ${recipient}
</syntaxhighlight>

== Useful ==

Useful command "'''postconf -e'''" allows you to add settings in "/etc/postfix/main.cf".<br>
Settings are applied immediately without Postfix restart.

<syntaxhighlight lang="bash">
postconf -e 'mynetworks = 127.0.0.0/8'
postconf -e 'inet_interfaces = all'
</syntaxhighlight>


<syntaxhighlight lang="bash">
postsuper –d ALL      # clear the messaging queue
</syntaxhighlight>

Generate MD5-CRYPT of a password:

<syntaxhighlight lang="bash">
doveadm pw -s MD5-CRYPT -p 12345 | sed 's/{MD5-CRYPT}//'
$1$aaY0jSv/$.fKpF9as01ahxyVbCwTAT/
</syntaxhighlight>


http://www.postfix.org/pgsql_table.5.html




Connect_Postfix_and_Amavis
--------------------------

Amavis by default listens on port 10024.

The only parameter in '''/etc/postfix/main.cf''' that you should modify is "content-filter"

<syntaxhighlight lang="bash">
...

content_filter = smtp-amavis:[127.0.0.1]:10024

...
</syntaxhighlight>

Now Postfix doesn't know what "smtp-amavis" is. So we need to describe it '''/etc/postfix/master.cf'''

<syntaxhighlight lang="bash">
...

smtp-amavis     unix    -       -       -       -       2       smtp
        -o smtp_data_done_timeout=1200
        -o smtp_send_xforward_command=yes
        -o disable_dns_lookups=yes
        -o max_use=20
</syntaxhighlight>

And restart Postfix:

<syntaxhighlight lang="bash">
service postfix restart
# * Stopping Postfix Mail Transport Agent postfix
#   ...done.
# * Starting Postfix Mail Transport Agent postfix
#   ...done.
</syntaxhighlight>
