SPF record for my domain
========================

.. warning::

   SPF record is a MUST-have feature. Without it all your mail probably
   goes to a spam folder in Gmail.

By now you should already have:

#. Your own domain name
#. Authoratative DNS server with a zone file for your domain

SPF record is just a TXT record included in a zone file.

So in your zone file add a text record like this:

.. code-block:: text

   @   IN  TXT    "v=spf1 +mx +ip4:89.179.240.127 +a:pashinin.com +a:server451.static.corbina.ru -all"


:code:`v=spf1` - just a version

Actions:

.. code-block:: text

   "+"	Pass           # accept, host is allowed to send
   "-"	Fail           # reject, host is NOT allowed to send
   "~"	SoftFail       # accept but mark
   "?"	Neutral        # no info, accept


:code:`-all` in the end means restrict all except what is before with :code:`+`.

:code:`mx` means all MX record in your zone file.

Instead of an IP and domains - insert your own IP and domains.

'''Example:'''

.. code-block:: text

   $TTL  3D                ; Time to live
   @	IN	SOA	ns0.yourdomain.com. yourmail.yourdomain.com. (
   			201302312
   			3H
   			15
   			4W
   			1D )

   		NS	ns0              ; DNS name server 1
   		NS	ns1              ; DNS name server 2 (at least 2)

   ;		MX	10 mail         ; Primary Mail Exchanger
   ;		MX	20 mail2        ; Secondary Mail Exchanger

   yourdomain.com.	                A       127.0.0.1
   ns0				A	127.0.0.1
   ns1				A	127.0.0.1

   s1				A	127.0.0.1
   www				CNAME	yourdomain.com.

   @   IN  TXT    "v=spf1 +mx +ip4:93.81.255.77 +a:domain1.tld +a:domain2.tld +a:domain3.tld -all"



* Restart your DNS server

This server will tell that "domain1.tld", "domain2.tld" and "domain3.tld" are allowed to send mail from this server.

== Check ==

1. Check a TXT record of a DNS server:


.. code-block:: text

   dig TXT xdev.me

   #;; ANSWER SECTION:
   #xdev.me.		259200	IN	TXT	"v=spf1 +mx +ip4:93.81.255.77 +a:pashinin.com +a:xdev.me +a:xdev.static.corbina.ru -all"



2. Send a mail from your server to (for example) Gmail and then look at the mail's source code. It should contain "pass":


.. code-block:: text

   Received-SPF: pass (google.com: domain of yourmail@domain.tld designates 93.81.255.77 as permitted sender) client-ip=93.81.255.77;
