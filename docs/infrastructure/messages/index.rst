Messages (event streaming platform)
===================================

.. toctree::
   :maxdepth: 1
   :caption: Contents

   what_to_use
   nats
   kafka







API + worker in one binary. Messages through channels.
------------------------------------------------------

This solution can not be used - I need persistent messages. Messages
sent through channels between threads will be lost on program restart.
