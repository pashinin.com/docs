Kafka
=====

No more autocreated topics in Kafka
-----------------------------------

When using a consumer I had an error:

.. code-block:: text

   Message consumption error: UnknownTopicOrPartition

Answer from Github (librdkafka):

    Since v1.6.0 the consumer no longer triggers topic auto creation
    (regardless of allow.auto.create.topics=..) for subscribed topics.
    Topics referenced through other means, such as assign(), etc, will
    be auto created.


Help
----

.. code-block:: bash

   # enter bash
   docker-compose exec kafka bash

   # View all Kafka topics
   kafka-topics --list --zookeeper zookeeper:2181

   # To see how many message are there
   kafka-log-dirs --describe --bootstrap-server localhost:9092 --topic-list outcomes

   # Consumers list
   kafka-consumer-groups --bootstrap-server 127.0.0.1:9092 --list

   # Configure topic
   # will error if topic does not exist
   kafka-configs --bootstrap-server 127.0.0.1:9092 --entity-type topics --entity-name <TOPIC_NAME> --describe --all
