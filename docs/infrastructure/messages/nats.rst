NATS
====

NATS (At-Most-Once Delivery)
----------------------------

Port 4222.

NATS (`Github <https://github.com/nats-io/nats-server>`_) is not
persistent and does not guarantee delivery. So NATS servers are used for
unimportant messages only (like informing users about
adding/removing/updating trees).

Websocket handler listenes to these messages and sends them to the end
user over a websocket connection.


JetStream (At-Least-Once Delivery)
----------------------------------

JetStream is actually NATS started with :code:`-js` flag. Written in
Golang.

`JetStream <https://docs.nats.io/jetstream/jetstream>`_ will be run in
clustered mode and will replicate data, so the best place to store
JetStream data would be locally on a fast SSD. One should specifically
avoid NAS or NFS storage for JetStream.

To enter NATS console in development:

.. code-block:: bash

   docker run --network dev_default --rm -it synadia/nats-box

.. code-block:: rust

   // asd
