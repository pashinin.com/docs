What to use as a message broker?
================================

Kafka
-----

Used by lots of organizations. Can do anything related to messaging.

Problems:

1. Written in Java = shit eating memory.
2. Too heavy
3. Fucking monster


NATS and JetStream
------------------

It is very light, written in Go. JetStream is just a mode to run NATS.

NATS has at-most-once delivery (can loose messages).

JetStream has at-least-once delivery (need to :code:`ack()` a message on
success).

Problems:

1. JetStream Rust client is not ready (no async support).
2. NATS is not so popular as Kafka

Conslusion:

NATS is ok to use for messages that can be lost.

JetStream is too early to use.
