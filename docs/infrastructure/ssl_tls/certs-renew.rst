Renew certificates
==================

.. note::

   I use a :code:`batch` Nomad job with :code:`acme.sh` Docker image to
   update my certs. It will run same command - :code:`acme.sh --cron`.

.. note::

   After migrating to Nomad - remove acme.sh from user's crontab.

   .. code-block:: text

      acme.sh --uninstall-cronjob

      # To check (this line should disappear)
      crontab -l -u root
      58 0 * * * "/root/.acme.sh"/acme.sh --cron --home "/root/.acme.sh"

.. note::

   In multi-region setup - each datacenter has it's own Vault
   storage. So all certs are unique per datacenter and renewal jobs run
   in each datacenter.
