How to disable TLS 1.0 and 1.1
==============================

The place where SSL/TLS should be terminated is your load balancer.

Fabio load balancer
-------------------

Add :code:`tlsmin=tls12` settings to :code:`proxy.addr` setting.

.. code-block:: text

   proxy.addr = :80, \
                :443;cs=mycerts;tlsmin=tls12
