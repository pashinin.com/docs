HTTP Strict Transport Security (HSTS)
=====================================

.. note::

   :code:`max-age` defines the time-to-live of the effect HSTS has in
   seconds.  Recommended - 31536000 (12 months) or 63072000 (24 months).

.. note::

   :code:`includeSubDomains`. Subdomains can manipulate cookies,
   potentially opening up a variety effects. Setting includeSubDomains
   is highly recommended.


https://hstspreload.org/?domain=pashinin.com


In Fabio
--------

.. code-block:: text

   proxy.header.sts.maxage = 31536000
   proxy.header.sts.subdomains = true
