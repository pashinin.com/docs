SSL/TLS certificates
====================

You need SSL/TLS certificates in order to have a "secured" address
bar for your site:

.. image:: address_bar_secured.png

But other services (like mail server) also need them. Of course
LetsEncrypt certs are used. They are updated with `acme.sh
<https://github.com/Neilpang/acme.sh>`_ using DNS challenge (adding TXT
record, checking, then removing it). So DNS server was set up for
dynamic changes of a zone.

.. note::

   You can check SSL `here
   <https://www.ssllabs.com/ssltest/index.html>`_. See :doc:`"bad"
   report with fixes <./ssl_report>`

.. note::

   It is mandatory to use DNS challenge. It allows to issue wildcard
   certificates like :code:`*.pashinin.com`.


1. Configure DNSSEC in a datacenter
2. Run these 3 commands in each DC:

.. code-block:: bash

   nomad run certs-issue.nomad
   nomad run certs-cron.nomad
   nomad run certs-deploy.nomad


Install acme.sh (as root)
-------------------------

.. code-block:: bash

   curl https://get.acme.sh | sh
   # Installed to /root/.acme.sh/acme.sh
   # Installing alias to '/root/.bashrc'

It will install a cron job for a user, **but** you will not find it in
/etc/cron... files! You can see user's cron jobs by :code:`crontab
-l`. To edit them: :code:`crontab -e`.

DNS config
----------

todo: nsupdate

DNS is also configured to resolve "consul" domains. So that "dig
vault.service.consul" returns an IP of unsealed Vault server.


Get certs (without deploy hook)
-------------------------------

My DNS server is the same machine which runs acme.sh, and DNS server
allows "local" changes. So I don't need to specify a key to update DNS
records. Though DNS server is configured for remote updates using a key.

.. code-block:: bash

   # Set this variables only if your DNS server is not the same machine
   # export NSUPDATE_SERVER="10.254.239.4"
   # export NSUPDATE_KEY="/etc/bind/keys/update.key"
   acme.sh --staging --issue -d pashinin.com  -d '*.pashinin.com' --dns dns_nsupdate --dnssleep 2
   # ...
   # Your cert is in  /root/.acme.sh/<domain>/<domain>.cer
   # Your cert key is in  /root/.acme.sh/<domain>/<domain>.key
   # The intermediate CA cert is in  /root/.acme.sh/<domain>/ca.cer
   # And the full chain certs is there:  /root/.acme.sh/<domain>/fullchain.cer


These NS variables are written to account.conf. So acme.sh remembers them.


Distribute certs
----------------

I use Vault to securely store certs and other sensitive data. Vault uses
Consul as a key-value storage. acme.sh needs following environment
variables for vault.sh deploy script:

.. code-block:: text

   VAULT_PREFIX - this contains the prefix path in vault ("acme")
   VAULT_ADDR - to find your vault server ("http://127.0.0.1:8200"), not https

So I added them to .acme.sh/account.conf file:


.. code-block:: text

   VAULT_PREFIX="acme"
   VAULT_ADDR="http://127.0.0.1:8200"


The full command I'm running is:

.. code-block:: bash

   acme.sh --staging --issue -d pashinin.com  -d '*.pashinin.com' --dns dns_nsupdate --dnssleep 2 --deploy --deploy-hook vault

acme.sh remembers arguments so when a cron job is running it will deploy
as I specified. It remembers them in <domain>/<domain>.conf.


Vault
-----

"acme" policy
^^^^^^^^^^^^^

First I created "acme" path in Vault:

.. code-block:: bash

   # "acme" policy
   #
   # "list" permission - Fabio needs it to scan certs
   vault policy write -address=http://0.0.0.0:8200 acme - <<EOF
   path "acme/*" {
     capabilities = ["create", "update", "read", "list"]
   }
   path "acme/data/*" {
     capabilities = ["create", "update", "read", "list"]
   }
   EOF
   Success! Uploaded policy: acme

   vault policy list -address=http://0.0.0.0:8200   # to see "acme" was created
   vault policy read -address=http://0.0.0.0:8200 acme


But then I need a token to actually write or I'll get an error:

.. code-block:: text

   Error writing data to acme/pashinin.com/cert.pem: Error making API request.

   URL: PUT http://vault.service.consul:8200/v1/acme/pashinin.com/cert.pem
   Code: 400. Errors:

   * missing client token

"acme" role
^^^^^^^^^^^

.. code-block:: bash

   # vault write auth/approle/role/acme policies="acme"
   vault write -address="http://0.0.0.0:8200" auth/approle/role/acme policies="acme" token_ttl="1m" secret_id_ttl="1m"
   vault read auth/approle/role/acme
   vault read auth/approle/role/acme/role-id
   Key        Value
   ---        -----
   role_id    xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx

Then I just add ROLE_ID to account.conf:

.. code-block:: text

   ROLE_ID="xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"

Generate secret_id (by human or provision software, Nomad?):

.. code-block:: bash

   vault write -field=secret_id -f auth/approle/role/acme/secret-id

   # List Secret ID Accessors (useful to revoke tokens):
   vault list auth/approle/role/acme/secret-id
   # <id1>
   # <id2>
   # <id3>

Then create a token:

.. code-block:: bash

   vault token create -policy=acme -display-name="For acme.sh"
   # -period=3d


.. code-block:: text

   --pre-hook


no handler for route (Vault error)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

   vault auth list -address=http://0.0.0.0:8200
   vault secrets list -address=http://0.0.0.0:8200
   vault secrets disable =acme

   Solution:

.. code-block:: bash

   vault secrets enable -address=http://0.0.0.0:8200 -path=acme kv
   vault auth enable -address=http://0.0.0.0:8200 approle


Certificate Authority Authorization (CAA)
-----------------------------------------

CAA is a type of DNS record that allows site owners to specify which
Certificate Authorities (CAs) are allowed to issue certificates
containing their domain names.

.. code-block:: text

   pashinin.com.        IN CAA  0 issue "letsencrypt.org"
   pashinin.com.        IN CAA  0 iodef "mailto:sergey@pashinin.com"

1. My certificates may be only from LetsEncrypt
2. уведомления о возможных несанкционированных попытках выдачи таких
   сертификатов будет отправляться на указанную электронную почту.


DH params
---------

TODO



Templates in Terraform
----------------------

Old:

.. code-block:: bash

   # acme/pashinin.com/fullchain.pem
   template {
     data = <<EOH
   {{ with secret "acme/pashinin.com/fullchain.pem" }}
   {{ .Data.value }}
   {{ end }}
   EOH
     destination = "secrets/fullchain.pem"
   }

   # acme/pashinin.com/cert.key
   template {
     data = <<EOH
   {{ with secret "acme/pashinin.com/cert.key" }}
   {{ .Data.value }}
   {{ end }}
   EOH
     destination = "secrets/cert.key"
   }

New (when certs are saved in "Fabio" path):

.. code-block:: bash

   template {
     data = <<EOH
   {{ with secret "acme/fabio/certs/pashinin.com" }}
   {{ .Data.cert }}
   {{ end }}
   EOH
     destination = "secrets/fullchain.pem"
   }

   template {
     data = <<EOH
   {{ with secret "acme/fabio/certs/pashinin.com" }}
   {{ .Data.key }}
   {{ end }}
   EOH
     destination = "secrets/cert.key"
   }


.. toctree::
   :maxdepth: 2
   :caption: Contents

   certs-renew
   ssl_report
