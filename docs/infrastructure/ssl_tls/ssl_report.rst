Example of a "bad" SSL report (with fixes)
==========================================

.. image:: ssl_report_B.png


Disable TLS 1.0/1.1
-------------------

:doc:`How to do it <how_to_disable_tls10_11>`

.. note::

   By PCI standards TLS 1.0 should not be used after 2016.

.. warning::

   Disabling TLS 1.0 and 1.1 will cause problems for IE 6-10, Safari
   5-6, Android 2-4.3.

..
   ..
      .. image:: ssl_old_browsers.png

After disabling TLS 1.0 and 1.1:

.. image:: ssl_report_A.png


HTTP Strict Transport Security (HSTS)
-------------------------------------

HTTP Strict Transport Security (HSTS) with long duration deployed on
this server.  :doc:`HSTS <hsts>`


DNS Certification Authority Authorization (CAA)
-----------------------------------------------

DNS Certification Authority Authorization (CAA) Policy found for this
domain.  MORE INFO »


.. toctree::
   :maxdepth: 1
   :caption: Contents

   how_to_disable_tls10_11
   hsts
