Tarantool
=========

Tarantool is not used anyhow now. Maybe put users table and
authentication into it...

.. note::

   Tarantool master-master replication can cause conflicts (when updating
   a row on multiple masters). Solution - do not use UPDATEs. Just
   use INSERTS and use "latest" record as a current state. Can always
   delete very old records automatically. Plus you will get a history of
   changes.

I'm running Tarantool on 3 nodes in full-mesh master-master mode.
