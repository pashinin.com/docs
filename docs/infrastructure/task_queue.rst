Task queue
==========

Task queue must work in "At-Least-Once" mode. I use
:code:`NATS-streaming`.

.. note::

   Other unimportant messages (which can be lost) can still use core NATS
   working in "At-Most-Once" mode.

.. warning::

   By default when using NATS-streaming if a worker has not yet finished
   a job in 30 seconds - NATS re-sends this message again. By that time
   the first running job may have finished and will return ACK. So in
   total - we had 2 jobs at least. If a job is running for a long time -
   each 30 seconds a new message will be sent from NATS until at least 1
   task is finished.

Durable queue group is created to not loose tasks even if there is no
worker now.
