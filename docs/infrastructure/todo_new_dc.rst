New datacenter (checklist)
==========================

#. Install Consul server, open tcp ports 8300, 8302; and join using :code:`-wan`

   .. warning::

      Before installing - uncomment "bootstrap" in Consul config for the
      first time!  Then comment it back.

   .. code-block:: bash

      consul join -wan pashinin.com

   (optional) remove old server from cluster:

   .. code-block:: bash

      consul force-leave us-east-server-1.us-east-2

#. Install Vault, initialize and add "nomad-cluster" role

   .. note::

      See Nomad-Vault integration section

#. Create "acme" path and "acme" role in Vault to save and load certificates

   .. code-block:: bash

      # /acme path
      vault secrets enable -address=http://0.0.0.0:8200 -path=acme kv-v2
      vault auth enable -address=http://0.0.0.0:8200 approle

      # "acme" policy
      #
      # "list" permission - Fabio needs it to scan certs
      vault policy write -address=http://0.0.0.0:8200 acme - <<EOF
      path "acme/*" {
        capabilities = ["create", "update", "read", "list"]
      }
      path "acme/data/*" {
        capabilities = ["create", "update", "read", "list"]
      }
      EOF
      Success! Uploaded policy: acme

      # "acme" role
      vault write -address="http://0.0.0.0:8200" auth/approle/role/acme policies="acme" token_ttl="1m" secret_id_ttl="1m"
      Success! Data written to: auth/approle/role/acme

      # Check "acme" was added
      vault secrets list -address="http://0.0.0.0:8200"

#. Install Nomad server

   Do not forget to uncomment "bootstrap" for the first time!

   Open ports (tcp/udp):

   .. code-block:: bash

      -A INPUT -s 89.179.240.127 -p tcp -m tcp --dport 4647:4648 -j ACCEPT
      -A INPUT -s 89.179.240.127 -p udp -m udp --dport 4647:4648 -j ACCEPT


#. Put DNS update key into Vault

   Just take a key from a working datacenter.

   Do not forget to replace :code:`...` with your key.

   .. code-block:: bash

      # generate key with knot-dns tools
      keymgr -t update    # "update" is a key name
      # tsig-keygen -a HMAC-SHA512 update

      # hmac-sha256:update:QJ9/pTl296AXA7ZVY2lpeHKzjTwAUVM0YYVh11pNCsU=
      key:
        - id: update
          algorithm: hmac-sha256
          secret: QJ9/pTl296AXA7ZVY2lpeHKzjTwAUVM0YYVh11pNCsU=

   .. code-block:: bash

      # vault write -address="http://10.254.239.4:8200" acme/dns/update.key value=-<<EOF
      vault kv put -address="http://0.0.0.0:8200" acme/dns/update.key value=-<<EOF
      key "update" {
        algorithm hmac-sha512;
        secret "...";
      };
      EOF
      # vault write -address="http://0.0.0.0:8200" acme/dns/update.secret value=...
      vault kv put -address="http://0.0.0.0:8200" acme/dns/update.secret value=...

      # Check
      vault kv list -address="http://0.0.0.0:8200" acme/dns
      vault kv get -address="http://0.0.0.0:8200" acme/dns/update.key


#. Issue certs, run certs-cron and certs-deploy jobs

   .. code-block:: bash

      nomad run jobs/certs/certs-issue-us.nomad

   certs-deploy is run using Terraform. To force run it:

   .. code-block:: bash

      curl --request POST http://localhost:4646/v1/job/certs-deploy/periodic/force

      # Check
      vault list -address="http://0.0.0.0:8200" acme/fabio/certs/

#. Test HTTPS response on new IP

   .. code-block:: bash

      curl --resolve docs.pashinin.com:443:18.188.240.62 https://docs.pashinin.com/en/

   Must see web page's source code.
