Tree
====

Viewing a Tree
--------------

#. Soft-deleted Trees are visible only for users with
   :code:`tree.restore` or :code:`tree.hard-delete` permission.


Removing Tree rules
-------------------

#. Anons can't delete anything.

#. User can soft-delete (mark deleted) a Tree if he has a Role with
   :code:`tree.soft-delete` permission for this Tree or one of parent Trees.

#. Hard-deleting a Tree does not actually removes a Tree but starts a
   task that will do it shortly.



.. toctree::
   :maxdepth: 2
   :caption: Tree

   order
