Ordering trees
==============

By name
-------

Ordering by name is tricky.

First there is a difference is between :code:`ORDER BY name` and
:code:`ORDER BY lower(name)`. I need case-insensitive ordering but
ordering by :code:`lower(name)` will not use index. I could create index
:code:`on lower(name)` in Postgres but `can't do it in CockroachDB
<https://github.com/cockroachdb/cockroach/issues/9682>`_.

Second - sorting strings with numbers:

.. code-block:: text

   ORDER BY lower(name)    how it should be
   IU-1                    IU-1
   IU-12                   IU-2
   IU-2                    IU-12

Solution may be creating a three new columns:

* lower-cased name part before integer
* integer
* lower-cased name part after integer

and sort by these three columns.

.. code-block:: sql

   -- string before first number
   SELECT (substring('asd-123-456', '[\D]+'));  -- "asd-"


SQL to set name_before_number, name_number, name_after_number
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: sql

   UPDATE trees SET name_before_number=lower(coalesce(regexp_extract(name, '^[\D]+'), ''));
   UPDATE trees SET name_number=coalesce(trim(leading '0' from substring(name, '[\d]+')), '0')::int as name_number;
   UPDATE trees SET name_after_number=lower(coalesce(substring(name, length(name_before_number)+1+length(substring(name, '[\d]+')), length(name)), ''));


By created timestamp
--------------------
