Debian - after installation
===========================

Bash completions for root
-------------------------

.. code-block:: bash

   sudo apt install bash-completion

Then add following lines to ~/.bashrc:

.. code-block:: text

   if [ -f /etc/bash_completion ]; then
       . /etc/bash_completion
   fi
