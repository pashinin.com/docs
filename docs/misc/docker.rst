Docker
======

Segfaults happened in Rust application (while making a http request)
built in this container and running in "scratch":

.. code-block:: text

   FROM rust:alpine as builder
   RUN apk add --no-cache musl-dev openssl-dev
   # ...

   # Bundle Stage
   FROM scratch
   COPY --from=builder /usr/src/app/target/release/app ./



This one is ok:

.. code-block:: text

   FROM rust:1.46.0-slim as builder
   RUN apt-get update && apt-get install -y pkg-config libssl-dev
   # ...

   # Bundle Stage
   FROM debian:buster-slim
   RUN apt-get update && apt-get install -y libssl1.1
   COPY --from=builder /usr/src/app/target/release/app ./


Also `musl can be slow in Docker
<https://andygrove.io/2020/05/why-musl-extremely-slow/>`_.
