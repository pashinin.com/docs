GLIBC
=====

Possible error:

.. code-block:: text

   root@node5:~# ldd vpn
   ./vpn: /lib/x86_64-linux-gnu/libc.so.6: version `GLIBC_2.32' not found (required by ./vpn)
   ./vpn: /lib/x86_64-linux-gnu/libc.so.6: version `GLIBC_2.33' not found (required by ./vpn)
           linux-vdso.so.1 (0x00007fffd4d60000)
           libssl.so.3 => not found
           libcrypto.so.3 => not found
           libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f6900357000)
           libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f6900335000)
           libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f69001f1000)
           libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f69001eb000)
           libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f6900024000)
           /lib64/ld-linux-x86-64.so.2 (0x00007f690121c000)

This happens when I build Rust binary on a new system (Debian 12, with
new glibc) and then try to run this binary on an old system (Debian 11).


Solution:

Create a Docker image with needed OS version, Rust version and then
build a binary from it.
