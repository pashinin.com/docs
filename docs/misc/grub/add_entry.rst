How to add "Windows" entry in GRUB menu
=======================================

1. Detect where is your OS

.. code-block:: bash

   sudo fdisk -l | grep "NTFS"
   /dev/nvme0n1p1 *       2048    1126399   1124352   549M  7 HPFS/NTFS/exFAT
   /dev/nvme0n1p2      1126400 1000212479 999086080 476.4G  7 HPFS/NTFS/exFAT

..
   .. code-block:: bash

      /dev/sda1   *        2048    7999487    3998720   82  Linux swap / Solaris # <-- Our loader (sda1)
      /dev/sda3         8001534  744697855  368348161    f  W95 (LBA)            # <-- Windows partition (sda3)
      /dev/sda5       167979008  356722687   94371840    7  HPFS/NTFS/exFAT
      /dev/sda6       356724736  744697855  193986560    7  HPFS/NTFS/exFAT
      /dev/sda7         8001536  167979007   79988736   83  Linux

2. Create a script for adding "windows" entry:

.. code-block:: bash

   touch /etc/grub.d/15_windows
   chmod +x /etc/grub.d/15_windows

When Windows is on GPT
----------------------

3. Get disk UUID

.. code-block:: bash

   sudo grub-probe -t fs_uuid -d /dev/nvme0n1p1
   6C78837E78834634



4. Use this content in script

.. code-block:: bash

   #! /bin/sh
   set -e
   echo "Adding Windows" >&2
   cat << EOF
   menuentry "Windows 10" {
   insmod part_gpt
   insmod fat
   insmod search_fs_uuid
   insmod chain
   search --fs-uuid --no-floppy --set=root 6C78837E78834634
   chainloader (${root})/efi/Microsoft/Boot/bootmgfw.efi
   }
   EOF


When Windows is on DOS
----------------------

..
   .. code-block:: bash

      #! /bin/sh
      set -e
      echo "Adding Windows" >&2
      cat << EOF
      menuentry "Windows 10" {
      insmod part_msdos
      insmod chain
      search --fs-uuid --no-floppy --set=root 6C78837E78834634
      chainloader (${root})/efi/Microsoft/Boot/bootmgfw.efi
      }
      EOF


.. code-block:: bash

   #! /bin/sh
   set -e
   echo "Adding Windows" >&2
   cat << EOF
   menuentry "Windows 10" {
   insmod part_msdos
   insmod chain
   set root=(hd0,2)
   chainloader +1
   }
   EOF


.. note::

   /dev/sda1  -  (hd0.1)
   /dev/sda2  -  (hd0.2)
   /dev/sda3  -  (hd0.3)


Update your GRUB config

.. code-block:: bash

   sudo grub-mkconfig -o /boot/grub/grub.cfg
