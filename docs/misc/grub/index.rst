GRUB
====

Change default GRUB record
--------------------------

Edit :code:`/etc/default/grub` and set 2 params:

.. code-block:: text

   GRUB_DEFAULT=saved
   GRUB_SAVEDEFAULT=true

Then run:

.. code-block:: bash

   sudo grub-mkconfig -o /boot/grub/grub.cfg


.. note::

   :code:`update-grub` is just a script that runs same
   :code:`grub-mkconfig` command.



On what disk is my GRUB installed
---------------------------------

.. code-block:: bash

   sudo grub-probe -t device /boot/grub

Then you will see either this:

.. code-block:: text

   /dev/sde1

or this (when disk was defined by it's UUID):

.. code-block:: text

   2a85c0a5-facc-4224-b5b5-df0696a925c6
