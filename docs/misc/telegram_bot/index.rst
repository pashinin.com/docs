Telegram bot
============

You can write Telegram bot in several languages:

To create a bot - talk to BotFarther https://core.telegram.org/bots#6-botfather.

Rust
----

#. `teloxide <https://github.com/teloxide/teloxide>`_ (1.2k stars) - asd
#. `telegram-rs/telegram-bot <https://github.com/telegram-rs/telegram-bot>`_ (844 stars)



Python
------

* aiogram - лучший на питоне, советуют использовать
* `python-telegram-bot
  <https://github.com/python-telegram-bot/python-telegram-bot>`_ - для
  простых проектов
