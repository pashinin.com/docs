VPN
===

.. toctree::
   :maxdepth: 2
   :caption: Contents

   openvpn/index
   wireguard/index




Configure VPN clients
---------------------

**Common problems**:

.. warning::

   **Can't connect to VPN**

   When testing VPN server from my LAN I saw **errors telling me to add
   "float" to configuration**. That's because .ovpn config expected
   answers from my external IP but was getting answers from my local IP.

   I really needed to open that .ovpn file and just add a word "float"
   to other params.

.. warning::

   **No DNS**

   I noticed "setenv opt block-outside-dns" option in ".ovpn" client
   config.


   **When connected to VPN I couldn't get any internet access**. Only
   could ping my VPN server but not web sites.

   First I checked default gateway - there should be no records. And it
   was so, ok.

   I added 8.8.8.8 as dns server for VPN connection. That solved the
   problem.

.. warning::

   **Same IP**

   When I connected Linux, Windows and a mobile phone OpenVPN clients -
   they all had the same IP: 10.8.0.2. WTF?

   OpenVPN use the common name (CN) to identify different clients and
   give different IP to clients that use certs with different CN (that's
   then name you enter when creating a new openvpn client).

   Server is actually configured to allow only 1 connection from a
   client. So my machines all had the same IP, yes, but only 1 of them
   was connected to OpenVPn server at time.

   When a new connection from the same client (ovpn file) is up the old
   one is stale.

   ..
      With --duplicate-cn, two connections with same common name are
      allowed, so one cert can be used by more than one connection/users.

      Without --duplicate-cn, every vpn cert must have their own CN, so
      every connection/user have one unique cert.

   ..
      However when I create the certs for all the clients, I use the
      information that I set in the vars.bat file, so all the certs had the
      same CN. Now, when I create a new cert for a client I assign as CN
      the user name.




VPN + SSH or just SSH?
----------------------


User management
---------------


VPN TCP or UDP?
---------------

**UDP**

* Faster Speed – UDP VPN service offers significantly greater speeds
  than TCP. For this reason it is the preferred protocol when streaming
  HD videos or downloading torrents/p2p .
* Preferred – UDP VPN tunnels are the preferred OpenVPN connection
  method if your network supports it.
* Lower Reliability – On rare occasions UDP can be less reliable that
  TCP VPN connections as UDP does not guarantee the delivery of packets.

TCP (bad. TCP over TCP is a bad idea)

* Better Reliability – TCP VPN service offers more stable connections as
  the protocol guarantees delivery of packets.
* **Bypass Firewalls** – TCP VPN tunnels are rarely blocked since they run
  on common ports (80, 443). Usually TCP VPN tunnels can bypass even the
  most strict corporate firewalls.

  Some networks may disallow OpenVPN connections on the default port
  and/or protocol. One strategy to circumvent this is to mimic HTTPS
  traffic which is very likely unobstructed.

  To do so, configure /etc/openvpn/server/server.conf as such:

  /etc/openvpn/server/server.conf

  .
  port 443
  proto tcp
  .


* Slower Speed – TCP features higher encryption methods that tend to
  slow transfer rates a little. For higher transfer speeds with OpenVPN
  use UDP



To see
------

https://github.com/gravitl/netmaker
