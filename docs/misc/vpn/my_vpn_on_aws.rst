My own VPN on AWS (bypass russian firewall)
*******************************************

Install OpenVPN server on AWS
=============================

.. code-block:: bash

   # Download and run installation script
   wget https://git.io/vpn -O openvpn-install.sh && bash openvpn-install.sh

   # To reconfigure VPN later just run:
   # sudo bash openvpn-install.sh

You will create "ovpn" file. Download just created "ovpn" file and
upload it to your router:

.. code-block:: bash

   # Download from AWS:
   scp ubuntu@AWS-IP:/home/ubuntu/MoscowBalancerClient.ovpn ~/MoscowBalancerClient.ovpn
   scp ubuntu@18.188.240.62:/home/ubuntu/MoscowBalancerClient.ovpn ~/MoscowBalancerClient.ovpn
   scp root@199.244.48.210:/root/MoscowBalancerClient.ovpn ~/MoscowBalancerClient.ovpn

   # Upload to my router (client) as .conf file:
   scp ~/MoscowBalancerClient.ovpn root@10.254.239.4:/etc/openvpn/MoscowBalancerClient.conf



Server config
=============


Firewall
========

VPN server
----------

Open 1194 port. In AWS this is done in "Network - security groups - Inboud tab".

iptables:

allow access from 89.179.240.127 IP only

Allow forwarding through OpenVPN server's interface

.. code-block:: text

   # -t nat
   -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE

   # -t filter
   -A INPUT -i eth0 -p udp -m state --state NEW --dport 1194 -j ACCEPT
   -A INPUT -i tun+ -j ACCEPT   # this one helped
   -A FORWARD -i tun+ -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
   -A FORWARD -i eth0 -o tun+ -m state --state RELATED,ESTABLISHED -j ACCEPT
   -A OUTPUT -o tun+ -j ACCEPT


Clients
=======

Masquerade traffic if your network is like "Desktop -> router -> VPN (AWS)"
---------------------------------------------------------------------------

.. code-block:: text

   LAN                                       ------------------------- Internet (ISP)
   ...             Router                   /
   10.254.239.2 -> enp4s0 (10.254.239.4)   /
                   enp3s0 (89.179.240.127)-      VPN server (AWS)
                   tun1 (10.8.0.2) ------------> tun0 (10.8.0.1)
                                                 eth0 (public IP)  --> Internet (ISP)

Some routes go through VPN. All other routes go through ISP.



On router's NAT table (:code:`iptables -t nat`):

.. code-block:: bash

   # MASQUERADE must be enabled on my ISP network card
   iptables -t nat -A POSTROUTING -o enp3s0 -j MASQUERADE

   # Masquerade traffic from LAN to "the world"
   # tun0-1 always switch
   iptables -t nat -A POSTROUTING -o tun3 -s 10.254.239.0/24 -j MASQUERADE
   iptables -t nat -A POSTROUTING -o tun3 -s 10.254.240.0/24 -j MASQUERADE

:code:`tun3` is an interface on balancer that is connected to my AWS VPN
server as a client. IT CHANGES SOMETIMES to tun0! Perhaps the cause is
different start order of OVPN server and client.


Test on Arch Linux machine
--------------------------

.. code-block:: bash

   yay -S openvpn
   sudo openvpn MoscowBalancerClient.ovpn



Use VPN only for some IPs
=========================

Add a line to your OpenVPN client config, like:

.. code-block:: text

   route The.IP.To.Go 255.255.255.255

(Where The.IP.To.Go is the IP you wish to route through the VPN)

This instructs OpenVPN to create the entry in your OS's routing table.

Alternatively, the OpenVPN server could be made to "push" this routing
configuration down to clients, by adding to the server config:

push "route The.IP.To.Go 255.255.255.255"

..
   EDIT: One thing I missed addressing--the default forwarding of all
   traffic... It could either be disabled on the server, or clients can
   elect to ignore "pushed" directives (so our second option "pushing" the
   route would not work) with:

   route no-pull


   For one IP only (192.168.0.1):

   route-nopull
   route 192.168.0.1 255.255.255.255


Re-route yourself for needed sites
==================================

Always use :code:`curl -Lv linkedin.com` to detect which IP fails.

Full /etc/openvpn/server.conf:

.. code-block:: ini

   local 172.31.12.132
   port 1194
   proto udp
   dev tun
   ca ca.crt
   cert server.crt
   key server.key
   dh dh.pem
   auth SHA512
   tls-crypt tc.key
   topology subnet
   server 10.8.0.0 255.255.255.0
   ifconfig-pool-persist ipp.txt
   #push "redirect-gateway def1 bypass-dhcp"
   #push "dhcp-option DNS 172.31.0.2"
   keepalive 10 120
   cipher AES-256-CBC
   user nobody
   group nogroup
   persist-key
   persist-tun
   status openvpn-status.log
   verb 3
   crl-verify crl.pem
   explicit-exit-notify

   #push "route 0.0.0.0 0.0.0.0"

   # dota 2 servers
   #push "route 146.66.156.2 255.255.255.255"
   #push "route 185.25.180.1 255.255.255.255"

   # myip.ru (for testing)
   push "route 178.62.9.171 255.255.255.255"


   # 7-zip.org
   # working at 2020.11.10
   #push "route 159.65.89.65 255.255.255.255"


   # lurkmore.so
   push "route 172.64.163.36 255.255.255.255"
   push "route 172.64.162.36 255.255.255.255"


   # telegram.org
   #push "route 149.154.167.99 255.255.255.255"
   #push "route 149.154.167.220 255.255.255.255"
   #push "route 149.154.167.0 255.255.255.0"
   # 149.154.167.80   updates.tdesktop.com

   # fatofthelan.com
   push "route 206.72.110.45 255.255.255.255"
   #206.72.110.45

   # nnm-club.me
   # nnm-club.to
   push "route 81.17.30.22 255.255.255.255"
   push "route 81.17.30.0 255.255.255.0"
   push "route 134.19.177.67 255.255.255.255"
   push "route 185.53.179.6 255.255.255.255"


   # kinozal.tv
   #kinozal.tv.             86334   IN      A       104.21.63.9
   #kinozal.tv.             86334   IN      A       172.67.142.12
   push "route 104.27.141.68 255.255.255.255"
   push "route 104.27.140.68 255.255.255.255"
   push "route 104.21.63.9 255.255.255.255"
   push "route 172.67.142.12 255.255.255.255"

   # kinkopilka.ru - OUT
   # push "route 94.242.60.7 255.255.255.255"

   # thepiratebay.org
   push "route 104.27.216.28 255.255.255.255"
   push "route 104.27.217.28 255.255.255.255"

   # piratbit.org
   push "route 193.106.30.196 255.255.255.255"

   # rutor.info
   push "route 91.132.60.13 255.255.255.255"

   # tfile.ru
   push "route 185.141.26.93 255.255.255.255"

   #push "route   255.255.255.255"

   # megafile.cc
   push "route 185.165.168.236 255.255.255.255"
   push "route 185.165.168.122 255.255.255.255"


   # vuejs.org
   # working at 2020.11.10
   #push "route 167.99.137.12  255.255.255.255"
   # ssr.vuejs.org
   #push "route 167.99.129.42  255.255.255.255"

   # linkedin.com (many ips)
   push "route 108.174.10.10 255.255.255.255"
   push "route 185.63.144.1 255.255.255.255"
   push "route 185.63.144.5 255.255.255.255"

   # slideshare.net (related to linkedin.com - same network)
   push "route 108.174.10.19 255.255.255.255"
   push "route 185.63.144.10 255.255.255.255"
   push "route 108.174.11.74 255.255.255.255"

   # rutracker.org
   push "route 195.82.146.214 255.255.255.255"
   push "route 172.67.137.176 255.255.255.255"
   push "route 104.21.56.234 255.255.255.255"
   push "route 45.132.105.85 255.255.255.255"
   push "route 46.175.146.105 255.255.255.255"

   # igg-games.com
   push "route 104.27.50.49 255.255.255.255"
   push "route 104.27.51.49 255.255.255.255"

   # tracker.freecad.io
   #push "route 46.101.135.35 255.255.255.255"

   # alleng.org
   push "route 5.45.74.234 255.255.255.255"
   push "route 5.45.72.0 255.255.255.0"


   # howtoinstall.me
   push "route 165.227.204.0 255.255.255.0"


   # youtube.com
   # push "route 173.194.222.0 255.255.255.0"


   # Rust packages (Fuck RKN)
   # static.crates.io
   #push "route 52.84.214.0 255.255.255.0"


After adding more records to openvpn config - do not forget to restart
vpn server:

.. code-block:: bash

   sudo systemctl restart openvpn-server@server.service


Start VPN automatically on boot
===============================

..
   SystemD scripts can be found here: https://github.com/OpenVPN/openvpn/tree/master/distro/systemd

To autostart OpenVPN *client* on Debian (balancer) do following:

.. code-block:: bash

   # Rename OpenVPNConfigFile.ovpn to OpenVPNConfigFile.conf
   sudo mv /etc/openvpn/MoscowBalancerClient.ovpn /etc/openvpn/MoscowBalancerClient.conf

   sudo vim /etc/default/openvpn
   # Uncomment AUTOSTART="all"

   sudo systemctl daemon-reload
   # sudo service openvpn start
   systemctl start openvpn@MoscowBalancerClient.service

.. warning::

   Different versions of OpenVPN may not work (for example a new one and
   one in Ubuntu 16.06). Complains on tls-crypt tag.

/etc/openvpn/server/server.conf
===============================

If you don't need DNS over VPN then disable DNS lines:

.. code-block:: text

   # This setting will route/force all traffic to pass through the VPN.
   # push "redirect-gateway def1 bypass-dhcp"

   ...

   # default route and dns server
   #push "redirect-gateway def1 bypass-dhcp"
   #push "dhcp-option DNS 172.31.0.2"
