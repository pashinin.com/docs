Configure VPN client on Linux
=============================

Using command line
------------------

.. code-block:: bash

   apt install openvpn

Copy .ovpn file to /etc/openvpn/client/myvpn.conf. Then:

.. code-block:: bash

   # start manually:
   openvpn --config /etc/openvpn/client/myvpn.conf

   # as systemd service:
   systemctl start openvpn-client@myvpn.service
   systemctl status openvpn@MoscowBalancerClient.service

..
   openvpn --remote 89.179.240.127 --dev tun1 --ifconfig 10.8.0.2 10.8.0.1


Using Gnome interface
---------------------

sudo apt install network-manager-openvpn-gnome openvpn-systemd-resolved
