OpenVPN client on Windows
=========================

Short story: `Download OpenVPN client (MSI)
<https://openvpn.net/community-downloads/>`_

.. warning::

   You can't use Windows built-in VPN functionality for connecting to
   OpenVPN. The Windows VPN client only supports the protocols L2TP,
   PPTP, IKEv2 and Microsoft's SSTP. OpenVPN is a SSL VPN, and the thing
   with a SSL VPN is that every company has its own Secret Sauce for the
   connection parameters. While in the future Windows could look into
   adding support for OpenVPN in particular, in that case the Protocol
   field will clearly call out OpenVPN.


Copy your config .ovpn file (from server) to C:\users\user\openvpn\configs

Then "Connect" (do not forget to open 1194 port on balancer).
