Firewall
========

Log when started VPN client on Linux:

.. code-block:: text

   openvpn[438465]: TUN/TAP device tun0 opened
   openvpn[438465]: TUN/TAP TX queue length set to 100
   openvpn[438465]: do_ifconfig, tt->did_ifconfig_ipv6_setup=0
   openvpn[438465]: /sbin/ip link set dev tun0 up mtu 1500
   openvpn[438465]: /sbin/ip addr add dev tun0 10.8.0.2/24 broadcast 10.8.0.255
   openvpn[438465]: /sbin/ip route add 10.254.239.4/32 dev enp38s0
   openvpn[438465]: /sbin/ip route add 0.0.0.0/1 via 10.8.0.1
   openvpn[438465]: /sbin/ip route add 128.0.0.0/1 via 10.8.0.1
   openvpn[438465]: Initialization Sequence Completed



NAT
---

Make traffic from the VPN masquerade as traffic from the physical
network interface.

.. code-block:: bash

   iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE

Accepting forwarded packets via the firewall's internal IP device allows
LAN nodes to communicate with each other; however they still are not
allowed to communicate externally to the Internet. To allow LAN nodes
with private IP addresses to communicate with external public networks,
configure the firewall for IP masquerading, which masks requests from
LAN nodes with the IP address of the firewall's external device.
