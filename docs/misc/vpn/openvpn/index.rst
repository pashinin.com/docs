OpenVPN
=======

.. note::

   OpenVPN is an SSL/TLS VPN solution. It is able to traverse NAT
   connections and firewalls.


.. note::

   When I connect any machine (client) to my VPN server (balancer)
   **all** traffic goes through this VPN server.

.. note::

   **OpenVPN** creates a TUN/TAP interface ("tun0").  Возможно создать
   туннель сетевого уровня, называемый TUN, и канального уровня — TAP,
   способный передавать Ethernet-трафик. Также возможно использование
   библиотеки компрессии LZO для сжатия потока данных. Используемый порт
   1194 выделен Internet Assigned Numbers Authority для работы данной
   программы

.. toctree::
   :maxdepth: 2
   :caption: Contents

   server_linux
   server_windows

   client_linux
   client_windows
