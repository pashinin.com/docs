OpenVPN server on Linux
=======================

Installation and notes
----------------------

Modern script: https://github.com/angristan/openvpn-install
Old script: https://git.io/vpn     (Nyr)

Download and run installation script. It will do everything.

.. code-block:: bash

   # wget https://git.io/vpn -O openvpn-install.sh && bash openvpn-install.sh
   curl -O https://raw.githubusercontent.com/angristan/openvpn-install/master/openvpn-install.sh
   chmod +x openvpn-install.sh
   ./openvpn-install.sh

Config: :code:`/etc/openvpn/server.conf`

.. note::

   Allow access from :code:`tun` interface on your future VPN server.
   Using iptables:

   .. code-block:: text

      -A INPUT -i tun1 -j ACCEPT
      -A FORWARD -i tun1 -j ACCEPT


   .. code-block:: bash

      iptables -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
      iptables -I FORWARD -s 10.8.0.0/24 -j ACCEPT
      iptables -I INPUT -p udp --dport 1194 -j ACCEPT
      iptables -t nat -A POSTROUTING -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to 104.237.156.154


.. note::

   The name of interface will always change (tun0/tun1/...) if you have
   both server and client. Edit :code:`/etc/openvpn/server.conf` and
   change one line to :code:`dev tun3` to give an exact name.


Create more clients (on server)
-------------------------------

.. code-block:: bash

   bash openvpn-install.sh
   # choose "Add a new user"
   ...
   Client "Sergey" added, configuration is available at: /root/Sergey.ovpn

.. code-block:: text

   Note: using Easy-RSA configuration from: /etc/openvpn/easy-rsa/vars
   Using SSL: openssl OpenSSL 1.1.1f  31 Mar 2020
   Generating an EC private key
   writing new private key to '/etc/openvpn/easy-rsa/pki/easy-rsa-945945.djds6T/tmp.OhjZbG'
   -----
   Using configuration from /etc/openvpn/easy-rsa/pki/easy-rsa-945945.djds6T/tmp.IJwpv2
   Check that the request matches the signature
   Signature ok
   The Subject's Distinguished Name is as follows
   commonName            :ASN.1 12:'test3'
   Certificate is to be certified until Oct  7 13:44:34 2024 GMT (825 days)

   Write out database with 1 new entries
   Data Base Updated

   Client test3 added.

   The configuration file has been written to /root/test3.ovpn.
   Download the .ovpn file and import it in your OpenVPN client.


Download it:

.. code-block:: bash

   # On Linux:
   scp root@10.254.239.4:/root/Sergey.ovpn ~/

   # On Windows:
   scp root@10.254.239.4:/root/Sergey.ovpn c:\Sergey.ovpn

.. note::

   SSH keys on Windows are located in C:\users\user\.ssh\

   You'll need :code:`pageant` to convert keys to .ppk files.


   Allow Multiple Clients Using Same Certificate
   Uncomment the line:

   ;duplicate-cn

.. note::

   How server knows it's clients?


Static IP for clients
---------------------

**Individual config for each client**

On server create :code:`/etc/openvpn/ccd` folder (client config
directory).

Then put a file there named **exactly as Common Name** when creating a
client.

File: :code:`/etc/openvpn/ccd/Sergey`  (Common Name was :code:`Sergey`)

.. code-block:: text

   ifconfig-push 10.8.0.2 255.255.255.0

**Single config file**

.. code-block:: text

   ifconfig-pool-persist ipp.txt
