VPN server on Windows
*********************

https://community.openvpn.net/openvpn/wiki/Easy_Windows_Guide

Certificates and Keys
=====================

#. Press Windows Key
#. Type "cmd", right click and **run as Administrator!**

.. code-block:: bash

   cd "C:\Program Files\OpenVPN\easy-rsa"
   init-config  # Initialize the OpenVPN configuration

.. note::

   Only run init-config once, during installation.

.. code-block:: text

   # Open the vars.bat file in a text editor:
   notepad vars.bat

   Edit the following lines in vars.bat, replacing "US", "CA," etc. with your company's information:

.. code-block:: text

   set KEY_COUNTRY=US
   set KEY_PROVINCE=CA
   set KEY_CITY=SanFrancisco
   set KEY_ORG=OpenVPN
   set KEY_EMAIL=mail@host.domain

Save the file and exit notepad.

Run the following commands:

.. code-block:: bash

   vars
   clean-all


Certificate authority (CA) certificate and key
----------------------------------------------

.. code-block:: bash

   build-ca

.. warning::

   Had an error: "openssl" command not found. **Solution:** added C:\Program
   files\OpenVPN\bin to PATH variable.

.. warning::

   Had an error: option "-config" needs a value. **Solution:** run
   :code:`vars` command.


The server certificate and key
------------------------------

.. code-block:: bash

   build-key-server server

#. When prompted, enter the "Common Name" as "server"
#. When prompted to sign the certificate, enter "y"
#. When prompted to commit, enter "y"


Client certificates and keys
----------------------------

For each client, choose a name to identify that computer, such as
"my-laptop" in this example.

.. code-block:: bash

   build-key my-laptop

When prompted, enter the "Common Name" as the name you have chosen
(e.g. "my-laptop") Repeat this step for each client computer that will
connect to the VPN.


Generate Diffie Hellman parameters
----------------------------------

.. note::

   This is necessary to set up the encryption.

.. code-block:: bash

   build-dh



Configuration Files
===================

Find the sample configuration files:

Start Menu -> All Programs -> OpenVPN -> OpenVPN Sample Configuration Files

Server Config File
------------------

Open server.ovpn

Find the following lines:

ca ca.crt
cert server.crt
key server.key

dh dh1024.pem

Edit them as follows:

ca "C:\\Program Files\\OpenVPN\\config\\ca.crt"
cert "C:\\Program Files\\OpenVPN\\config\\server.crt"
key "C:\\Program Files\\OpenVPN\\config\\server.key"

dh "C:\\Program Files\\OpenVPN\\config\\dh1024.pem"

Save the file as C:\Program Files\OpenVPN\easy-rsa\server.ovpn


**Copy these files from** C:\Program Files\OpenVPN\easy-rsa\ to C:\Program
Files\OpenVPN\config\ on the server:

.. code-block:: text

   ca.crt
   dh2048.pem
   server.crt
   server.key
   server.ovpn

**Change some params in .ovpn file**:

#. Comment out "ta.key" mention
#. Change dh from 1024 to 2048

.. note::

   Once loaded into OpenVPN you can change existing configuration only
   by right-clicking and choosing "Change configuration".



For each client ("my-laptop")
-----------------------------


Copy client.ovpn file to configs/ folder.

Find the following lines:

.. code-block:: text

   ca ca.crt
   cert client.crt
   key client.key

Edit them as follows:

.. code-block:: text

   ca "C:\\Program Files\\OpenVPN\\config\\ca.crt"
   cert "C:\\Program Files\\OpenVPN\\config\\mike-laptop.crt"
   key "C:\\Program Files\\OpenVPN\\config\\mike-laptop.key"

Notice that the name of the client certificate and key files depends
upon the Common Name of each client.

.. note::

   You can also include the ca, cert and key content in the client
   file. You have to copy the file content inside the tag <ca></ca>,
   <cert></cert> and <key></key>.


Edit the following line, replacing "my-server-1" with your server's
public Internet IP Address or Domain Name. If you need help, see Static
Internet IP below.

.. code-block:: text

   # remote my-server-1 1194
   remote 10.254.239.6 1194

Save the file as C:\Program Files\OpenVPN\easy-rsa\mike-laptop.ovpn (in
this example. Each client will need a different, but similar, config
file depending upon that client's Common Name.)


Test on Linux:

.. code-block:: bash

   cp my-laptop.ovpn /etc/openvpn/client/my-laptop.conf
   systemctl start openvpn-client@my-laptop.service
