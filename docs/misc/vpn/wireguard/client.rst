Wireguard client
================

Debian
------

Create an automatic WireGuard connection at OS startup via the Systemd
initialization system, which allows you to start and manage Linux system
daemons:

.. code-block:: bash

   sudo apt install wireguard resolvconf

   # create config at /etc/wireguard/test.conf

   #
   sudo systemctl enable wg-quick@test.service



Example of balancer (as a client) config
----------------------------------------

[Interface]
PrivateKey =

Address = 10.10.0.2/32
DNS = 10.254.239.4

# fix fastly.net sites
MTU = 1280
PostUp =   iptables -A FORWARD -i test -m state --state RELATED,ESTABLISHED -j ACCEPT; iptables -A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu
PostDown = iptables -D FORWARD -i test -m state --state RELATED,ESTABLISHED -j ACCEPT; iptables -D FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

[Peer]
PublicKey =
Endpoint = VPNSERVER:51800

# All traffic goes through wireguard
# AllowedIPs = 0.0.0.0/0

# Only some IPs goes through wireguard
#              rutracker                                                                                                        https://releases.hashicorp.com                                             registry.terraform.io       pcgamesn.com
AllowedIPs = 104.21.56.234/32, 104.21.83.43/32, 172.67.137.176/32, 172.67.212.135/32,       188.114.96.1/32, 188.114.97.1/32,   65.9.44.0/24, 108.157.229.0/24, 18.173.5.0/24, 13.33.243.0/24,             151.101.86.49/32,          188.114.99.0/24, 188.114.98.0/24


# Check client-server connection every 20s
# PersistentKeepalive = 20
