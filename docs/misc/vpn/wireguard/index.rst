Wireguard
=========

Install and create server keys
------------------------------

.. code-block:: bash

   sudo apt install wireguard

   cd /etc/wireguard
   wg genkey | tee wg1-privatekey | wg pubkey | tee wg1-publickey
   chmod 600 privatekey


Create (multiple) clients
-------------------------

Generate client keys:

.. code-block:: bash

   cd /etc/wireguard
   NAME=hacker1; wg genkey | tee "/etc/wireguard/${NAME}_privatekey" | wg pubkey | tee "/etc/wireguard/${NAME}_publickey"


Create client config:

.. code-block:: bash

   [Interface]
   PrivateKey = ****CLIENT PRIVATE KEY****

   Address = 10.10.0.2/32
   DNS = 8.8.8.8

   [Peer]
   PublicKey = ****SERVER PUBLIC KEY****
   Endpoint = ****SERVER IP****:51800

   # All traffic goes through wireguard
   AllowedIPs = 0.0.0.0/0

   # Check client-server connection every 20s
   # PersistentKeepalive = 20


Add client info to server config (wg0.conf):

.. code-block:: bash

   [Peer]
   PublicKey = 5GueYbsKecKNSycPZPkWEVUN6C56ri1HX7jisvvkaCA=
   AllowedIPs = 10.10.0.15/32


Download your config file:

.. code-block:: bash

   scp root@195.181.245.180:/etc/wireguard/Sergey.ovpn ./
   scp -P 2222 root@10.254.239.4:/etc/wireguard/dron-test_client.conf ./


Server config
-------------

.. code-block:: bash

   vim /etc/wireguard/wg0.conf


.. code-block:: text

   [Interface]
   PrivateKey = ****SERVER PRIV KEY****
   Address = 10.10.0.1/24
   ListenPort = 51800
   # SaveConfig = true

   # substitute eth0 in the following lines to match the Internet-facing interface
   # if the server is behind a router and receives traffic via NAT, these iptables rules are not needed
   PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
   PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE

   [Peer]
   PublicKey = ****CLIENT PUBLIC KEY****
   AllowedIPs = 10.10.0.2/32

.. note::

   SaveConfig = true


Allow forwarding packets in kernel
----------------------------------

.. code-block:: bash

   echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
   sysctl -p

Start Wireguard
---------------

.. code-block:: bash

   systemctl enable wg-quick@wg0.service
   systemctl restart wg-quick@wg0.service
   systemctl status wg-quick@wg0.service


Monitoring
----------

.. code-block:: bash

   sudo wg show


Multiple devices problem
------------------------

Do not use same key on several devices!

If same key is used on multiple devices then problems appear: packets
will go to an incorrect user endpoint, users will see lags and
disconnections.


Firewall
--------

.. code-block:: bash

   nft list tables
   nft list table nat


Problems
--------

not working sites (fastly cdn) (mtu problem)

https://serverfault.com/questions/1110341/cant-access-fastly-cdn-sites-through-nat

https://forum.level1techs.com/t/strange-issue-with-github-when-routing-traffic-over-wireguard-vpn/189403/13
