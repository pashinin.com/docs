Shadowsocks
===========

Shadowsocks has different modes: proxy and VPN. It may work as a VPN
(default) so you don't event need Wireguard. But it has only a single
password.

Server
------

.. code-block:: bash

   apt install shadowsocks-libdev


server — ip вашего сервера. Надо доавить свой public IP.

password — меняем на свой пароль

method — выбираем метод с сайта shadowsocks

.. code-block:: bash

   firewall-cmd --add-port=8388/udp
   firewall-cmd --add-port=8388/tcp

   firewall-cmd --add-port=11000-11200/udp
   firewall-cmd --add-port=11000-11200/tcp


После этого Shadowsocks работает нормально. В режиме VPN даже Wireguard не нужен.



Wireguard client
----------------

Wireguard пускает весь траффик через себя. А нам надо его пустить через Shadowsocks.



.. code-block:: bash

   #!/usr/bin/env python
   # -*- coding: utf-8 -*-

   import base64
   import json
   import os
   import sys
   import re


   def get_files():
       res = []
       for file in sorted(os.listdir('.')):
           if not file.endswith(".json"):
               continue
           numbers = tuple(map(int, re.findall(r'\d+', file)))
           n = 0
           if numbers:
               n = numbers[0]
           res.append((n, file))
       res.sort()
       return res


   def ss_string(file):
       f = open(file)
       data = json.load(f)
       server = data['server'][0]
       server_port = data['server_port']
       method = data['method']
       password = data['password']
       mp = f"{method}:{password}"
       mp64 = base64.b64encode(mp.encode('utf-8')).decode('utf-8')
       # method64 = base64.b64encode(method.encode('utf-8'))
       # p = base64.b64encode(data['password'].encode('utf-8'))
       return f"ss://{mp64}@{server}:{server_port}"


   if __name__ == "__main__":
       for n, f in get_files():
           print(f"{f:<15}{ss_string(f)}")
