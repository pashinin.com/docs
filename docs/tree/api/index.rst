Tree API
========



.. http:post:: /tree_alias

   Create an alias for an existing Tree. For example when you open
   tree-xyz.hostname.tld you may want to see the contents of a
   tree-abc.hostname.tld. It is like a simple link to an exsiting Tree.

   .. code-block:: text

      {
          service: "files-rs",
          version: "0.1.0",
          description: "Files management",
          commit_short_sha: ""
      }
