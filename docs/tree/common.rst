Common Tree architecture
========================

"Everything is a Tree" vs "multiple tables with a connection tables"
--------------------------------------------------------------------

Trees are like "folders" in a filesystem. But what about files? Is it
also a Tree of a different type? Or should it be a separate table for
files and another connections table which points that file to a
specific Tree?

When everything is a Tree






Id ``int64``
  Base62 of this number is used in URLs.

  Currently I use :code:`id INT8 NOT NULL DEFAULT unique_rowid()`

Name ``string``
  All trees have a name. Unique in current folder.

ParentId ``int64``
  Id of a parent Tree. It duplicates data stored in a closure table but
  allows to SELECT direct children and direct parents without
  additional JOIN.


SoftDeletedAt ``Time``
  Date and time when a user clicked "Delete" to remove a Tree.
  A Tree will be hidden and deleted after 30 days.
  Can be restored within this time.

..
   ToRemove ``bool``
     Marks a Tree for removal. Such trees are not visible for anyone and
     are going to be DELETEd from a database by workers.


Size - ``uint64``, default: :code:`NULL`
  Size of a Tree in bytes.
  Not :code:`uint32` since folders can be bigger than 4Gb.
