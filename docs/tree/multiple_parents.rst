Multiple parents
================

Should a Tree have multiple parents?
------------------------------------

A task can be in different categories, structures or task lists.

Those trees (structures) can be separated by years and the same task can
of course be in several trees.

Same file can be in different folders. Altough file deduplication is
done with object storage, not Trees table.

So yes, a Tree can be in several other Trees.




Problems with multiple parents
------------------------------

#. It is not enough to supply Tree ID in URL to open a web-page. Need to
   supply both: Tree ID and it's Parent ID.

   .. code-block:: text

      /title?tree=xxxxxxxxxxxxxxxxx&parent=xxxxxxxxxxxxxxxxx

#. Can't detect unique name easily when creating a new Tree.

#. Permissions. Who can access a Tree if it exists in different Trees at
   the same moment and those Trees have different permissions?


Solution
--------

Do not have multiple parents of the same Tree.
