Tree snapshots
==============

Snapshots are required to make fast backups and save a Tree state.

Can be used for example

.. code-block:: text

   Tree-1
     Tree-2
       Tree-3

Tree changes
------------

Changing (renaming) a Tree creates a new Tree.

.. code-block:: text

   Tree-1       /-----Tree-1-NEW
     Tree-2 ----        Tree-2
       Tree-3             Tree-3


.. code-block:: text

   Tree-1       copy    Tree-1-NEW
     Tree-2     ---->     Tree-2
       Tree-3   ---->       Tree-3
