SQL queries for Tree objects
============================


INSERT
------

Complexity: :code:`O(parents count + 1)`

.. code-block:: text

   TRANSACTION {
      insert into main table (also settings "parent_id")

      for parent in parents {
         insert (parent-child) into closure table
      }
   }

The deeper an item the more it takes to INSERT.

.. note::

   Changing a Tree does not happen often. It is optimized for SELECTs.

..
   Adding/moving a new Tree into another Tree is simply settings it's
   :code:`ParentId` param. When :code:`ParentId IS NULL` such Tree is
   considered Root Tree.


SELECT
------

Having a closure table makes it possible to

#. SELECT all parents of an item
#. SELECT any children (any depth) of an item

in a single request with 1 JOIN only.

.. note::

   With parent IDs only it would take N requests to SELECT all parents
   of an item.

.. code-block:: sql

   -- Path retrieving

   SELECT id, name, parent_id, struct.lvl from trees tree
   JOIN tree_connections struct ON tree.id=struct.parent
   WHERE struct.child=431464806806847489 ORDER BY struct.lvl DESC;

.. code-block:: text

   +--------------------+------------------+--------------------+-------+
   | id                 | name             | parent_id          | lvl   |
   |--------------------+------------------+--------------------+-------|
   | 431188837079187457 | МГТУ им. Баумана | <null>             | 2     |
   | 431330221294780417 | ИУ               | 431188837079187457 | 1     |
   | 431464806806847489 | ИУ-2             | 431330221294780417 | 0     |
   +--------------------+------------------+--------------------+-------+

.. code-block:: sql

   -- Direct children

   SELECT id, name from trees tree
   JOIN structures struct ON tree.id=struct.child
   WHERE struct.parent=431330221294780417 AND struct.lvl=1;

.. code-block:: text

   +--------------------+--------+
   | id                 | name   |
   |--------------------+--------|
   | 431464806806847489 | ИУ-2   |
   | 435678203017297921 | ИУ-1   |
   ...
   | 453633630175690755 | ИУ-9   |
   | 453634152575401987 | ИУ-11  |
   | 453650119615578115 | ИУ-10  |
   +--------------------+--------+


.. code-block:: sql

   -- Select Trees with incorrect parent_id
   select id, name from trees where not parent_id IN (select id from trees);

   DELETE FROM trees WHERE id IN (
   SELECT id from trees tree
   JOIN tree_connections tc ON tree.id=tc.child
   WHERE tc.parent<>tree.parent_id AND tc.lvl=1);
