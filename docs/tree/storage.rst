Tree storage in SQL
===================

Currently SQL database (Postgres) is used to store tree structured
data.

At first I've used two techinques at the same time.

Closure table
-------------

A separate table (tree_connections) for storing parent-child information.

2. `parent_id` field in the "trees" table.  10-15% faster to get a list of direct
   children than using JOIN with closure table.

.. warning::

   Using `parent_id` field restricts a Tree to have only one parent.
   Same Tree can be used in several places. Examples:

   * a book/article/film can be in multiple categories

   Thus not using this field, **using only closure table**.


+------------------------------+-----------------------------+
| Operation                    | O(n)                        |
+==============================+=============================+
| Insert                       | O(<tree depth>)             |
+------------------------------+-----------------------------+
| SELECT all parents           | O(1)                        |
+------------------------------+-----------------------------+
| SELECT all children          | O(1)                        |
+------------------------------+-----------------------------+
| DELETE                       | O(<tree depth>)             |
+------------------------------+-----------------------------+


..
   All operations need at most **one** JOIN.

..
   #. **"id - parent id" connection.**

      + ultra fast inserts - O(1). But actually "inserts" should not be so important.

      + That will cause a problem when getting all elements from a current
        one to the root one (whole path). This can't be made in 1 SQL request.

   #. Nested sets
