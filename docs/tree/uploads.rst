Uploads
=======

All uploaded files and newly created folders are always marked as DRAFT
(:code:`Draft = true`) at first.

An access to a "draft" Tree has only :code:`Owner` or admin or people
who were granted such permissions.

Anonymous uploaders can not control files after upload in any way.

Name collision
--------------

When uploading a bunch of files into a folder the names of files and/or
folders can clash with existing structure.

#. When uploading a file :code:`NAME` into a folder where another
   folder :code:`NAME` exists: :code:`/folder/FILENAME/` Just try to
   put uploading file into that folder. Resulting structure:
   :code:`folder/NAME/NAME`.

#. When existing file (Tree of :code:`type=1`) and uploaded file are
   identical (names and SHA1 hashes are the same) - "think" that the
   upload was successful (and auto published if an existing file was
   published).

#. When existing file (Tree of :code:`type=1`) and uploaded file have the
   same name but different SHA1 hashes - a new Tree must be created with
   :code:`type=2` (alias to a Tree) pointing to existing Tree (set
   :code:`parent_id` set to an existing Tree id).

   Then this draft will be manually checked and maybe the existing Tree
   will be replaced with this new draft.

#. Otherwise just create a new :code:`draft` Tree with :code:`Txt1`
   field set to SHA1 of an uploaded file.
