UI consists of panels that user can switch between.

#. :code:`preview`

   When no items selected in a list this panel displays information
   about current tree: name, total number of items.

   When items are selected this panel displays information
   about selected items.


Navigation
----------

Navigation is primarily single-click.

When user single-clicks on a folder - user moves to this folder right
away.

When user single-clicks on another item - it depends. If user has
multi-column interface then selected item is only displayed in
:code:`preview` panel. When there is only one column in interface then
user is moved to the page which main view will display this item.

Selection
---------

Ctrl-click is used to start selection.

Two-column interface
--------------------

Interface is two-column on wide enough screens.

Left column is always a tree list. Single-click selects an
item. Double-click opens an item.

Right column by default is :code:`preview` but can be switched. The
default :code:`preview` panel can't be closed. Any other panels can be
closed to return back to :code:`preview` panel.


One-column interface
--------------------
