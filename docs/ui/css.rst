CSS
===

Common CSS
----------

There are common CSS (and JS) files used in several projects. Named like
:code:`main.4fe4e660a5953c55f9af.css`. Different projects might use
different versions of CSS files. So multiple files should co-exist:

.. code-block:: text

   main.[hash-1].css
   main.[hash-2].css
   ...

But each build will generate files of current version only.
