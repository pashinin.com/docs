Deps
====

New projects deps
-----------------

.. code-block:: text

   "dependencies": {
     "core-js": "^2.6.5",
     "register-service-worker": "^1.6.2",
     "vue": "^2.6.10",
     "vue-class-component": "^7.0.2",
     "vue-property-decorator": "^8.1.0",
     "vue-router": "^3.0.3",
     "vuex": "^3.0.1"
   },
   "devDependencies": {
     "@vue/cli-plugin-babel": "^3.11.0",
     "@vue/cli-plugin-pwa": "^3.11.0",
     "@vue/cli-plugin-typescript": "^3.11.0",
     "@vue/cli-service": "^3.11.0",
     "sass": "^1.18.0",
     "sass-loader": "^7.1.0",
     "typescript": "^3.4.3",
     "vue-template-compiler": "^2.6.10"
   }


deepmerge
---------

Required from Vuetify docs.


fibers
------

Required from Vuetify docs.




vue-cli-plugin-vuetify
----------------------

Loads `vuetify-loader` into Webpack which does tree shaking for Vuetify.


terser
------

Instead of `uglify-js` (to minify code) but this project is active.


chalk
-----

Colorizes test in terminal.


sinon
-----

For testing.
