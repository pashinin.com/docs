Forms
=====

Example:

.. code-block:: text

   <form action="//{{f.api_host()}}/auth/signout" data-cb="signout">
     <button id="btn-exit" type="submit">
       <span class="loading"><span class="title">Выход</span></span>
     </button>
   </form>
