Icons
=====

The best icons project is `MDI <https://materialdesignicons.com/>`_.

Default example is stupid - to import all icons at once (increasing a
bundle size by several hundred Kb).

The correct way to import icons is described in `Vuetify docs
<https://vuetifyjs.com/en/customization/icons#install-material-design-icons-js-svg>`_:

.. code-block:: text

   <template>
     <v-icon>{{ svgPath }}</v-icon>
   </template>

   <script>
     import { mdiAccount } from '@mdi/js'

     export default {
       data: () => ({
         svgPath: mdiAccount
       }),
     }
   </script>

Here an "mdi-account" icon is imported *only*.
