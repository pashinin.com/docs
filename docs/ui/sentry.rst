Check Sentry in browser
=======================

To check if Sentry is working in a browser run this in console:

.. code-block:: text

   __SENTRY__.hub.getClient()


:code:`undefined` means Sentry was not initialized. Any other object
means Sentry is working.
