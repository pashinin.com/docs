Source maps
===========

I need source maps in both dev and prod. In prod - for Sentry (to see
good error reports).

.. code-block:: bash

   yarn add -D source-map-loader



Use ""inlineSources": true," in tsconfig.
