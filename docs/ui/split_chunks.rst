Split JS chunks
===============

script tags in HTML can be "defer", "async" and without any keyword.

Webpack by default makes them all "defer".

To make it async edit HtmlWebpackPlugin settings:

new HtmlWebpackPlugin({
    filename: 'scripts.html',
    scriptLoading: 'defer',
