Updates
=======

Frontend updates are connected with the fact that frontend
uses :term:`service workers<Service worker>`. Currently user sees an
"Update" button that he must click to apply the update when a site
update is available. Otherwise the user will be using the old version.

In other words - browser's "reload" button doesn't work the way it was
without service workers. Service worker is in a "waiting" state, waiting
to be activated.

To read: https://github.com/GoogleChrome/workbox/issues/1120

https://stackoverflow.com/questions/40100922/activate-updated-service-worker-on-refresh


.. note::

   If you need to test a service worker locally, build the application and
   run a simple HTTP-server from your build directory. It's recommended to
   use a browser incognito window to avoid complications with your browser
   cache:

   .. code-block:: bash

      sudo yarn global add serve  # run only once

      make build
      make serve

   Then make changes to the source code and run these commands
   again. Reload the page and you must see a notification to update a site.


When a user clicks "Update" button we only tell a new service worker to
become active. And when it becomes active we need to reload a
page with :code:`document.location.reload()`. To detect this event:

:code:`navigator.serviceWorker.addEventListener('controllerchange', () => {`

.. note::

   fired when the document's associated ServiceWorkerRegistration acquires
   a new ServiceWorkerRegistration.active worker.
